		<meta charset="utf-8">
		<title>{{ $pageTitle }}</title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- #CSS Links -->
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/font-awesome.min.css') }}">

		<!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/smartadmin-production-plugins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/smartadmin-production.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/smartadmin-skins.min.css') }}">

		<!-- SmartAdmin RTL Support is under construction
			 This RTL CSS will be released in version 1.5
		<link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> -->

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

		<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/css/your_style.css') }}">

		<!-- #FAVICONS -->
		<link rel="shortcut icon" href="{{ URL::asset('assets/smartadmin/img/favicon/favicon.ico') }}" type="image/x-icon">
		<link rel="icon" href="{{ URL::asset('assets/smartadmin/img/favicon/favicon.ico') }}" type="image/x-icon">
		<link rel="apple-touch-icon" sizes="57x57" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-57x57.png') }}">
		<link rel="apple-touch-icon" sizes="60x60" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-60x60.png') }}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-72x72.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-76x76.png') }}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-114x114.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-120x120.png') }}">
		<link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-144x144.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-152x152.png') }}">
		<link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('assets/smartadmin/img/favicon/apple-icon-180x180.png') }}">
		<link rel="icon" type="image/png" sizes="192x192"  href="{{ URL::asset('assets/smartadmin/img/favicon/android-icon-192x192.png') }}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('assets/smartadmin/img/favicon/favicon-32x32.png') }}">
		<link rel="icon" type="image/png" sizes="96x96" href="{{ URL::asset('assets/smartadmin/img/favicon/favicon-96x96.png') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('assets/smartadmin/img/favicon/favicon-16x16.png') }}">
		<link rel="manifest" href="{{ URL::asset('assets/smartadmin/img/favicon/manifest.json') }}">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="{{ URL::asset('assets/smartadmin/img/favicon/ms-icon-144x144.png') }}">
		<meta name="theme-color" content="#ffffff">

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- #APP SCREEN / ICONS -->
		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="{{ URL::asset('assets/smartadmin/img/splash/sptouch-icon-iphone.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ URL::asset('assets/smartadmin/img/splash/touch-icon-ipad.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ URL::asset('assets/smartadmin/img/splash/touch-icon-iphone-retina.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ URL::asset('assets/smartadmin/img/splash/touch-icon-ipad-retina.png') }}">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/smartadmin/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/smartadmin/img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="{{ URL::asset('assets/smartadmin/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('assets/smartadmin/js/plugin/jquery-ui-month-picker/css/MonthPicker.css') }}">
