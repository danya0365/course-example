		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

		<!-- #PLUGINS -->
		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-2.0.2.min.js') }}"><\/script>');
			}
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
			}
		</script>

		<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>

		<!-- BOOTSTRAP JS -->
		<script src="{{ URL::asset('assets/smartadmin/js/bootstrap/bootstrap.min.js') }}"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="{{ URL::asset('assets/smartadmin/js/notification/SmartNotification.min.js') }}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{ URL::asset('assets/smartadmin/js/smartwidgets/jarvis.widget.min.js') }}"></script>

		<!-- EASY PIE CHARTS -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>

		<!-- SPARKLINES -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>

		<!-- JQUERY VALIDATE -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/select2/select2.min.js') }}"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>

		<!-- browser msie issue fix -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>

		<!-- FastClick: For mobile devices: you can disable this in app.js -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/fastclick/fastclick.min.js') }}"></script>

		<!--[if IE 8]>
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->

		<script src="{{ URL::asset('assets/smartadmin/js/plugin/maxlength/bootstrap-maxlength.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/clockpicker/clockpicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/noUiSlider/jquery.nouislider.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/ion-slider/ion.rangeSlider.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/knob/jquery.knob.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/moment.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/jquery.mockjax.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/x-editable.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/typeahead/typeahead.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/typeahead/typeaheadjs.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/dropzone/dropzone.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-ui-month-picker/MonthPicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-number/jquery.number.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/summernote/summernote.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
		<!-- <script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-latitude-longitude-picker-gmaps/js/jquery-gmaps-latlon-picker.js') }}"></script> -->

		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.config.js') }}"></script>

		<!-- MAIN APP JS FILE -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.min.js') }}"></script>


		<div class="upload-progress">
		<div class="bar-holder">
			<div class="progress">
				<div class="progress-bar bg-color-teal" aria-valuetransitiongoal="25" aria-valuenow="25" style="width: 0;">0%</div>
			</div>
		</div>
		</div>

		<!-- end widget grid -->

		<style>
		div.upload-progress
		{
			position: fixed;
			top: 50%;
			left: 0;
			width: 100%;
			height: 50px;
			display: none;
			z-index: 99999;
		}
		.bar-holder
		{
			margin: 0 auto;
			width: 90%;
		}
		</style>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">


		function sendFileForSummernote(file, editor, welEditable)
		{
		    data = new FormData();
		    data.append("file", file);
		    $.ajax({
		        data: data,
		        type: "POST",
		        xhr: function() {
		                var myXhr = $.ajaxSettings.xhr();
		                if (myXhr.upload) myXhr.upload.addEventListener('progress',progressHandlingFunction, false);
		                return myXhr;
		            },
		        url: "{{ URL::to('uploads/image'); }}",
		        cache: false,
		        contentType: false,
		        processData: false,
		        success: function(response) {

					if ( response.status == '1' )
					{
						var imageList = response.data;
						var imageUrl = imageList['original'];

						editor.insertImage(welEditable, imageUrl);
					}


		            console.log(response);
		        }
		    });
		}


		function progressHandlingFunction(e){

		    if(e.lengthComputable){

		    	$('div.upload-progress').fadeIn();
		        //$('progress').attr({value:e.loaded, max:e.total});
		        var width = parseInt( e.loaded * 100 / e.total );
		        $('div.progress>div').css('width',  width + '%').html(width + '%');


		        // reset progress on complete
		        if (e.loaded == e.total) {
		            //$('progress').attr('value','0.0');

		            setTimeout(function(){ $('div.upload-progress').fadeOut(function(){ $('div.progress>div').css('width',  0); }); }, 1000);
		        }
		    }

		    console.log(e);
		}


		function showConfirmDelete(dataSend, url, callback)
		{

			$.SmartMessageBox({
				title : "ยืนยันลบข้อมูล",
				content : "การกระทำนี้ไม่สามารถย้อนกลับได้ กรุณายืนยันอีกครั้ง",
				buttons : '[No][Yes]'
			}, function(ButtonPressed) {
				if (ButtonPressed === "Yes") {

					setTimeout(function()
					{
						ajaxPost(dataSend, url, callback);
					}, 500);

				}
				if (ButtonPressed === "No") {

				}

			});

		}

		function attachDrobzone(target, url, callback)
		{
			Dropzone.autoDiscover = false;

			$(target).dropzone({
	            createImageThumbnails: false,
				method: "post",
				url: url,
				maxFilesize: 2.0,
				dictResponseError: 'Error uploading file!',
				clickable: true,
	            dictDefaultMessage: '',
			    init: function() {

			        thisDropzone = this;

			        this.on("complete", function (file) {

				      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {

				      }

				    });

				    this.on("success", function(file, response) {
				      thisDropzone.removeAllFiles(true);

					  if ( typeof(callback) == "function" ) {
					  	callback(file, response);
					  }

				    });

				    this.on("removedfile", function(file) {

				      //$('#banner_image').val('');

				    });

	                //and this to handle any error
	                this.on("error", function(file, response) {

	                    /*
	                    var errorMessage = 'Please try again or select new file';

	                      if (file.size > 2.0*1024*1024) {
	                         errorMessage = "File was Larger than 3.5Mb!";
	                      }

	                      if(!file.type.match('image.*')) {
	                        errorMessage = "Upload Image Only!";

	                      }
	                      thisDropzone.removeAllFiles(true);
						  */

	                    thisDropzone.removeAllFiles(true);

						console.log(response);

	  					$.bigBox({
							title : "Error",
							content : response,
							color : "#C46A69",
							timeout: 3000,
							icon : "fa fa-warning shake animated",
							//number : "4"
						}, function() {

						});
	                });

			    },
			});
		}

		function ajaxPost(dataSend, url, callback)
		{
			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: url,
				type: 'POST',
				success: function(data)
				{
					//console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>" + ( data.message == '' ? 'สำเร็จ' : data.message ) + "</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}

					}

				},
				error: function(a,b,c)
				{

					console.log(a);

					$.smallBox({
							title : "Failed ",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		}

		  $.sound_path = "{{ URL::to('assets/smartadmin/sound') }}/";

		  {{ ! Input::get('ajax') ? ' $(function(){ pageJavascriptTrigger() }) ' : '' }}

		</script>
