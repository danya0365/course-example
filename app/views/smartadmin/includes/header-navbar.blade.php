
<script type="text/javascript">

</script>
<header>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="navbar navbar-default">

					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="{{ URL::to('home') }}">หน้าแรก</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
						@if ( $app->isLogedin )
							<li class="{{ Request::url() == URL::to('updated_profile') ? 'active' : '' }} ">
								<a class="ajax" href="{{ URL::to('update_profile') }}">อัพเดตโปรไฟล์</a>
							</li>
							<li class="{{ Request::url() == URL::to('course_list') ? 'active' : '' }} ">
								<a class="ajax" href="{{ URL::to('course_list') }}">คอร์สเรียน</a>
							</li>
							<li class="{{ Request::url() == URL::to('admin') ? 'active' : '' }} ">
								<a href="{{ URL::to('admin') }}">จัดการหลังบ้าน</a>
							</li>
							<li class="{{ Request::url() == URL::to('change_password') ? 'active' : '' }} ">
								<a class="ajax" href="{{ URL::to('change_password') }}">เปลี่ยนรหัสผ่าน</a>
							</li>
						@else
							<li class="{{ Request::url() == URL::to('login') ? 'active' : '' }} ">
								<a href="{{ URL::to('login') }}">เข้าสู่ระบบ</a>
							</li>
							<!--
							<li class="{{ Request::url() == URL::to('resend_confirm_email') ? 'active' : '' }} ">
								<a href="{{ URL::to('resend_confirm_email') }}">ขอรหัสยืนยันอีเมล์ใหม่</a>
							</li>
						-->
						@endif
						</ul>

						<ul class="nav navbar-nav navbar-right">
							@if ( $app->isLogedin )
							<li>
								<a href="{{ URL::to('logout') }}">ออกจากระบบ [{{ $app->user->user_name }}]</a>
							</li>
							@endif
						</ul>
					</div><!-- /.navbar-collapse -->

				</div>
			</div>
		</div>
	</div>
</header>
