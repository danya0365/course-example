		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

		<!-- #PLUGINS -->
		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-2.0.2.min.js') }}"><\/script>');
			}
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
			}
		</script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>

		<!-- BOOTSTRAP JS -->
		<script src="{{ URL::asset('assets/smartadmin/js/bootstrap/bootstrap.min.js') }}"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="{{ URL::asset('assets/smartadmin/js/notification/SmartNotification.min.js') }}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{ URL::asset('assets/smartadmin/js/smartwidgets/jarvis.widget.min.js') }}"></script>

		<!-- EASY PIE CHARTS -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>

		<!-- SPARKLINES -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>

		<!-- JQUERY VALIDATE -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/select2/select2.min.js') }}"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>

		<!-- browser msie issue fix -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>

		<!-- FastClick: For mobile devices: you can disable this in app.js -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/fastclick/fastclick.min.js') }}"></script>

		<!--[if IE 8]>
			<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>
		<![endif]-->

		<script src="{{ URL::asset('assets/smartadmin/js/plugin/maxlength/bootstrap-maxlength.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/clockpicker/clockpicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-tags/bootstrap-tagsinput.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/noUiSlider/jquery.nouislider.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/ion-slider/ion.rangeSlider.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/knob/jquery.knob.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/moment.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/jquery.mockjax.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/x-editable/x-editable.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/typeahead/typeahead.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/typeahead/typeaheadjs.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/dropzone/dropzone.min.js') }}"></script>
		<script type="text/javascript">
			Dropzone.autoDiscover = false;
		</script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>

		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-ui-month-picker/MonthPicker.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-number/jquery.number.min.js') }}"></script>

		<script src="{{ URL::asset('assets/smartadmin/js/plugin/summernote/summernote.min.js') }}"></script>

		<script src="{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>



		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.config.js') }}"></script>

		<!-- MAIN APP JS FILE -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.min.js') }}"></script>


		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">

		function ajaxPost(dataSend, url, callback)
		{
			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: url,
				type: 'POST',
				success: function(data)
				{
					//console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>" + ( data.message == '' ? 'สำเร็จ' : data.message ) + "</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}

					}

				},
				error: function(a,b,c)
				{

					console.log(a);

					$.smallBox({
							title : "Failed ",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		}
		
		  $.sound_path = "{{ URL::to('assets/smartadmin/sound') }}/";

		  {{ ! Input::get('ajax') ? ' $(function(){ pageJavascriptTrigger() }) ' : ' ' }}

		</script>
