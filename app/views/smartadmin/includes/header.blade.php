

			<div id="logo-group" style="width: 170px">



				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo" style="width: 170px"> <img src="{{ URL::asset('assets/smartadmin/img/logo-white.png') }}" alt="Smart Logo"> </span>
				<!-- END LOGO PLACEHOLDER -->



			</div>


			<!-- #TOGGLE LAYOUT BUTTONS -->
			<!-- pulled right: nav area -->
			<div class="pull-left">

				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-left" style="margin-left: 13px; padding: 0">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

			</div>



			<!-- #TOGGLE LAYOUT BUTTONS -->
			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- #MOBILE -->
				<!-- Top menu profile link : this shows only when top menu is active -->
				<ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
					<li class="">
						<a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
							<img src="{{ $app->user->photo ? $app->user->photo : URL::asset('assets/smartadmin/img/avatars/sunny-big.png') }}" alt="{{ $app->user->name }}" class="online" />
						</a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
							</li>
							<li class="divider"></li>
							<li>
								<a href="{{ URL::to('profile')}}" class="padding-10 padding-top-5 padding-bottom-5"><i class="fa fa-sign-out fa-lg"></i> <strong><u>P</u>rofile</strong></a>
							</li>
						</ul>
					</li>
				</ul>

				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right">
					<span> <a href="{{ URL::to('profile')}}" title="ไปรไฟล์"><i class="fa fa-sign-out"></i></a> </span>
				</div>
				<!-- end logout button -->


				<!-- fullscreen button -->
				<div id="fullscreen" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
				</div>
				<!-- end fullscreen button -->



			</div>
			<!-- end pulled right: nav area -->
