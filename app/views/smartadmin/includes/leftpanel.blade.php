		<!-- #NAVIGATION -->
		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as is -->

					<a href="{{ Request::url() }}">
						<img src="{{ $app->user->photo ? $app->user->photo : URL::asset('assets/smartadmin/img/avatars/1.png') }}" alt="{{ $app->user->user_name }}" class="online" />
						<span>

							@if ( $app->isLogedin )
								{{ $app->user->user_name }}
							@else
								Guest
							@endif
						</span>
					</a>

				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive

			To make this navigation dynamic please make sure to link the node
			(the reference to the nav > ul) after page load. Or the navigation
			will not initialize.
			-->
			<nav>
				<!--
				NOTE: Notice the gaps after each icon usage <i></i>..
				Please note that these links work a bit different than
				traditional href="" links. See documentation for details.
				-->

				<ul>
					@if ( $app->user->permission->is_can_manage_user )
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">จัดการผู้ใช้งาน</span></a>
						<ul>
							<li>
								<a href="{{ URL::asset('admin/user/all_users') }}"><i class="fa fa-lg fa-fw fa-list"></i> ผู้ใช้งานทั้งหมด</a>

							</li>

						</ul>
					</li>
					@endif


					@if ( $app->user->permission->is_can_manage_role )
					<li>
						<a href="#"><i class="fa fa-lg fa-fw fa-group"></i> <span class="menu-item-parent">จัดการกลุ่มผู้ใช้งาน</span></a>
						<ul>
							<li>
								<a href="{{ URL::asset('admin/role/all_roles') }}"><i class="fa fa-lg fa-fw fa-list"></i> กลุ่มผู้ใช้งานทั้งหมด</a>

							</li>

						</ul>
					</li>
					@endif

					@if ( $app->user->permission->is_can_manage_setting )
					<li>
						<a href="{{ URL::asset('admin/setting') }}"><i class="fa fa-lg fa-fw fa-gear"></i> <span>ตั้งค่า</span></a>
					</li>
					@endif

				</ul>


			</nav>
			<span class="minifyme" data-action="minifyMenu"> <i class="fa fa-arrow-circle-left hit"></i> </span>

		</aside>
		<!-- END NAVIGATION -->
