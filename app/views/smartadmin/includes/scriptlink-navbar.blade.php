		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)
		<script data-pace-options='{ "restartOnRequestAfter": true }' src="js/plugin/pace/pace.min.js"></script>-->

		<!-- #PLUGINS -->
		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-2.0.2.min.js') }}"><\/script>');
			}
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ URL::asset('assets/smartadmin/js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
			}
		</script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>

		<!-- BOOTSTRAP JS -->
		<script src="{{ URL::asset('assets/smartadmin/js/bootstrap/bootstrap.min.js') }}"></script>

		<!-- CUSTOM NOTIFICATION -->
		<script src="{{ URL::asset('assets/smartadmin/js/notification/SmartNotification.min.js') }}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{ URL::asset('assets/smartadmin/js/smartwidgets/jarvis.widget.min.js') }}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>



		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('assets/smartadmin/js/plugin/jquery-number/jquery.number.min.js') }}"></script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.config.js') }}"></script>

		<!-- MAIN APP JS FILE -->
		<script src="{{ URL::asset('assets/smartadmin/js/app.min.js') }}"></script>


		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">

		$(function()
		{
			$('.navbar-nav a').on('click', function(e)
			{
				$('.navbar-nav li').removeClass('active');
				$(this).parent().addClass('active');
			})
		})

		function ajaxPost(dataSend, url, callback)
		{
			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: url,
				type: 'POST',
				success: function(data)
				{
					//console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>" + ( data.message == '' ? 'สำเร็จ' : data.message ) + "</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

						if ( typeof(callback) == "function" ) {

							callback(data);
						}

					}

				},
				error: function(a,b,c)
				{

					console.log(a);

					$.smallBox({
							title : "Failed ",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		}


		  $.sound_path = "{{ URL::to('assets/smartadmin/sound') }}/";

		  {{ ! Input::get('ajax') ? ' $(function(){ pageJavascriptTrigger() }) ' : ' ' }}

		</script>
