<!DOCTYPE html>
<html lang="en-us">
	<head>
		@include('smartadmin.includes.head')
		@yield('head')
	</head>

	<body>



			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				@yield('content', 'Default Layout Content')

			</div>


@include('smartadmin.includes.scriptlink-single')
@yield('script')




	</body>
</html>
