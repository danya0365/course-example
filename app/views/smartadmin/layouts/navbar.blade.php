<!DOCTYPE html>
<html lang="en-us">
	<head>
		@include('smartadmin.includes.head')
		@yield('head')
		<style media="screen">
		#navbar-layout #content {
			padding: 10px 14px !important;
			position: relative;
			margin-bottom: 60px;
		}
		</style>
	</head>

	<body id="navbar-layout">

		@include('smartadmin.includes.header-navbar')

		<!-- MAIN CONTENT -->
		<div id="content-wrapper">
			<div id="content" class="container">

				@yield('content', 'Default Layout Content')

			</div>
		</div>


@include('smartadmin.includes.footer-single')
@include('smartadmin.includes.scriptlink-navbar')
@yield('script')




	</body>
</html>
