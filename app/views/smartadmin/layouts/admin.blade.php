<!DOCTYPE html>
<html lang="en-us">
	<head>
	@include('smartadmin.includes.head')


	<style media="screen">
	@media (min-width: 768px)
{
.modal-dialog {
  margin: 30px auto !important;
}
}
.note-editor .note-editable {
  font-family: Menlo,Monaco,monospace,sans-serif !important;
}

.note-editor .note-codable {
  display: none;
  width: 100%;
  padding: 10px;
  border: none;
  box-shadow: none;
  font-family: Menlo,Monaco,monospace,sans-serif !important;
  font-size: 14px;
  color: #ccc;
  background-color: #222;
  resize: none;
  -ms-box-sizing: border-box;
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  -webkit-border-radius: 0;
  -moz-border-radius: 0;
  border-radius: 0;
  margin-bottom: 0;
}
.codeview .note-codable { display: block !important}

.modal-dialog .modal-header {
  padding: 15px ;
  border-bottom: 1px solid #e5e5e5;
  min-height: 16.43px;
}
.modal-dialog .modal-body {
  position: relative ;
  padding: 20px;
}
.modal-dialog h5
{
  font-size: 17px ;
  font-weight: 300;
  margin: 10px 0;
  line-height: normal;
}
.modal-dialog .modal-footer {
  padding: 20px;
  text-align: right;
  border-top: 1px solid #e5e5e5;
}
.modal-dialog .btn {
  display: inline-block;
  margin-bottom: 0;
  font-weight: 400;
  text-align: center;
  vertical-align: middle;
  touch-action: manipulation;
  cursor: pointer;
  background-image: none;
  border: 1px solid transparent;
  white-space: nowrap;
  padding: 6px 12px;
  font-size: 13px;
  line-height: 1.42857143;
  border-radius: 2px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}
	</style>
	</head>
<body  class="fixed-header fixed-page-footer fixed-ribbon fixed-navigation smart-style-1">

	<header id="header">
		@include('smartadmin.includes.header')
	</header>

	@include('smartadmin.includes.leftpanel')

		<!-- #MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">

				<span class="ribbon-button-alignment">
					<span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh" rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true" data-reset-msg="Would you like to RESET all your saved widgets and clear LocalStorage?"><i class="fa fa-refresh"></i></span>
				</span>

				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<!-- This is auto generated -->
				</ol>
				<!-- end breadcrumb -->

				<!-- You can also add more buttons to the
				ribbon for further usability

				Example below:

				<span class="ribbon-button-alignment pull-right">
				<span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
				<span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
				<span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
				</span> -->

			</div>
			<!-- END RIBBON -->

			<!-- #MAIN CONTENT -->
			<div id="content">
				@yield('content', 'Default Layout Content')
			</div>

			<!-- END #MAIN CONTENT -->

		</div>
		<!-- END #MAIN PANEL -->


@include('smartadmin.includes.footer')
@include('smartadmin.includes.shortcut')
@include('smartadmin.includes.scriptlink')
@yield('bottom')
</body>
</html>
