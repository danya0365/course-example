<!DOCTYPE html>
<html lang="en-us" id="extr-page">
	<head>
		@include('smartadmin.includes.head')
		@yield('head')
	</head>
	
	<body>

		@include('smartadmin.includes.header-single')

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
				
				@yield('content', 'Default Layout Content')
				
			</div>

		</div>

@include('smartadmin.includes.footer-single')		
@include('smartadmin.includes.scriptlink-single')	  
@yield('script')
		
		


	</body>
</html>