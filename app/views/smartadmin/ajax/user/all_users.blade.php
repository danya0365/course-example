
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>All Users</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 5%" >Id</th>
									<th style="width: 20%" class="nosort">First - Last Name</th>
									<th style="width: 10%" class="nosort">User Name</th>
									<th style="width: 10%" class="nosort">Nick Name</th>
									<th style="width: 10%" class="nosort">Roles</th>

									<th style="width: 10%">Updated by</th>
									<th style="width: 10%">Updated at</th>

								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->



<a style="display: none" data-toggle="modal" data-target="#createDataModal" id="createDataModal-button">Click me</a>

<!-- Modal -->
<div class="modal fade" id="createDataModal" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">


			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="createDataModalLabel">สร้างใหม่</h4>
			</div>
			<div class="modal-body">




			<form id="createData-form">
				<div class="row">

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							User Name
							</label>

							<input class="form-control" name="user_name" />

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Nick Name
							</label>

							<input class="form-control" name="nick_name" />

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="password">
							Password
							</label>

							<input class="form-control" name="password" id="password" />

						</div>
					</div>
					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="user_roles">User Roles</label>
							<select multiple="" class="form-control" id="user_roles" size="5" style="height: 100%;">
							@foreach ( $roleList AS $role)
								<option value="{{ $role->id }}">{{ $role->name }}</option>
							@endforeach
							</select>
						</div>
					</div>
				</div>

			</form>




			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					ยกเลิก
				</button>
				<button type="button" class="btn btn-primary" id="createData-submit">
					ยืนยัน
				</button>
			</div>



		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var otable;


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/







		/* COLUMN FILTER  */
	    otable = $('#datatable').DataTable({
			"processing": false,
	        "serverSide": true,
	        "ajax": {
	            "url": "{{ Request::url() }}"
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();
		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 0, "desc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					},
					"type": "num"
		        },
		        {
		        	"data": "full_name",
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/user/edit') }}' + '/' + full.id;
						var fullName = full.first_name + ' ' + full.last_name;
					    return '<a href="' + url + '" class="ajax">' + ( fullName == ' ' ? '<i>Empty</i>' : fullName ) + '</a>';
					    //return '<a href="#" class="whois_name_edit" id="whois_name_' + full.whois_id + '" data-type="text" data-pk="1" data-original-title="Enter whois name">' + full.whois_name + '</a>';
					} ,
					"type": "string"
		        },
				{
		        	"data": "user_name",
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/user/edit') }}' + '/' + full.id;
					    return '<a href="' + url + '" class="ajax">' + ( data == '' ? '<i>Empty</i>' : data ) + '</a>';
					    //return '<a href="#" class="whois_name_edit" id="whois_name_' + full.whois_id + '" data-type="text" data-pk="1" data-original-title="Enter whois name">' + full.whois_name + '</a>';
					} ,
					"type": "string"
		        },
		        {
		        	"data": "nick_name",
		        	"render": function ( data, type, full, meta ) {
						var url = '{{ URL::to('admin/user/edit') }}' + '/' + full.id;
					    return '<a href="' + url + '" class="ajax">' + ( data == '' ? '<i>Empty</i>' : data ) + '</a>';
					} ,
					"type": "string"
		        },


		        {
		        	"data": "roles",
		        	"render": function ( data, type, full, meta ) {
						if ( ! full.roles ) {
							return '-';
						}

		        		var roles = [];
		        		$.each(full.roles, function(k,v)
		        		{
			        		roles.push(v.name);
		        		});

					    return roles.join(', ');
					} ,
					"type": "string"
		        },

		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {

						if ( full.updated_user ) {

							return full.updated_user.user_name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);

					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;
						// X-Editable
						//if ( full.status == 0 )

					    	//return '<a href="#" class="status-selected" id="status_' + full.whois_id + '" data-type="select" data-pk="1" data-value="' + full.status + '" data-original-title="' + full.status_title + '"></a>';

					    //else
					    	//return ( full.status == 1 ? '<span class="txt-color-green">' + full.status_title + '</span>' : '<span class="txt-color-red">' + full.status_title + '</span>' )
					    // End X-Editable

					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });



		$('#createDataModal').on('show.bs.modal', function (e) {

				$("#user_name").val('');
				$("#nick_name").val('');
				$("#first_name").val('');
				$("#last_name").val('');
				$("#password").val('');
		});


		// custom toolbar
    	$("div.toolbar").html('<div class="text-right"><button class="btn btn-primary" id="createData-button">เพิ่มข้อมูลใหม่</button></div>');

		$('#createData-button').click(function()
		{
			$('#createDataModal').modal('show');
		});


		$('#createData-submit').click(function(e)
		{
			$(this).prop('disabled');

			e.preventDefault();
			//alert('Submit');

			var dataSend = $('#createData-form').serialize();

			console.log( dataSend );

			var userRoles = [];
			$('#user_roles option:selected').each(function(){ userRoles.push($(this).val()); });

			if ( userRoles.length <= 0 )
			{
				$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>กรุณาเลือก Roles</i>",
							color : "#C46A69",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				return;
			}

			dataSend += ( '&user_roles=' + userRoles.join(',') );

			console.log( dataSend );


			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: '{{ URL::to('admin/user/create') }}',
				type: 'POST',
				success: function(data)
				{
					$(this).prop('disabled', '');

					console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>เรียบร้อย</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						$('#createDataModal').modal('hide');

						otable.ajax.reload();
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

					}

				},
				error: function(a,b,c)
				{
					$(this).prop('disabled', '');

					console.log(a);

					$.smallBox({
							title : "Failed update data",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		});


	};
	// end pagefunction
	pagefunction();


}

	{{ Request::ajax() ? 'pageJavascriptTrigger()' : '' }}
</script>
<style>
.modal-dialog {
  position: relative;
  width: 90% !important;
  margin: 30px auto !important;
}

.modal, modal-backdrop{
    height: 100vh;
}

@media (min-width:620px)
{
	.modal-dialog
	{
		width: 600px !important;
	}
}
</style>
