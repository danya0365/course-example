
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">



		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข {{ $userInfo->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">


						<form class="smart-form" id="smart-form">
							<input type="hidden" name="id" value="{{ $userInfo->id }}" />
							<fieldset>
								<input type="hidden" name="current_retailer_id" value="{{ $userInfo->current_retailer_id }}">
								<div class="row">
									<section class="col col-6">
										<label  class="label">
											User Name
										</label>
										<label class="input">
											<input type="text" name="user_name" value="{{ $userInfo->user_name }}" disabled="" />
										</label>
									</section>
									<section class="col col-6">
										<label  class="label">
											Nick Name
										</label>
										<label class="input">
											<i class="icon-prepend fa fa-envelope-o"></i>
											<input type="text" name="nick_name"  value="{{ $userInfo->nick_name }}" />
										</label>
									</section>

									<section class="col col-6">
										<label  class="label" >
											ชื่อจริง
										</label>
										<label class="input">

											<input  name="first_name" type="text" value="{{ $userInfo->first_name }}">

										</label>
									</section>
									<section class="col col-6">
										<label  class="label">
											นามสกุล
										</label>
										<label class="input">

											<input  name="last_name" type="text" value="{{ $userInfo->last_name }}">

										</label>
									</section>
									<section class="col col-6">
										<label  class="label" for="birthday">
											วันเกิด
										</label>
										<label class="input">
											<i class="icon-prepend fa fa-calendar"></i>
											<input  name="birthday" type="text" value="{{ $userInfo->birthday }}">

										</label>
									</section>

								</div>

							</fieldset>
							<fieldset>
								<div class="row">

									<section class="col col-6">
										<label class="label" for="photo">
											Password (ถ้าไม่ต้องการเปลี่ยน ให้ปล่อยว่าง)
										</label>
										<label class="input">
											<input type="text" name="password" id="password" value="" />
										</label>
									</section>



									<section class="col col-6">
										<label class="toggle">
										<input type="checkbox" value="1" name="is_confirmed_email"  {{ $userInfo->is_confirmed_email == '1' ? 'checked="checked"' : '' }}>
										<i data-swchon-text="YES" data-swchoff-text="NO"></i>Is Approved Account</label>
									</section>


									<section class="col col-6">
										<label  class="label" for="user_roles">
											User Roles
										</label>

										<label class="select select-multiple">
											<select class="custom-scroll" name="user_roles" id="user_roles" multiple="">
												@foreach ( $roleList AS $role)

												<option value="{{ $role->id }}"  {{ in_array($role->id,  $userRoles)  ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
												@endforeach
											</select>
											<div class="note">
												<strong>Note:</strong> สามารถเลือกได้หลายตำแหน่ง
											</div>
										</label>



									</section>

								</div>
							</fieldset>
							<div class="form-actions">


								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>

								<button type="button" class="btn btn-default btn-lg pull-left" id="delete-user">
									<i class="fa fa-trash-o"></i>

									Delete
								</button>
							</div>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	<!-- row end -->
	</div>
</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/
		var currentYear = new Date().getFullYear();

		$('#delete-user').click(function(e)
		{
			e.preventDefault();

			var dataSend = 'id={{ $userInfo->id }}';

			showConfirmDelete(dataSend, '{{ URL::to('admin/user/delete_user') }}', function(response)
			{
				if ( response['status'] == 1 ) {

					window.history.back();
				}

			});
		})


		$("[name=birthday]").datepicker({
			maxDate: (currentYear-10) + '-12-31',
		    defaultDate: "now",
		    changeMonth: true,
		    changeYear: true,
		    numberOfMonths: 1,
		    dateFormat: 'yy-mm-dd',
		    prevText: '<i class="fa fa-chevron-left"></i>',
		    nextText: '<i class="fa fa-chevron-right"></i>',
		    onClose: function (selectedDate) {
		        //$("#startDate").datepicker("option", "maxDate", selectedDate);

				console.log(selectedDate);
		    }
		});

		$('#smart-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();
			var userRoles = [];
			$('#user_roles option:selected').each(function(){ userRoles.push($(this).val()); });

			if ( userRoles.length <= 0 )
			{
				$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>กรุณาเลือก Roles</i>",
							color : "#C46A69",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				return;
			}

			dataSend += ( '&user_roles=' + userRoles.join(',') );

			console.log( dataSend );

			ajaxPost(dataSend, '{{ Request::url() }}', function()
			{
				console.log('Updated success');

			});

		});

	};
	// end pagefunction
	pagefunction();

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
