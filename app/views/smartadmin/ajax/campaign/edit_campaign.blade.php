
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">




		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข Campaign</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">


						<form class="smart-form" id="edit-campaign-form">
							<input type="hidden" name="campaign_id" value="{{ $campaign['id'] }}" />
							<fieldset>

									<section>
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name"  value="{{ $campaign['name'] }}" />
										</label>
									</section>

									<section>
										<label  class="label" for="detail">
											Detail
										</label>

										<label class="textarea textarea-resizable">
											<textarea rows="3" class="custom-scroll" name="detail">{{ isset($campaign['detail']) ? $campaign['detail'] : '' }}</textarea>
										</label>
									</section>

									<section>
										<label  class="label" for="quarter_number">
											ไตรมาสที่
										</label>
										<div class="inline-group">
											<label class="radio">
												<input type="radio" value="1" name="quarter_number" {{ $campaign['quarter_detail']['number'] == 1 ? 'checked="checked"' : '' }}>
												<i></i> 1</label>
											<label class="radio">
												<input type="radio" value="2" name="quarter_number" {{ $campaign['quarter_detail']['number'] == 2 ? 'checked="checked"' : '' }}>
												<i></i> 2</label>
											<label class="radio">
												<input type="radio" value="3" name="quarter_number" {{ $campaign['quarter_detail']['number'] == 3 ? 'checked="checked"' : '' }}>
												<i></i> 3</label>
											<label class="radio">
												<input type="radio" value="4" name="quarter_number" {{ $campaign['quarter_detail']['number'] == 4 ? 'checked="checked"' : '' }}>
												<i></i> 4</label>

										</div>
									</section>

									<section>
										<label  class="label" for="quarter_year">
											ไตรมาสปี
										</label>
										<label class="input">
											<input type="text" name="quarter_year"  value="{{ $campaign['quarter_detail']['year'] }}" />
										</label>
									</section>


							</fieldset>

							<footer>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Create
								</button>
							</footer>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->






		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="add-edit-promotion-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข Promotion ของ Campaign [{{ $campaign['name'] }}]</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">


						<form class="smart-form" id="add-edit-promotion-form">
							<input type="hidden" name="id" value="{{ $promotion['id'] }}" />
							<input type="hidden" name="campaign_id" value="{{ $campaign['id'] }}" />
							<fieldset>

									<section>
										<label  class="label" for="photo">
											Photo
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 400px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url({{ isset($promotion['photo']) ? $promotion['photo'] : '' }});" id="photo_view"></div>

						                         <div class="dropzone" id="promotion_mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
										<label class="input">
											<input type="hidden" name="photo" id="photo" value="{{ isset($promotion['photo']) ? $promotion['photo'] : '' }}" />
										</label>
									</section>
									<section style="display: none">
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name" id="name" value="{{ $promotion['name'] }}" />
										</label>
									</section>

									<section>
										<label  class="label" for="photo">
											Uploads PDF File
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 400px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url();" id="pdf_preview"></div>

						                         <div class="dropzone" id="pdf_mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
									</section>

									<section >
										<label  class="label" for="detail">
											Detail
										</label>
										<label  class="label">
											<div class="detail_editor">{{ $promotion['detail'] }}</div>
										</label>

									</section>

									<section style="display: none">
										<label  class="label" for="display_order">
											Display Order
										</label>
										<label class="input">
											<input type="text" name="display_order" value="{{ $promotion['display_order'] }}" />
										</label>
									</section>

							</fieldset>

							<div class="form-actions">

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</div>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->




		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-campaign-point-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Campaign แต้มสะสม</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form class="smart-form" id="add-user-campaign-point-form">
							<input type="hidden" name="campaign_id" value="{{ $campaign['id'] }}" />
							<fieldset>

								<input type="hidden" name="user_id" value="0">
								<section>
									<label class="label" for="user_code_autocomplete">
									รหัสร้านค้า
									</label>

									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>

										<input type="text" name="user_code_autocomplete">

										<b class="tooltip tooltip-top-right">
											<i class="fa fa-warning txt-color-teal"></i>
												ลบข้อมูลช่องนี้ แล้วเคาะ Space Bar 1 ครั้ง เพื่อแสดงรายชื่อร้านค้าทั้งหมด</b>
									</label>
								</section>
								<section>
									<label class="label" for="point">
									Point
									</label>

									<label class="input">

										<input  name="point" type="text" >

									</label>
								</section>
							</fieldset>

							<footer>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Add
								</button>
							</footer>
						</form>



						<form class="smart-form" id="search-user-campaign-point-form">
							<fieldset>

								<input type="hidden" name="user_id" value="0">
								<section>
									<label class="label" for="user_code_autocomplete">
									รหัสร้านค้า
									</label>

									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>

										<input type="text" name="user_code_autocomplete">

										<b class="tooltip tooltip-top-right">
											<i class="fa fa-warning txt-color-teal"></i>
												ลบข้อมูลช่องนี้ แล้วเคาะ Space Bar 1 ครั้ง เพื่อแสดงรายชื่อร้านค้าทั้งหมด</b>
									</label>
								</section>


							</fieldset>
							<footer >
								<div class="pull-left">
									<button type="submit" class="btn btn-primary" >ค้นหา</button>
								</div>

							</footer>
						</form>

						<table id="user-campaign-point-datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 30%" class="nosort">User</th>
									<th style="width: 20%">Point</th>
									<th style="width: 10%" class="nosort">Updated by</th>
									<th style="width: 10%">Updated at</th>
									<th style="width: 10%">X</th>
								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->





		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="campaign-point-reward-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Trip ของรางวัลสำหรับ แต้มสะสม</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form class="smart-form" id="add-campaign-point-reward-form">
							<input type="hidden" name="campaign_id" value="{{ $campaign['id'] }}" />
							<fieldset>
								<section>
									<label class="label" for="name">
									ชื่อของรางวัล
									</label>

									<label class="input">

										<input  name="name" type="text" >

									</label>
								</section>

								<section>
									<label class="label" for="required_point">
									แต้มที่ใช้
									</label>

									<label class="input">

										<input  name="required_point" type="text" >

									</label>
								</section>
							</fieldset>

							<footer>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Add
								</button>
							</footer>
						</form>

						<table id="campaign-point-reward-datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 30%" class="nosort">Name</th>
									<th style="width: 20%">Point</th>
									<th style="width: 10%" class="nosort">Updated by</th>
									<th style="width: 10%">Updated at</th>
									<th style="width: 10%">X</th>
								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->






		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-campaign-summary-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Campaign สรุปยอด</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form class="smart-form" id="add-user-campaign-summary-form">
							<input type="hidden" name="campaign_id" value="{{ $campaign['id'] }}" />
							<fieldset>

								<input type="hidden" name="user_id" value="0">
								<section>
									<label class="label" for="user_code_autocomplete">
									รหัสร้านค้า
									</label>

									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>

										<input type="text" name="user_code_autocomplete">

										<b class="tooltip tooltip-top-right">
											<i class="fa fa-warning txt-color-teal"></i>
												ลบข้อมูลช่องนี้ แล้วเคาะ Space Bar 1 ครั้ง เพื่อแสดงรายชื่อร้านค้าทั้งหมด</b>
									</label>
								</section>
								<section>
									<label class="label" for="name">
									Name
									</label>

									<label class="input">

										<input  name="name" type="text" >

									</label>
								</section>
								<section>
									<label class="label" for="point">
									Detail
									</label>

									<label class="textarea textarea-resizable">
										<textarea rows="3" class="custom-scroll" name="detail"></textarea>
									</label>
								</section>

								<section>
									<label  class="label" for="summary_date">
										Date
									</label>
									<label class="input">
										<i class="icon-prepend fa fa-calendar"></i>
										<input name="summary_date" type="text" value="">

									</label>
								</section>

								<section >
									<label  class="label" for="paid_time">
										Time
									</label>
									<label class="input">

										<input name="summary_time" type="text" value="00:00:00">

									</label>
								</section>

								<section>
									<label class="label" for="status">Status</label>
									<label class="select">
										<select name="status">
											<option value="0" >รอประมวลผล</option>
											<option value="1" >เสร็จสิ้น</option>
											<option value="-1">ปฎิเสธ</option>
										</select> </label>
								</section>

								<section>
									<label class="label" for="status_text">
									Status ที่แสดงในแอพ MAM (สามารถแก้ไขข้อความเองได้)
									</label>

									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>

										<input type="text" name="status_text">

										<b class="tooltip tooltip-top-right">
											<i class="fa fa-warning txt-color-teal"></i>
												ลบข้อมูลช่องนี้ แล้วเคาะ Space Bar 1 ครั้ง เพื่อแสดงรายละเอียด</b>
									</label>

								</section>


							</fieldset>

							<footer>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Add
								</button>
							</footer>
						</form>



						<form class="smart-form" id="search-user-campaign-summary-form">
							<fieldset>

								<input type="hidden" name="user_id" value="0">
								<section>
									<label class="label" for="user_code_autocomplete">
									รหัสร้านค้า
									</label>

									<label class="input">
										<i class="icon-append fa fa-question-circle"></i>

										<input type="text" name="user_code_autocomplete">

										<b class="tooltip tooltip-top-right">
											<i class="fa fa-warning txt-color-teal"></i>
												ลบข้อมูลช่องนี้ แล้วเคาะ Space Bar 1 ครั้ง เพื่อแสดงรายชื่อร้านค้าทั้งหมด</b>
									</label>
								</section>


							</fieldset>
							<footer >
								<div class="pull-left">
									<button type="submit" class="btn btn-primary" >ค้นหา</button>
								</div>

							</footer>
						</form>

						<table id="user-campaign-summary-datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 20%" class="nosort">User</th>
									<th style="width: 30%">Detail</th>
									<th style="width: 10%" class="nosort">Updated by</th>
									<th style="width: 10%">Updated at</th>
									<th style="width: 10%">X</th>
								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->


	</div>

	<!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */


var userCampaignPointTable, userCampaignSummaryTable, campaignPointRewardTable;
var campaignId = {{ $campaign['id'] }};

function xEditable()
{

	$('.edit_point_reward').each(function(e)
	{
		var row = $(this).parent().parent();

		var dataRow = campaignPointRewardTable.row( row ).data();

		$(this).editable({
			ajaxOptions: {
			    type: 'POST',
			    dataType: 'json'
			},
			url: '{{ URL::to('admin/point_reward/edit_point_reward') }}',
	        type: 'text',
	        pk: 1,
	        name: 'required_point',
	        title: 'Enter Point',
	        onblur: 'submit',
	        params: function(params) {

			    return {id: dataRow.id, required_point: params.value};
			},
			validate: function(value) {
			    if($.trim(value) == '') {
			        return 'This field is required';
			    }
			},
			success: function(response, newValue) {

				if ( response.status == '1' )
				{
					$.smallBox({
								title : "สำเร็จ",
								content : "<i class='fa fa-clock-o'></i> <i>อัพเดตเรียบร้อย</i>",
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 3000
							});

					//otable.ajax.reload();
				}
				else

				{
					$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>" + response.message + "</i>",
								color : "#C46A69",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});

					//otable.ajax.reload();
				}
			},
			error: function(response, newValue) {

				$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>ไม่สามารถเชื่อมต่อ Server</i>",
								color : "#C46A69",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});


			    if(response.status === 500) {
			        return 'Service unavailable. Please try later.';
			    } else {
			        return response.responseText;
			    }


			}
	    });
	});



	$('.edit_point').each(function(e)
	{
		var row = $(this).parent().parent();

		var dataRow = userCampaignPointTable.row( row ).data();

		$(this).editable({
			ajaxOptions: {
			    type: 'POST',
			    dataType: 'json'
			},
			url: '{{ URL::to('admin/user_campaign_point/edit') }}',
	        type: 'text',
	        pk: 1,
	        name: 'point',
	        title: 'Enter Point',
	        onblur: 'submit',
	        params: function(params) {
			    //originally params contain pk, name and value
			    return {id: dataRow.id, point: params.value};
			},
			validate: function(value) {
			    if($.trim(value) == '') {
			        return 'This field is required';
			    }
			},
			success: function(response, newValue) {

				if ( response.status == '1' )
				{
					$.smallBox({
								title : "สำเร็จ",
								content : "<i class='fa fa-clock-o'></i> <i>อัพเดตเรียบร้อย</i>",
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 3000
							});

					//otable.ajax.reload();
				}
				else

				{
					$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>" + response.message + "</i>",
								color : "#C46A69",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});

					//otable.ajax.reload();
				}
			},
			error: function(response, newValue) {

				$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>ไม่สามารถเชื่อมต่อ Server</i>",
								color : "#C46A69",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});


			    if(response.status === 500) {
			        return 'Service unavailable. Please try later.';
			    } else {
			        return response.responseText;
			    }


			}
	    });
	});

}



function deleteUserCampaignPointData(id)
{
	$.SmartMessageBox({
		title : "ยืนยันลบข้อมูล",
		content : "คุณต้องการลบข้อมูลไช่ไหม",
		buttons : "[ยกเลิก][ยืนยัน]",
	}, function(buttonPress, targetPlayer) {

		if ( buttonPress == 'ยืนยัน' )
		{
			var dataSend = 'id=' + id;

			ajaxPost(dataSend, '{{ URL::to('admin/user_campaign_point/delete') }}', function()
			{
				console.log('Delete success');

				userCampaignPointTable.ajax.reload();
			});
		}

	});
}

function deleteUserCampaignSummaryData(id)
{
	$.SmartMessageBox({
		title : "ยืนยันลบข้อมูล",
		content : "คุณต้องการลบข้อมูลไช่ไหม",
		buttons : "[ยกเลิก][ยืนยัน]",
	}, function(buttonPress, targetPlayer) {

		if ( buttonPress == 'ยืนยัน' )
		{
			var dataSend = 'id=' + id;

			ajaxPost(dataSend, '{{ URL::to('admin/user_campaign_summary/delete') }}', function()
			{
				console.log('Delete success');

				userCampaignSummaryTable.ajax.reload();
			});
		}

	});
}

function deleteCampaignPointRewardData(id)
{
	$.SmartMessageBox({
		title : "ยืนยันลบข้อมูล",
		content : "คุณต้องการลบข้อมูลไช่ไหม",
		buttons : "[ยกเลิก][ยืนยัน]",
	}, function(buttonPress, targetPlayer) {

		if ( buttonPress == 'ยืนยัน' )
		{
			var dataSend = 'id=' + id;

			ajaxPost(dataSend, '{{ URL::to('admin/point_reward/delete_point_reward') }}', function()
			{
				console.log('Delete success');

				tripPointRewardTable.ajax.reload();
			});
		}

	});
}

var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/
		var currentYear = new Date().getFullYear();


		$("[name=summary_date]").datepicker({
		    defaultDate: "now",
		    changeMonth: true,
		    changeYear: true,
		    numberOfMonths: 1,
		    dateFormat: 'yy-mm-dd',
		    prevText: '<i class="fa fa-chevron-left"></i>',
		    nextText: '<i class="fa fa-chevron-right"></i>',
		    onClose: function (selectedDate) {
		        //$("#startDate").datepicker("option", "maxDate", selectedDate);

				console.log(selectedDate);
		    }
		});

		$('#delete').click(function(e)
		{
			e.preventDefault();

			var dataSend = 'id={{ $campaign['id'] }}';

			showConfirmDelete(dataSend, '{{ URL::to('admin/campaign/delete_campaign') }}', function()
			{
				window.history.back();
			});
		})

		$('.detail_editor').summernote({
			focus : false,
			toolbar: [
			    ['style', ['style']],
			    ['font', ['bold', 'italic', 'underline', 'clear']],
			    ['fontname', ['fontname']],
				['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']],
			    ['table', ['table']],
			    ['insert', ['link', 'picture', 'hr']],
			    ['view', ['fullscreen', 'codeview']],
			    ['help', ['help']]
			],
			onImageUpload: function(files, editor, welEditable) {
                sendFileForSummernote(files[0], editor, welEditable);
            }
		});

		$('#edit-campaign-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();


			ajaxPost(dataSend, '{{ Request::url() }}', function()
			{
				console.log('Updated success');

			});

		});

		$('#add-edit-promotion-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			dataSend += '&detail=' + encodeURIComponent( $('.detail_editor').code() );

			console.log( dataSend );

			ajaxPost(dataSend, '{{ URL::to('admin/promotion/edit/' . $promotion['id'] ) }}', function()
			{
				console.log('Updated success');

			});

		});



		attachDrobzone( $("#promotion_mydropzone") ,'{{ URL::to('uploads/image'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;
				var imageUrl = imageList['original'];
				$('#photo').val(imageUrl);
				$('#photo_view').css('background-image', 'url(' + imageUrl + ')');
			}
		});

		attachDrobzone( $("#pdf_mydropzone") ,'{{ URL::to('uploads/pdf'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;

				if ( imageList ) {

					var pdfList = imageList['images'];
					var imageUrl = imageList['images'][0];

					if ( imageUrl ) {

						$('#pdf_preview').css('background-image', 'url(' + imageUrl + ')');

						$('#photo').val(imageUrl);
						$('#photo_view').css('background-image', 'url(' + imageUrl + ')');

						var imageHtml = '';

						$.each(pdfList, function(k,v)
						{
							imageHtml += '<p><img src="' + v + '" style="max-width: 100%;"><br></p>';
						})

                        $('.detail_editor').code(imageHtml)

					}

				}

			}
		});



		$('[name=user_code_autocomplete]', '#search-user-campaign-point-form').autocomplete({
			source: function(request, response) {
				$.ajax({
					url: "{{ URL::to('json/users') }}",
					dataType: "json",
					type: "POST",
					data: {
						search: request.term
					},
					success: function(data){

						response( $.map( data, function( item ) {
							return {
								label: item.code,
								value: item.code,
								id: item.id,
							}
						}));
					 }
				})
			},
			select: function(event, ui) {

				//console.log(ui);
				$("[name=user_id]", '#search-user-campaign-point-form').val(ui.item.id);  // ui.item.value contains the id of the selected label
			}
		}).keyup(function()
		{
			if ( $(this).val() == "" ) {

				$("[name=user_id]", '#search-user-campaign-point-form').val('0');
			}
		});


		$('#search-user-campaign-point-form').on('submit', function(e)
		{
			e.preventDefault();
			userCampaignPointTable.ajax.reload();
		})



		$('[name=status_text]', '#add-user-campaign-summary-form').autocomplete({
			source: function(request, response) {

				var data = campaignPointRewardTable.rows().data();

				console.log(data);

				var returnData = $.map( data, function( item ) {
					return {
						label: item.name,
						value: item.name,
						id: item.id,
					}
				});

				response( returnData );

			},
			select: function(event, ui) {

				//console.log(ui);
				$("[name=status_text]", '#add-user-campaign-summary-form').val(ui.item.value);  // ui.item.value contains the id of the selected label
			}
		}).keyup(function()
		{

		});

		$('[name=user_code_autocomplete]', '#add-user-campaign-point-form').autocomplete({
			source: function(request, response) {
				$.ajax({
					url: "{{ URL::to('json/users') }}",
					dataType: "json",
					type: "POST",
					data: {
						search: request.term
					},
					success: function(data){

						response( $.map( data, function( item ) {
							return {
								label: item.code,
								value: item.code,
								id: item.id,
							}
						}));
					 }
				})
			},
			select: function(event, ui) {

				//console.log(ui);
				$("[name=user_id]", '#add-user-campaign-point-form').val(ui.item.id);  // ui.item.value contains the id of the selected label
			}
		}).keyup(function()
		{
			if ( $(this).val() == "" ) {

				$("[name=user_id]", '#add-user-campaign-point-form').val('0');
			}
		});

		$('#add-user-campaign-point-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			console.log( dataSend );

			ajaxPost(dataSend, '{{ URL::to('admin/user_campaign_point/create') }}', function()
			{
				console.log('Created success');
				userCampaignPointTable.ajax.reload();
			});

		});


	    userCampaignPointTable = $('#user-campaign-point-datatable').DataTable({
			"processing": true,
	        "serverSide": true,
			"ajax": {
	            "url": "{{ URL::to('admin/user_campaign_point/get_datas') }}",
	            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach
						dataPost.user_id = $('input[name=user_id]', '#search-user-campaign-point-form').val()
						dataPost.user_code = $('input[name=user_code_autocomplete]', '#search-user-campaign-point-form').val()
						dataPost.campaign_id = campaignId;
			       },
			       "dataSrc": function ( json ) {
		                //Make your callback here.
		                //alert("Done!");
		                //$('#country').val(json.country);
		                return json.data;
		            }
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();
				xEditable();
		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 0, "desc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
					    return $.number(data);
					},
					"type": "num"
		        },

				{
		        	"data": "user_id",
		        	"render": function ( data, type, full, meta ) {

						var title = '';

						if ( full.user ) {

							//console.log('User: ', full.user);
							title = full.user.code + ' / ' + (full.user.full_name == "" ? "-" : full.user.full_name);
						}

						if ( full.user_id == '0' ) {
							title = '-';
						}

					    return title;
					},
					"type": "string"
		        },

				{
		        	"data": "point",
		        	"render": function ( data, type, full, meta ) {
		        		return '<a href="#" class="edit_point" id="edit_point_' + full.id + '" data-type="text" data-pk="1" data-original-title="Enter point">' + full.point + '</a>';
					},
					"type": "string"
		        },

		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
						if ( full.updated_user ) {

							return full.updated_user.name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);
					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;

					},
					"type": "string"
		        },
				{
		        	"render": function ( data, type, full, meta ) {
					    return '<button onclick="deleteUserCampaignPointData(' + full.id + ')">X</a>';
					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });





		$('[name=user_code_autocomplete]', '#search-user-campaign-summary-form').autocomplete({
			source: function(request, response) {
				$.ajax({
					url: "{{ URL::to('json/users') }}",
					dataType: "json",
					type: "POST",
					data: {
						search: request.term
					},
					success: function(data){

						response( $.map( data, function( item ) {
							return {
								label: item.code,
								value: item.code,
								id: item.id,
							}
						}));
					 }
				})
			},
			select: function(event, ui) {

				//console.log(ui);
				$("[name=user_id]", '#search-user-campaign-summary-form').val(ui.item.id);  // ui.item.value contains the id of the selected label
			}
		}).keyup(function()
		{
			if ( $(this).val() == "" ) {

				$("[name=user_id]", '#search-user-campaign-summary-form').val('0');
			}
		});

		$('#search-user-campaign-summary-form').on('submit', function(e)
		{
			e.preventDefault();
			userCampaignSummaryTable.ajax.reload();
		})


		$('[name=user_code_autocomplete]', '#add-user-campaign-summary-form').autocomplete({
			source: function(request, response) {
				$.ajax({
					url: "{{ URL::to('json/users') }}",
					dataType: "json",
					type: "POST",
					data: {
						search: request.term
					},
					success: function(data){

						response( $.map( data, function( item ) {
							return {
								label: item.code,
								value: item.code,
								id: item.id,
							}
						}));
					 }
				})
			},
			select: function(event, ui) {

				//console.log(ui);
				$("[name=user_id]", '#add-user-campaign-summary-form').val(ui.item.id);  // ui.item.value contains the id of the selected label
			}
		}).keyup(function()
		{
			if ( $(this).val() == "" ) {

				$("[name=user_id]", '#add-user-campaign-summary-form').val('0');
			}
		});


		$('#add-user-campaign-summary-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			console.log( dataSend );

			ajaxPost(dataSend, '{{ URL::to('admin/user_campaign_summary/create') }}', function()
			{
				console.log('Created success');
				userCampaignSummaryTable.ajax.reload();
			});

		});

	    userCampaignSummaryTable = $('#user-campaign-summary-datatable').DataTable({
			"processing": true,
	        "serverSide": true,
			"ajax": {
	            "url": "{{ URL::to('admin/user_campaign_summary/get_datas') }}",
	            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach
						dataPost.user_id = $('input[name=user_id]', '#search-user-campaign-summary-form').val()
						dataPost.user_code = $('input[name=user_code_autocomplete]', '#search-user-campaign-summary-form').val()
						dataPost.campaign_id = campaignId;
			       },
			       "dataSrc": function ( json ) {
		                //Make your callback here.
		                //alert("Done!");
		                //$('#country').val(json.country);
		                return json.data;
		            }
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();
		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 0, "desc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
					    return $.number(data);
					},
					"type": "num"
		        },

				{
		        	"data": "user_id",
		        	"render": function ( data, type, full, meta ) {

						var title = '';

						if ( full.user ) {

							//console.log('User: ', full.user);
							title = full.user.code + ' / ' + (full.user.full_name == "" ? "-" : full.user.full_name);
						}

						if ( full.user_id == '0' ) {
							title = '-';
						}

					    return title;
					},
					"type": "string"
		        },

				{
		        	"data": "detail",
		        	"render": function ( data, type, full, meta ) {
						if ( typeof full.detail != 'undefined' ) {
							return full.detail;
						}
		        		return '-';
					},
					"type": "string"
		        },

		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
						if ( full.updated_user ) {

							return full.updated_user.name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);
					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;

					},
					"type": "string"
		        },
				{
		        	"render": function ( data, type, full, meta ) {
					    return '<button onclick="deleteUserCampaignSummaryData(' + full.id + ')">X</a>';
					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });




		$('#add-campaign-point-reward-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			console.log( dataSend );

			ajaxPost(dataSend, '{{ URL::to('admin/point_reward/create_point_reward') }}', function()
			{
				console.log('Created success');
				campaignPointRewardTable.ajax.reload();
			});

		});

	    campaignPointRewardTable = $('#campaign-point-reward-datatable').DataTable({
			"processing": true,
	        "serverSide": true,
			"ajax": {
	            "url": "{{ URL::to('admin/point_reward/all_point_rewards') }}",
	            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach
						dataPost.campaign_id = campaignId;
			       },
			       "dataSrc": function ( json ) {
		                //Make your callback here.
		                //alert("Done!");
		                //$('#country').val(json.country);
		                return json.data;
		            }
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();
				xEditable();
		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 2, "asc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
					    return $.number(data);
					},
					"type": "num"
		        },

				{
		        	"data": "name",
		        	"render": function ( data, type, full, meta ) {


					    return data;
					},
					"type": "string"
		        },

				{
		        	"data": "required_point",
		        	"render": function ( data, type, full, meta ) {
		        		return '<a href="#" class="edit_point_reward" id="edit_point_reward_' + full.id + '" data-type="text" data-pk="1" data-original-title="Enter point">' + full.required_point + '</a>';
					},
					"type": "string"
		        },

		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
						if ( full.updated_user ) {

							return full.updated_user.name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);
					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;

					},
					"type": "string"
		        },
				{
		        	"render": function ( data, type, full, meta ) {
					    return '<button onclick="deleteCampaignPointRewardData(' + full.id + ')">X</a>';
					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });


	};
	// end pagefunction
	pagefunction();

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
