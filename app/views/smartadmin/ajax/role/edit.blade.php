
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-6 col-md-4 col-lg-4">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Edit: {{ $roleInfo->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">


						<form class="smart-form" id="edit-detail-form">
							<input type="hidden" name="id" value="{{ $roleInfo->id }}" />
							<fieldset>
								<section >
									<label  class="label" for="name">
										Name
									</label>
									<label class="input">
										<input type="text" name="name" id="name" value="{{ $roleInfo->name }}" />
									</label>
								</section>

								<section >
									<label  class="label" for="is_admin">
										Is Guest
									</label>

									<label class="select">
										<select class="form-control" name="is_guest" id="is_guest" disabled="">
											<option value="1"  {{ $roleInfo->is_guest == '1' ? 'selected="selected"' : '' }}>YES</option>
											<option value="0"  {{ $roleInfo->is_guest == '0' ? 'selected="selected"' : '' }}>NO</option>
										</select>
										<i></i>
									</label>
								</section>


							</fieldset>
							<div class="form-actions">
								<button class="btn btn-default btn-lg pull-left" id="trash" style="display: none">
									<i class="fa fa-trash-o"></i>

									Trash
								</button>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</div>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->


		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-6 col-md-8 col-lg-8">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Permission: {{ $roleInfo->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">


						<form class="smart-form" id="edit-permission-form">
							<input type="hidden" name="id" value="{{ $roleInfo->id }}" />
							<fieldset>

								<div class="row">


									<section class="col col-6">
										<label  class="label" for="is_can_manage_user">
											is_can_manage_user
										</label>

										<label class="select">
											<select class="form-control" name="is_can_manage_user" id="is_can_manage_user" >
												<option value="1"  {{ $roleInfo->is_can_manage_user == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_manage_user == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>

									<section class="col col-6">
										<label  class="label" for="is_can_manage_role">
											is_can_manage_role
										</label>

										<label class="select">
											<select class="form-control" name="is_can_manage_role" id="is_can_manage_role" >
												<option value="1"  {{ $roleInfo->is_can_manage_role == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_manage_role == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>
									<section class="col col-6">
										<label  class="label" for="is_can_manage_setting">
											is_can_manage_setting
										</label>

										<label class="select">
											<select class="form-control" name="is_can_manage_setting" id="is_can_manage_setting" >
												<option value="1"  {{ $roleInfo->is_can_manage_setting == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_manage_setting == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>

									<section class="col col-6">
										<label  class="label" for="is_can_manage_device">
											is_can_update_profile
										</label>

										<label class="select">
											<select class="form-control" name="is_can_update_profile" id="is_can_update_profile" >
												<option value="1"  {{ $roleInfo->is_can_update_profile == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_update_profile == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>

									<section class="col col-6">
										<label  class="label" for="is_can_create_course">
											is_can_create_course
										</label>

										<label class="select">
											<select class="form-control" name="is_can_create_course" id="is_can_create_course" >
												<option value="1"  {{ $roleInfo->is_can_create_course == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_create_course == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>

									<section class="col col-6">
										<label  class="label" for="is_can_view_course">
											is_can_view_course
										</label>

										<label class="select">
											<select class="form-control" name="is_can_view_course" id="is_can_view_course" >
												<option value="1"  {{ $roleInfo->is_can_view_course == '1' ? 'selected="selected"' : '' }}>YES</option>
												<option value="0"  {{ $roleInfo->is_can_view_course == '0' ? 'selected="selected"' : '' }}>NO</option>
											</select>
											<i></i>
										</label>
									</section>



								</div>
							</fieldset>
							<div class="form-actions">
								<button class="btn btn-default btn-lg pull-left" id="trash" style="display: none">
									<i class="fa fa-trash-o"></i>

									Trash
								</button>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</div>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->


	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var otable;


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();


	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/


		$('#edit-permission-form, #edit-detail-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $('#edit-permission-form').serialize();
			dataSend += '&' + $('#edit-detail-form').serialize();

			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: '{{ Request::url() }}',
				type: 'POST',
				success: function(data)
				{
					console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>อัพเดตเรียบร้อย</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						$('#createWhoISModal').modal('hide');

						//otable.ajax.reload();
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

					}

				},
				error: function(a,b,c)
				{

					console.log(a);

					$.smallBox({
							title : "Failed update data",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		});

	};
	// end pagefunction
	pagefunction();


}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
