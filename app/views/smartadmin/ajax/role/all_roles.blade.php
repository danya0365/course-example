
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>All Roles</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 70%" class="nosort">Name</th>
									<th style="width: 10%">Updated by</th>
									<th style="width: 10%">Updated at</th>

								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->



<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var otable;


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();


	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/







		/* COLUMN FILTER  */
	    otable = $('#datatable').DataTable({
			"processing": false,
	        "serverSide": true,
	        "ajax": {
	            "url": "{{ Request::url() }}"
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();

		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					} ,

					"type": "num"
		        },
		        {
		        	"data": "name",
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/role/edit') }}' + '/' + full.id;
					    return '<a href="' + url + '" class="ajax">' + data + '</a>';
					    //return '<a href="#" class="whois_name_edit" id="whois_name_' + full.whois_id + '" data-type="text" data-pk="1" data-original-title="Enter whois name">' + full.whois_name + '</a>';
					} 	,
					"type": "string"
		        },
		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
						if ( full.updated_user ) {

							return full.updated_user.user_name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);
					} ,
					"type": "string"
		        },
		        {
		        	"data": "updated_at",
		        	"render": function ( data, type, full, meta ) {

						return full.updated_at_text;
						// X-Editable
						//if ( full.status == 0 )

					    	//return '<a href="#" class="status-selected" id="status_' + full.whois_id + '" data-type="select" data-pk="1" data-value="' + full.status + '" data-original-title="' + full.status_title + '"></a>';

					    //else
					    	//return ( full.status == 1 ? '<span class="txt-color-green">' + full.status_title + '</span>' : '<span class="txt-color-red">' + full.status_title + '</span>' )
					    // End X-Editable

					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });




	};
	// end pagefunction
	pagefunction();


}

	{{ Request::ajax() ? 'pageJavascriptTrigger()' : '' }}
</script>
<style>
.modal-dialog {
  position: relative;
  width: 90% !important;
  margin: 30px auto !important;
}

.modal, modal-backdrop{
    height: 100vh;
}

@media (min-width:620px)
{
	.modal-dialog
	{
		width: 600px !important;
	}
}

</style>
