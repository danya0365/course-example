
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">




		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข {{ $product->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">


						<form class="smart-form" id="smart-form">
							<input type="hidden" name="id" value="{{ $product->id }}" />
							<fieldset>


									<section>
										<label  class="label" for="photo">
											Photo
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 400px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url({{ $product->photo }});" id="photo_view"></div>

						                         <div class="dropzone" id="mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
										<label class="input">
											<input type="hidden" name="photo" id="photo" value="{{ $product->photo }}" />
										</label>
									</section>
									<section >
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name" id="name" value="{{ $product->name }}" />
										</label>
									</section>

									<section>
										<label  class="label" for="photo">
											Uploads PDF File
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 400px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url();" id="pdf_preview"></div>

						                         <div class="dropzone" id="pdf_mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
									</section>

									<section>
										<label  class="label" for="detail">
											Detail
										</label>
										<label  class="label">
											<div class="detail_editor">{{ $product->detail }}</div>
										</label>

									</section>


									<section>
										<label class="label" id="type_id">Type</label>
										<label class="select">
											<select name="type_id">
												<option value="0">-</option>
											@foreach ( $types AS $type )
												<option value="{{ $type->id }}" {{ $type->id == $product->type_id  ? 'selected="selected"' : '' }}>{{ $type->product_brand->name }} - {{ $type->name }}</option>
											@endforeach
											</select> </label>
									</section>

									<section >
										<label  class="label" for="display_order">
											Display Order
										</label>
										<label class="input">
											<input type="text" name="display_order" id="display_order" value="{{ $product->display_order }}" />
										</label>
									</section>


							</fieldset>

							<div class="form-actions">
								<button class="btn btn-default btn-lg pull-left" id="delete">
									<i class="fa fa-trash-o"></i>

									Delete
								</button>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</div>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->



	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/
		var currentYear = new Date().getFullYear();

		$('#delete').click(function(e)
		{
			e.preventDefault();

			var dataSend = 'id={{ $product->id }}';

			showConfirmDelete(dataSend, '{{ URL::to('admin/product/delete_product') }}', function()
			{
				window.history.back();
			});
		})


		$('#smart-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			dataSend += '&detail=' + encodeURIComponent( $('.detail_editor').code() );

			console.log( dataSend );

			ajaxPost(dataSend, '{{ Request::url() }}', function()
			{
				console.log('Updated success');

			});

		});

		$('.detail_editor').summernote({
			focus : false,
			toolbar: [
			    ['style', ['style']],
			    ['font', ['bold', 'italic', 'underline', 'clear']],
			    ['fontname', ['fontname']],
				['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']],
			    ['table', ['table']],
			    ['insert', ['link', 'picture', 'hr']],
			    ['view', ['fullscreen', 'codeview']],
			    ['help', ['help']]
			],
			onImageUpload: function(files, editor, welEditable) {
                sendFileForSummernote(files[0], editor, welEditable);
            }
		});


		attachDrobzone( $("#mydropzone") ,'{{ URL::to('uploads/image'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;
				var imageUrl = imageList['original'];
				$('#photo').val(imageUrl);
				$('#photo_view').css('background-image', 'url(' + imageUrl + ')');
			}
		});

		attachDrobzone( $("#pdf_mydropzone") ,'{{ URL::to('uploads/pdf'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;

				if ( imageList ) {

					var pdfList = imageList['images'];
					var imageUrl = imageList['images'][0];

					if ( imageUrl ) {

						$('#pdf_preview').css('background-image', 'url(' + imageUrl + ')');

						var imageHtml = '';

						$.each(pdfList, function(k,v)
						{
							imageHtml += '<p><img src="' + v + '" style="max-width: 100%;"><br></p>';
						})

                        $('.detail_editor').code(imageHtml)

					}

				}

			}
		});

		//$('.detail_editor').code('<p>test</p>');

	};
	// end pagefunction
	pagefunction();

	//loadScript('{{ URL::asset('assets/smartadmin/js/plugin/morris/morris.min.js')}}', pagefunction);

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
