
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">




		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข {{ $productType->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">


						<form class="smart-form" id="smart-form">
							<input type="hidden" name="id" value="{{ $productType->id }}" />
							<fieldset>


									<section>
										<label  class="label" for="photo">
											Photo
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 150px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url({{ $productType->photo }});" id="photo_view"></div>

						                         <div class="dropzone" id="mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
										<label class="input">
											<input type="hidden" name="photo" id="photo" value="{{ $productType->photo }}" />
										</label>
									</section>
									<section >
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name" id="name" value="{{ $productType->name }}" />
										</label>
									</section>

									<section>
										<label class="label" id="brand_id">Brand</label>
										<label class="select">
											<select name="brand_id">
												<option value="0">-</option>
											@foreach ( $brands AS $brand )
												<option value="{{ $brand->id }}" {{ $brand->id == $productType->brand_id  ? 'selected="selected"' : '' }}>{{ $brand->name }}</option>
											@endforeach
											</select> </label>
									</section>

									<section >
										<label  class="label" for="display_order">
											Display Order
										</label>
										<label class="input">
											<input type="text" name="display_order" id="display_order" value="{{ $productType->display_order }}" />
										</label>
									</section>


							</fieldset>

							<footer>
								<button class="btn btn-default btn-lg pull-left" id="delete">
									<i class="fa fa-trash-o"></i>

									Delete
								</button>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</footer>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->



	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/
		var currentYear = new Date().getFullYear();

		$('#delete').click(function(e)
		{
			e.preventDefault();

			var dataSend = 'id={{ $productType->id }}';

			showConfirmDelete(dataSend, '{{ URL::to('admin/product/delete_type') }}', function()
			{
				window.history.back();
			});
		})


		$('#smart-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();
			console.log( dataSend );

			ajaxPost(dataSend, '{{ Request::url() }}', function()
			{
				console.log('Updated success');

			});

		});



		attachDrobzone( $("#mydropzone") ,'{{ URL::to('uploads/image'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;
				var imageUrl = imageList['original'];
				$('#photo').val(imageUrl);
				$('#photo_view').css('background-image', 'url(' + imageUrl + ')');
			}
		});



	};
	// end pagefunction
	//pagefunction();

	loadScript('{{ URL::asset('assets/smartadmin/js/plugin/morris/morris.min.js')}}', pagefunction);

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
