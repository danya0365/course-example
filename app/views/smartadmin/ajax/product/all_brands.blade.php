
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>All Product Brands</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 30%" class="nosort">Name</th>
									<th style="width: 10%">Updated by</th>
									<th style="width: 10%">Updated at</th>
									<th style="width: 20%">Display Order</th>
									<th style="width: 20%">Types</th>
								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->

	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->



<a style="display: none" data-toggle="modal" data-target="#createDataModal" id="createDataModal-button">Click me</a>

<!-- Modal -->
<div class="modal fade" id="createDataModal" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">


			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="createDataModalLabel">สร้างใหม่</h4>
			</div>
			<div class="modal-body">




<form id="createData-form">

				<div class="row">
					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<label class="label dropzone-container" style="display: block; max-width: 150px !important; margin: 10px auto !important">
		                    <div style="margin-top: 100%;"></div>

		                         <div class="file_preview" style="" id="photo_view"></div>

		                         <div class="dropzone" id="mydropzone">
		                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
		                         </div>

				          </label>


							<input type="hidden" class="form-control" name="photo" id="photo" />

					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label for="name">
							Name
							</label>

							<input class="form-control" name="name" id="name" />

						</div>
					</div>

				</div>

</form>




			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					ยกเลิก
				</button>
				<button type="button" class="btn btn-primary" id="createData-submit">
					ยืนยัน
				</button>
			</div>



		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var otable;


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/







		/* COLUMN FILTER  */
	    otable = $('#datatable').DataTable({
			"processing": false,
	        "serverSide": true,
			"ajax": {
	            "url": "{{ Request::url() }}",
	            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach
			       },
			       "dataSrc": function ( json ) {
		                //Make your callback here.
		                //alert("Done!");
		                //$('#country').val(json.country);
		                return json.data;
		            }
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();

		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 3, "desc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					},
					"type": "num"
		        },
		        {
		        	"data": "name",
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/product/edit_brand') }}' + '/' + full.id;
					    return '<a href="' + url + '" class="ajax">' + ( data == '' ? '<i>Empty</i>' : data ) + '</a>';
					    //return '<a href="#" class="whois_name_edit" id="whois_name_' + full.whois_id + '" data-type="text" data-pk="1" data-original-title="Enter whois name">' + full.whois_name + '</a>';
					} ,
					"type": "string"
		        },


		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
						if ( full.updated_user ) {

							return full.updated_user.name;
						}

						if ( full.updated_by == '0' ) {
							return '-';
						}
						return $.number(data);
					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;
						// X-Editable
						//if ( full.status == 0 )

					    	//return '<a href="#" class="status-selected" id="status_' + full.whois_id + '" data-type="select" data-pk="1" data-value="' + full.status + '" data-original-title="' + full.status_title + '"></a>';

					    //else
					    	//return ( full.status == 1 ? '<span class="txt-color-green">' + full.status_title + '</span>' : '<span class="txt-color-red">' + full.status_title + '</span>' )
					    // End X-Editable

					},
					"type": "string"
		        },
				{
		        	"data": "display_order",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					},
					"type": "num"
		        },
				{
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/product/all_types') }}' + '?brand_id=' + full.id;
					    return '<a href="' + url + '" class="ajax">View</a>';
					},
					"type": "num"
		        }
			],

			"iDisplayLength": {{ $limit }}
	    });



		$('#createDataModal').on('show.bs.modal', function (e) {

				$("#name").val('');
				$("#photo").val('');
				$('#photo_view').css('background-image', 'url("")');
		});


		// custom toolbar
    	//$("div.toolbar").html('<div class="text-right"><button class="btn btn-primary" id="createData-button">เพิ่มข้อมูลใหม่</button></div>');

		$('#createData-button').click(function()
		{
			$('#createDataModal').modal('show');
		});


		$('#createData-submit').click(function(e)
		{
			$(this).prop('disabled');

			e.preventDefault();
			//alert('Submit');

			var dataSend = $('#createData-form').serialize();

			console.log( dataSend );

			if ( $('#name').val() == '' )
			{
				$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>กรุณาพิมพ์ Name</i>",
							color : "#C46A69",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				return;
			}

			console.log( dataSend );


			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: '{{ URL::to('admin/product/create_brand') }}',
				type: 'POST',
				success: function(data)
				{
					$(this).prop('disabled', '');

					console.log(data);

					if ( data.status == '1' )
					{
						$.smallBox({
							title : "สำเร็จ",
							content : "<i class='fa fa-clock-o'></i> <i>เรียบร้อย</i>",
							color : "#296191",
							iconSmall : "fa fa-thumbs-up bounce animated",
							timeout : 3000
						});

						$('#createDataModal').modal('hide');

						otable.ajax.reload();
					}
					else

					{
						$.smallBox({
							title : "ล้มเหลว",
							content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});

					}

				},
				error: function(a,b,c)
				{
					$(this).prop('disabled', '');

					console.log(a);

					$.smallBox({
							title : "Failed update data",
							content : "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
							color : "#c26565",
							iconSmall : "fa fa-thumbs-down bounce animated",
							timeout : 3000
						});
				}
			});
		});


		attachDrobzone( $("#mydropzone") ,'{{ URL::to('uploads/image'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;
				var imageUrl = imageList['original'];
				$('#photo').val(imageUrl);
				$('#photo_view').css('background-image', 'url(' + imageUrl + ')');
			}
		});


	};
	// end pagefunction
	pagefunction();


}

	{{ Request::ajax() ? 'pageJavascriptTrigger()' : '' }}
</script>
<style>
.modal-dialog {
  position: relative;
  width: 90% !important;
  margin: 30px auto !important;
}

.modal, modal-backdrop{
    height: 100vh;
}

@media (min-width:620px)
{
	.modal-dialog
	{
		width: 600px !important;
	}
}
</style>
