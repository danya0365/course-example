
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">




		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>แก้ไข {{ $productBrand->name }}</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">


						<form class="smart-form" id="smart-form">
							<input type="hidden" name="id" value="{{ $productBrand->id }}" />
							<fieldset>


									<section>
										<label  class="label" for="photo">
											Photo
										</label>
										<label class="label dropzone-container" style="display: block; max-width: 150px !important; margin: 10px auto !important">
						                    <div style="margin-top: 100%;"></div>

						                         <div class="file_preview" style="background-image: url({{ $productBrand->photo }});" id="photo_view"></div>

						                         <div class="dropzone" id="mydropzone">
						                             <a class="dz-message" data-dz-message><span>Browse file</span></a>
						                         </div>

								        </label>
										<label class="input">
											<input type="hidden" name="photo" id="photo" value="{{ $productBrand->photo }}" />
										</label>
									</section>
									<section >
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name" id="name" value="{{ $productBrand->name }}" />
										</label>
									</section>
									<section >
										<label  class="label" for="display_order">
											Display Order
										</label>
										<label class="input">
											<input type="text" name="display_order" id="display_order" value="{{ $productBrand->display_order }}" />
										</label>
									</section>


							</fieldset>

							<footer>
								<button class="btn btn-default btn-lg pull-left" id="delete">
									<i class="fa fa-trash-o"></i>

									Delete
								</button>

								<button class="btn btn-primary btn-lg" type="submit" >
									<i class="fa fa-save"></i>
									Update
								</button>
							</footer>
						</form>



					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->







		<!-- NEW WIDGET START -->
		<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-101" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>All Point Rewards for this Brand</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">



						<form class="smart-form" id="added-point-reward-form">
							<input type="hidden" name="brand_id" value="{{ $productBrand->id }}">
							<fieldset>

									<section >
										<label  class="label" for="name">
											Name
										</label>
										<label class="input">
											<input type="text" name="name"  value="" />
										</label>
									</section>

									<section >
										<label  class="label" for="required_point">
											Required Point
										</label>
										<label class="input">
											<input type="text" name="required_point"  value="0" />
										</label>
									</section>

									<section >
										<label  class="label" for="quarter_number">
											ไตรมาสที่
										</label>
										<div class="inline-group">
											<label class="radio">
												<input type="radio" value="1" name="quarter_number" {{ $currentQuarterNumber == 1 ? 'checked="checked"' : '' }}>
												<i></i> 1</label>
											<label class="radio">
												<input type="radio" value="2" name="quarter_number" {{ $currentQuarterNumber == 2 ? 'checked="checked"' : '' }}>
												<i></i> 2</label>
											<label class="radio">
												<input type="radio" value="3" name="quarter_number" {{ $currentQuarterNumber == 3 ? 'checked="checked"' : '' }}>
												<i></i> 3</label>
											<label class="radio">
												<input type="radio" value="4" name="quarter_number" {{ $currentQuarterNumber == 4 ? 'checked="checked"' : '' }}>
												<i></i> 4</label>

											<label class="radio">
												<input type="radio" value="0" name="quarter_number" {{ $currentQuarterNumber == 4 ? 'checked="checked"' : '' }}>
													<i></i> ไม่สนใจ</label>

										</div>
									</section>


									<section>
										<label  class="label" for="quarter_year">
											ไตรมาสปี
										</label>
										<label class="input">
											<input type="text" name="quarter_year"  value="{{ $currentQuarterYear }}" />
										</label>
									</section>



							</fieldset>

							<footer>


								<button class="btn btn-primary btn-lg" type="submit" >

									Create
								</button>
							</footer>
						</form>


						<form class="smart-form" id="search-point-reward-form">
							<fieldset>
								<section>
									<label  class="label" for="quarter_number">
										ไตรมาสที่
									</label>
									<div class="inline-group">
										<label class="radio">
											<input type="radio" value="1" name="quarter_number" {{ $currentQuarterNumber == 1 ? 'checked="checked"' : '' }}>
											<i></i> 1</label>
										<label class="radio">
											<input type="radio" value="2" name="quarter_number" {{ $currentQuarterNumber == 2 ? 'checked="checked"' : '' }}>
											<i></i> 2</label>
										<label class="radio">
											<input type="radio" value="3" name="quarter_number" {{ $currentQuarterNumber == 3 ? 'checked="checked"' : '' }}>
											<i></i> 3</label>
										<label class="radio">
											<input type="radio" value="4" name="quarter_number" {{ $currentQuarterNumber == 4 ? 'checked="checked"' : '' }}>
											<i></i> 4</label>

										<label class="radio">
											<input type="radio" value="0" name="quarter_number" {{ $currentQuarterNumber == 4 ? 'checked="checked"' : '' }}>
												<i></i> ไม่สนใจ</label>

									</div>
								</section>

								<section >
									<label  class="label" for="quarter_year">
										ไตรมาสปี
									</label>
									<label class="input">
										<input type="text" name="quarter_year" id="quarter_year" value="{{ $currentQuarterYear }}" />
									</label>
								</section>

							</fieldset>
							<footer>

								<button class="btn btn-primary btn-lg" type="submit">

									Search
								</button>
							</footer>

						</form>

						<table id="point-reward-datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 10%" >Id</th>
									<th style="width: 30%" class="nosort">Name</th>
									<th style="width: 20%" >Required Point</th>
									<th style="width: 20%" >Quarter</th>
									<th style="width: 10%">Updated by</th>
									<th style="width: 10%">Updated at</th>

								</tr>
							</thead>

						</table>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->


	</div>

	<!-- end row -->

	<!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */


var pointRewardTable;

var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];


	// pagefunction
	var pagefunction = function() {
		//console.log("cleared");

		/* // DOM Position key index //

			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class

			Also see: http://legacy.datatables.net/usage/features
		*/
		var currentYear = new Date().getFullYear();

		$('#search-point-reward-form').on('submit', function(e)
		{
			e.preventDefault();
			pointRewardTable.ajax.reload();
		})

		$('#added-point-reward-form').on('submit', function(e)
		{
			var obj = this;
			e.preventDefault();

			var dataSend = $(this).serialize();
			console.log( dataSend );

			ajaxPost(dataSend, '{{ URL::to('admin/point_reward/create_point_reward') }}', function()
			{
				$('[name=name]', obj).val('');

				pointRewardTable.ajax.reload();
			});
		})

		$('#delete').click(function(e)
		{
			e.preventDefault();

			var dataSend = 'id={{ $productBrand->id }}';

			showConfirmDelete(dataSend, '{{ URL::to('admin/product/delete_brand') }}', function()
			{
				window.history.back();
			});
		})


		$('#smart-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();
			console.log( dataSend );

			ajaxPost(dataSend, '{{ Request::url() }}', function()
			{
				console.log('Updated success');

			});

		});



		attachDrobzone( $("#mydropzone") ,'{{ URL::to('uploads/image'); }}', function(file, response)
		{
			if ( response.status == '1' )
			{
				var imageList = response.data;
				var imageUrl = imageList['original'];
				$('#photo').val(imageUrl);
				$('#photo_view').css('background-image', 'url(' + imageUrl + ')');
			}
		});



	    pointRewardTable = $('#point-reward-datatable').DataTable({
			"processing": false,
	        "serverSide": true,
			"ajax": {
	            "url": "{{ Request::url() }}",
	            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach

					  dataPost.getPointRewards = 1;
					  dataPost.quarter_number = $('input[name=quarter_number]:checked', '#search-point-reward-form').val()
					  dataPost.quarter_year = $('[name=quarter_year]', '#search-point-reward-form').val();
			       },
			       "dataSrc": function ( json ) {
		                //Make your callback here.
		                //alert("Done!");
		                //$('#country').val(json.country);
		                return json.data;
		            }
	            },
            "sDom": "<'dt-toolbar'<'col-xs-6'><'col-xs-6'<'toolbar'>>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

	        "drawCallback": function( settings ) {
		        pageSetUp();

		    },


		    'aoColumnDefs': [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],

			"order": [[ 3, "desc" ]],

		    "columns": [
		        {
		        	"data": "id",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					},
					"type": "num"
		        },
		        {
		        	"data": "name",
		        	"render": function ( data, type, full, meta ) {
		        		var url = '{{ URL::to('admin/point_reward/edit_point_reward') }}' + '/' + full.id;
					    return '<a href="' + url + '" class="ajax">' + ( data == '' ? '<i>Empty</i>' : data ) + '</a>';
					    //return '<a href="#" class="whois_name_edit" id="whois_name_' + full.whois_id + '" data-type="text" data-pk="1" data-original-title="Enter whois name">' + full.whois_name + '</a>';
					} ,
					"type": "string"
		        },

				{
		        	"data": "required_point",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
					    return $.number(data);
					},
					"type": "num"
		        },

				{
		        	"data": "quarter_title",
		        	"render": function ( data, type, full, meta ) {
					    return data;
					},
					"type": "string"
		        },

		        {
		        	"data": "updated_by",
		        	"render": function ( data, type, full, meta ) {
		        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
		        		var url = '#';
					    //return '<a href="' + url + '" class="ajax">' + data + '</a>';
					    return (data == '0' ? '-' : full.updated_user.name);
					},
					"type": "string"
		        },
		        {
		        	"data": "updated_at_text",
		        	"render": function ( data, type, full, meta ) {

						return data;
						// X-Editable
						//if ( full.status == 0 )

					    	//return '<a href="#" class="status-selected" id="status_' + full.whois_id + '" data-type="select" data-pk="1" data-value="' + full.status + '" data-original-title="' + full.status_title + '"></a>';

					    //else
					    	//return ( full.status == 1 ? '<span class="txt-color-green">' + full.status_title + '</span>' : '<span class="txt-color-red">' + full.status_title + '</span>' )
					    // End X-Editable

					},
					"type": "string"
		        },
			],

			"iDisplayLength": {{ $limit }}
	    });



	};
	// end pagefunction
	//pagefunction();

	loadScript('{{ URL::asset('assets/smartadmin/js/plugin/morris/morris.min.js')}}', pagefunction);

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
