
<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">
		<article class="col-sm-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>การตั้งค่า</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

							<form class="smart-form" id="setting-form">


							<fieldset>

							@foreach ( $settings AS $setting)
								@if ( ! in_array($setting->var, ['mam_trip_title', 'mam_trip_desc', 'serenade_title', 'serenade_desc']) )
								<section>
									<label class="label">
										{{ $setting->name }}
									</label>

									@if ( $setting->input_type == 'editor' )
									<label  class="label">
										<div class="{{ $setting->var }}">{{ $setting->value }}</div>
									</label>
									@else
									<label class="input">
										<input name="{{ $setting->var }}" id="{{ $setting->var }}" value="{{ $setting->value }}" />
									</label>

									@endif
								</section>
								@endif
							@endforeach
							</fieldset>


								<footer>
									<button type="submit" class="btn btn-primary">
										อัพเดท
									</button>
								</footer>


							</form>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->


		</article>

	</div>

	<!-- end row -->

</section>

<div class="upload-progress">
<div class="bar-holder">
	<div class="progress">
		<div class="progress-bar bg-color-teal" aria-valuetransitiongoal="25" aria-valuenow="25" style="width: 0;">0%</div>
	</div>
</div>
</div>

<!-- end widget grid -->

<style>
div.upload-progress
{
	position: fixed;
	top: 50%;
	left: 0;
	width: 100%;
	height: 50px;
	display: none;
	z-index: 99999;
}
.bar-holder
{
	margin: 0 auto;
	width: 90%;
}
</style>
<script type="text/javascript">
	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */


var pageJavascriptTrigger = function()
{
	page_title = '{{ $pageTitle }}';
	pageSetUp();


	/*
	 * PAGE RELATED SCRIPTS
	 */

	// pagefunction

	var pagefunction = function() {

		@foreach ( $settings AS $setting )
			@if ( $setting->input_type == 'editor' )
		$('.{{ $setting->var }}').summernote({
			focus : false,
			toolbar: [
			    ['style', ['style']],
			    ['font', ['bold', 'italic', 'underline', 'clear']],
			    ['fontname', ['fontname']],
				['fontsize', ['fontsize']],
			    ['color', ['color']],
			    ['para', ['ul', 'ol', 'paragraph']],
			    ['height', ['height']],
			    ['table', ['table']],
			    ['insert', ['link', 'picture', 'hr']],
			    ['view', ['fullscreen', 'codeview']],
			    ['help', ['help']]
			],
			onImageUpload: function(files, editor, welEditable) {
                sendFileForSummernote(files[0], editor, welEditable);
            }
		});
			@endif
		@endforeach
		// Fill all progress bars with animation
		$('.progress-bar').progressbar({
			display_text : 'fill'
		});


		$('#setting-form').on('submit', function(e)
		{
			e.preventDefault();

			var dataSend = $(this).serialize();

			@foreach ( $settings AS $setting )
				@if ( $setting->input_type == 'editor' )
			dataSend += '&{{ $setting->var }}=' + encodeURIComponent( $('.{{ $setting->var }}').code() );
				@endif
			@endforeach

			console.log(dataSend);

			$.ajax({
				dataType: 'json',
				data: dataSend,
				url: '{{ Request::url() }}',
				type: 'POST',
				success: function(data)
				{

					if ( data.status == '1' )
					{
						$.smallBox({
								title : "สำเร็จ",
								content : "<i class='fa fa-clock-o'></i> <i>อัพเดตข้อมูลเรียบร้อย</i>",
								color : "#296191",
								iconSmall : "fa fa-thumbs-up bounce animated",
								timeout : 3000
							});

						//$('a[href="{{ URL::to('admin/member/list') }}"]').click();
					}
					else
					{
						$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>" + data.message + "</i>",
								color : "#c26565",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});

					}


					/*
					setTimeout(function(){
						location.reload();
					}, 0.7);
					*/
				},
				error: function(a,b,c)
				{
					console.log(a);

					$.smallBox({
								title : "ล้มเหลว",
								content : "<i class='fa fa-clock-o'></i> <i>กรุณาลองอีกครั้ง</i>",
								color : "#c26565",
								iconSmall : "fa fa-thumbs-down bounce animated",
								timeout : 3000
							});
				}
			});
		});
	};

	loadScript("{{ URL::asset('admincptheme/js/plugin/bootstrap-progressbar/bootstrap-progressbar.min.js') }}", pagefunction);

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}

</script>
