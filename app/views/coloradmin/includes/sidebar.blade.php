		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<div class="image">
							<a href="javascript:;"><img src="{{ $app->user->photo ? $app->user->photo : 'assets/coloradmin/img/user-12.jpg' }}" alt="" /></a>
						</div>
						<div class="info">
							{{ $app->user->full_name ? $app->user->full_name : $app->user->name }}
							<small>{{ $app->user->role->name ? $app->user->role->name : '<i>None</i>' }}</small>
						</div>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">เมนู</li>
					<li>
						<a href="{{ URL::to('admin/index') }}" data-toggle="ajax" class="active">
						    
						    <i class="fa fa-laptop"></i>
						    <span>มุมมองโดยรวม</span>
					    </a>
					</li>
					<li>
						<a href="{{ URL::to('admin/user/lastactivity') }}" data-toggle="ajax">
						    
						    <i class="fa fa-user"></i>
						    <span>ความเคลื่อนไหวล่าสุด</span>
					    </a>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-dollar"></i>
						    <span>จัดการผู้ใช้งาน</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="{{ URL::to('admin/user/createstaff') }}" data-toggle="ajax">สร้าง Staff</a></li>
						    <li><a href="{{ URL::to('admin/user/createfranchise') }}" data-toggle="ajax">สร้าง Franchise</a></li>
						    <li><a href="{{ URL::to('admin/user/createguest') }}" data-toggle="ajax">สร้าง Guest</a></li>
						    <li><a href="{{ URL::to('admin/user/createadmin') }}" data-toggle="ajax">สร้าง Admin</a></li>
						    <li><a href="{{ URL::to('admin/user/liststaff') }}" data-toggle="ajax">Staff ทั้งหมด</a></li>
						    <li><a href="{{ URL::to('admin/user/listfranchise') }}" data-toggle="ajax">Franchise ทั้งหมด</a></li>
						    <li><a href="{{ URL::to('admin/user/listguest') }}" data-toggle="ajax">Guest ทั้งหมด</a></li>
						    <li><a href="{{ URL::to('admin/user/listadmin') }}" data-toggle="ajax">Admin ทั้งหมด</a></li>
						</ul>
					</li>
					
					<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-dollar"></i>
						    <span>จัดการสาขา</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="{{ URL::to('admin/branch/create') }}" data-toggle="ajax">สร้างสาขาใหม่</a></li>
						    <li><a href="{{ URL::to('admin/branch/list') }}" data-toggle="ajax">รายชื่อสาขาทั้งหมด</a></li>
						    <li><a href="{{ URL::to('admin/branch/stafflist') }}" data-toggle="ajax">แก้ไขสาขาของ Staff</a></li>
						</ul>
					</li>
					
					<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-dollar"></i>
						    <span>จัดการรหัสเซซชั่น</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="{{ URL::to('admin/sessioncode/create') }}" data-toggle="ajax">สร้างรหัสใหม่</a></li>
						    <li><a href="{{ URL::to('admin/sessioncode/activelist') }}" data-toggle="ajax">รหัสที่ยังไม่ถูกใช้งาน</a></li>
						    <li><a href="{{ URL::to('admin/sessioncode/usedlist') }}" data-toggle="ajax">รหัสที่ใช้งานแล้ว</a></li>
						    <li><a href="{{ URL::to('admin/sessioncode/expiredlist') }}" data-toggle="ajax">รหัสที่หมดอายุแล้ว</a></li>
						</ul>
					</li>

					<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="fa fa-gears"></i>
						    <span>ตั้งค่า</span>
					    </a>
						<ul class="sub-menu">
						    <li><a href="{{ URL::to('admin/setting/system') }}" data-toggle="ajax">ระบบ</a></li>
						    <li><a href="{{ URL::to('admin/setting/companybranch') }}" data-toggle="ajax">บริษัทและสาขา</a></li>
						    <li><a href="{{ URL::to('admin/setting/customer') }}" data-toggle="ajax">ลูกค้า</a></li>
						    <li><a href="{{ URL::to('admin/setting/goods') }}" data-toggle="ajax">สินค้า</a></li>
						    <li><a href="{{ URL::to('admin/setting/employee') }}" data-toggle="ajax">พนักงาน</a></li>
						    <li><a href="{{ URL::to('admin/setting/person') }}" data-toggle="ajax">บุคคล</a></li>
						    <li><a href="{{ URL::to('admin/setting/supplier') }}" data-toggle="ajax">ผู้จำหน่าย</a></li>
						    
						</ul>
					</li>
					@if ( $app->user->role->can_access_staffcp )
					<li>
						<a href="{{ URL::to('staff') }}">
						    
						    <i class="fa fa-laptop"></i>
						    <span>ไปยัง Staff Panel</span>
					    </a>
					</li>
					@endif
					@if ( $app->user->role->can_access_franchisecp )
					<li>
						<a href="{{ URL::to('franchise') }}">
						    
						    <i class="fa fa-laptop"></i>
						    <span>ไปยัง Franchise Panel</span>
					    </a>
					</li>
					@endif
					
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->