	<meta charset="utf-8" />
	<title id="page-title">{{ $page_title }} | Mainboard Service</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/css/animate.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/css/style.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/css/style-responsive.min.css') }}" rel="stylesheet" />
	<link href="{{ URL::asset('assets/coloradmin/css/theme/default.css') }}" rel="stylesheet" id="theme" />
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/coloradmin/plugins/pace/pace.min.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
	
<!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
<link href="{{ URL::asset('assets/coloradmin/plugins/gritter/css/jquery.gritter.css') }}" rel="stylesheet" />	
<link href="{{ URL::asset('assets/coloradmin/plugins/DataTables/css/data-table.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/ionRangeSlider/css/ion.rangeSlider.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/ionRangeSlider/css/ion.rangeSlider.skinNice.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/password-indicator/css/password-indicator.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-combobox/css/bootstrap-combobox.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/jquery-tag-it/css/jquery.tagit.css') }}" rel="stylesheet" />

<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap3-editable/css/bootstrap-editable.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap3-editable/inputs-ext/address/address.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap3-editable/inputs-ext/typeaheadjs/lib/typeahead.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datepicker/css/datepicker3.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datetimepicker/css/datetimepicker.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/select2/select2.css') }}" rel="stylesheet" />
<link href="{{ URL::asset('assets/coloradmin/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.css') }}" rel="stylesheet" />


<!-- ================== END PAGE LEVEL STYLE ================== -->