	<!-- ================== BEGIN BASE JS ================== -->
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery/jquery-1.9.1.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!--[if lt IE 9]>
		<script src="{{ URL::asset('assets/coloradmin/crossbrowserjs/html5shiv.js') }}"></script>
		<script src="{{ URL::asset('assets/coloradmin/crossbrowserjs/respond.min.js') }}"></script>
		<script src="{{ URL::asset('assets/coloradmin/crossbrowserjs/excanvas.min.js') }}"></script>
	<![endif]-->
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery-hashchange/jquery.hashchange.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery-cookie/jquery.cookie.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/gritter/js/jquery.gritter.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/DataTables/js/jquery.dataTables.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/DataTables/js/dataTables.fixedHeader.js') }}"></script>
	
	
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/masked-input/masked-input.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/password-indicator/js/password-indicator.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-combobox/js/bootstrap-combobox.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/jquery-tag-it/js/tag-it.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/select2/select2.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/mockjax/jquery.mockjax.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/moment/moment.min.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-wysihtml5/lib/js/wysihtml5-0.3.0.js') }}"></script>
	<script src="{{ URL::asset('assets/coloradmin/plugins/bootstrap-wysihtml5/src/bootstrap-wysihtml5.js') }}"></script>
	<!-- ================== END BASE JS ================== -->
	
	<!-- CUSTOM NOTIFICATION -->
	<script src="{{ URL::asset('assets/coloradmin/plugins/notification/SmartNotification.min.js') }}"></script>
	
	<!-- ================== BEGIN PAGE LEVEL JS ================== -->
	<script src="{{ URL::asset('assets/coloradmin/js/apps.js') }}"></script>
	
	
	<!-- ================== END PAGE LEVEL JS ================== -->
	
	<script>
		$(document).ready(function() {
			App.init();
			
			{{ ! Input::get('ajax') ? ' if ( typeof(ajaxReady) == "function"){ ajaxReady(); }' : '' }}
		});
		
	</script>