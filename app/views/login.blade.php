@extends('smartadmin.layouts.navbar')
@section('content')
<div class="row">
				@if($errors->has())
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="alert alert-danger alert-block" id="error-wrapper">
							<h4 class="alert-heading">Error!</h4>
							@foreach ($errors->all() as $error)
								<p>{{ $error }}</p>
							@endforeach
						</div>
					</div>
				@endif

				@if ( isset($success) ||  !empty($success) )
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
					<div class="alert alert-success alert-block" id="success-wrapper">
						{{ $success }}
					</div>
				</div>
				@endif

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="well no-padding" id="login-wrapper">
							<form action="{{ Request::url() }}" id="login-form" class="smart-form client-form" method="post">
								<header>
									Sign In
								</header>

								<fieldset>

									<section>
										<label class="label">User Name</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" name="user_name" value="Admin">
											</label>
									</section>



									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" name="password" value="Admin">
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<div class="note">
											<a href="{{ URL::to('request_new_password')}}">Forgot password?</a>
										</div>
									</section>

									<section>
										<label class="checkbox">
											<input type="checkbox" name="remember" checked="">
											<i></i>Stay signed in</label>
									</section>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>

						<!-- Removed Faceook Logged In

						<h5 class="text-center"> - Or sign in using -</h5>

							<ul class="list-inline text-center">
								<li>
									<a href="{{ URL::to('facebook/authen') }}" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
								</li>
							</ul>

						-->

					</div>
</div>
@stop
@section('script')
<script>
function pageJavascriptTrigger()
{
	$(function()
	{
		//$('body').append('<div id="background-stretch"></div>');
	})
}
</script>
@stop
@section('head')
<style>
#extr-page #main {
    background: transparent !important;
    margin: 0;
    height: 100vh;
}

#background-stretch {
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0px;
    top: 0px;
    z-index: -1; /* Ensure div tag stays behind content; -999 might work, too. */

	background-image: url({{ URL::to('assets/smartadmin/img/destop_background.jpg') }});
	background-position: center center;
	background-origin: content-box;
	background-size: cover;
}

#background-stretch>img {
    width:100%;
    height:100%;
}

#extr-page #main {
    background: transparent !important;
    margin: 0;
}
#login-wrapper, #error-wrapper, #success-wrapper
{
	margin: 15px auto !important;
	width: 370px;
}
@media screen and (max-width: 400px) {
    /* STYLES HERE */
	#login-wrapper, #error-wrapper, #success-wrapper
	{
		width: 100%;
	}
}
</style>
@stop
