

<!-- row -->
<div class="row">

	@if($errors->has())
	<div class="col-md-6 col-md-offset-3">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
		</div>
	</div>
	@endif

	@if ( isset($error) ||  !empty($error) )
	<div class="col-md-6 col-md-offset-3">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			<p>{{ $error }}</p>
		</div>
	</div>
	@endif

	@if ( isset($success) ||  !empty($success) )
	<div class="col-md-6 col-md-offset-3">
		<div class="alert alert-success alert-block" id="success-wrapper">
			{{ $success }}
		</div>
	</div>
	@endif


</div>



<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-md-6 col-md-offset-3">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>อัพเดตโปรไฟล์</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form class="smart-form" action="{{ Request::url() }}" method="post">
							<input type="hidden" name="id" value="{{ $user->id }}">
							<fieldset>

								<section>
									<label class="label">
									First Name
									</label>

									<label class="input">
										<input type="text" name="first_name" value="{{ $user->first_name }}">
									</label>
								</section>
								<section>
									<label class="label">
									Last Name
									</label>

									<label class="input">
										<input type="text" name="last_name" value="{{ $user->last_name }}">
									</label>
								</section>
								<section>
									<label class="label">
									Nick Name
									</label>

									<label class="input">
										<input type="text" name="nick_name" value="{{ $user->nick_name }}">
									</label>
								</section>
								<section>
									<label class="label">
									Birthday
									</label>

									<label class="input">
										<i class="icon-prepend fa fa-calendar"></i>
										<input type="text" name="birthday" value="{{ $user->birthday }}">
									</label>
								</section>

							</fieldset>
							<footer>
								<div class="pull-right">
									<button type="submit" class="btn btn-primary" >อัพเดต</button>
								</div>
							</footer>
						</form>


					</div>
				</div>
			</div>
		</article>
	</div>
</section>




<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */
var pageJavascriptTrigger = function()
{
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	var currentYear = new Date().getFullYear();

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];

	$("[name=birthday]").datepicker({
		maxDate: (currentYear-10) + '-12-31',
		defaultDate: "now",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onClose: function (selectedDate) {
			//$("#startDate").datepicker("option", "maxDate", selectedDate);

			console.log(selectedDate);
		}
	});


	$('.smart-form').on('submit', function(e)
	{
		e.preventDefault();

		var data = $(this).serialize();

		ajaxPost(data, '{{ Request::url() }}', function()
		{
			console.log('Updated success');

		});
	})

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
