


<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-md-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="user-edit-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false">
				<!-- widget options:
				usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

				data-widget-colorbutton="false"
				data-widget-editbutton="false"
				data-widget-togglebutton="false"
				data-widget-deletebutton="false"
				data-widget-fullscreenbutton="false"
				data-widget-custombutton="false"
				data-widget-collapsed="true"
				data-widget-sortable="false"

				-->
				<header>
					<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>รายการคอร์สเรียน</h2>

				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body no-padding">

						<form class="smart-form" id="search-form">
							<fieldset>

								<input type="hidden" name="brand_id">
								<section>
									<label class="label" >
									Name
									</label>

									<label class="input">
										<input type="text" name="name">
									</label>
								</section>
								<section>
									<label class="label" >
									ช่วงเวลา
									</label>

									<label class="input">
										<i class="icon-prepend fa fa-clock-o"></i>
										<input type="text" name="time_range" class="time-picker">
									</label>
								</section>


							</fieldset>
							<footer >
								<div class="pull-right">
									<button type="submit" class="btn btn-primary" >ค้นหา</button>
								</div>

							</footer>
						</form>

						<table id="datatable" class="table table-striped table-bordered table-hover" width="100%">
							<thead>

								<tr>

									<th style="width: 5%" >Id</th>
									<th style="width: 10%" class="nosort">Name</th>
									<th style="width: 10%" class="nosort">Subject</th>
									<th style="width: 15%" class="nosort">Start at</th>
									<th style="width: 15%" class="nosort">End at</th>
									<th style="width: 5%" class="nosort">Number of student</th>
									<th style="width: 10%" class="nosort">Category</th>
									<th style="width: 10%">Updated by</th>
									<th style="width: 10%">Updated at</th>

								</tr>
							</thead>

						</table>

					</div>
				</div>
			</div>
		</article>
	</div>
</section>




<a style="display: none" data-toggle="modal" data-target="#createDataModal" id="createDataModal-button">Click me</a>

<!-- Modal -->
<div class="modal fade" id="createDataModal" tabindex="-1" role="dialog" aria-labelledby="createDataModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">


			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					&times;
				</button>
				<h4 class="modal-title" id="createDataModalLabel">สร้างใหม่</h4>
			</div>
			<div class="modal-body">




<form id="createData-form">

				<div class="row">

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Name
							</label>

							<input class="form-control" name="name"/>

						</div>
					</div>


					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Description
							</label>

							<textarea class="form-control" name="description" rows="4" cols="40"></textarea>

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Subject
							</label>

							<input class="form-control" name="subject" />

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Number of students
							</label>

							<input class="form-control" name="number_of_student" />

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							Start time
							</label>


							<div class="input-group">
								<input class="form-control time-picker" type="text" name="start_time" placeholder="Select time">
								<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
							</div>

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="form-group">
							<label>
							End time
							</label>


							<div class="input-group">
								<input class="form-control time-picker" type="text" name="end_time" placeholder="Select time">
								<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
							</div>

						</div>
					</div>

					<div class="col col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<input type="hidden" name="category_id" value="">
						<div class="form-group">
							<label>
							Category
							</label>

							<input class="form-control" name="category_autocomplete" />

						</div>
					</div>

				</div>

</form>




			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					ยกเลิก
				</button>
				<button type="button" class="btn btn-primary" id="createData-submit">
					ยืนยัน
				</button>
			</div>



		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

var otable;
var pageJavascriptTrigger = function()
{
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	var currentYear = new Date().getFullYear();

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];

	$('[name=category_autocomplete]').autocomplete({
		source: function(request, response) {
			$.ajax({
				url: "{{ URL::to('json/categories') }}",
				dataType: "json",
				type: "POST",
				data: {
					search: request.term
				},
				success: function(data){

					response( $.map( data, function( item ) {
						return {
							label: item.name,
							value: item.name,
							id: item.id,
						}
					}));
				 }
			})
		},
		select: function(event, ui) {

			//console.log(ui);
			$("[name=category_id]").val(ui.item.id);  // ui.item.value contains the id of the selected label
		}
	}).keyup(function()
	{
		if ( $(this).val() == '') {

			$("[name=category_id]").val('');
		}
	});


	$("[name=birthday]").datepicker({
		maxDate: (currentYear-10) + '-12-31',
		defaultDate: "now",
		changeMonth: true,
		changeYear: true,
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		prevText: '<i class="fa fa-chevron-left"></i>',
		nextText: '<i class="fa fa-chevron-right"></i>',
		onClose: function (selectedDate) {
			//$("#startDate").datepicker("option", "maxDate", selectedDate);

			console.log(selectedDate);
		}
	});



	$('#search-form').on('submit', function(e)
	{
		e.preventDefault();
		otable.ajax.reload();
	})

	/* COLUMN FILTER  */
    otable = $('#datatable').DataTable({
		"processing": false,
        "serverSide": true,
		"ajax": {
            "url": "{{ Request::url() }}",
            "data": function ( dataPost ) {

					@foreach ( $queryStrings AS $key => $value )
			          dataPost.{{ $key }} = '{{ $value }}';
					@endforeach

					dataPost.name = $('input[name=name]', '#search-form').val()
					dataPost.time_range = $('input[name=time_range]', '#search-form').val()
		       },
		       "dataSrc": function ( json ) {
	                //Make your callback here.
	                //alert("Done!");
	                //$('#country').val(json.country);
	                return json.data;
	            }
            },
        "sDom": "<'dt-toolbar'<'col-xs-6'f><'col-xs-6'<'toolbar'>>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-xs-6'i><'col-xs-6'p>>",

        "drawCallback": function( settings ) {
	        pageSetUp();

	    },


	    'aoColumnDefs': [{
	        'bSortable': false,
	        'aTargets': ['nosort']
	    }],

		"order": [[ 0, "desc" ]],

	    "columns": [
	        {
	        	"data": "id",
	        	"render": function ( data, type, full, meta ) {
	        		//var url = '{{ URL::to('admin/whois/edit') }}' + '/' + full.whois_id;
	        		var url = '#';
				    //return '<a href="' + url + '" class="ajax">' + $.number(data) + '</a>';
				    return $.number(data);
				},
				"type": "num"
	        },
	        {
	        	"data": "name",
	        	"render": function ( data, type, full, meta ) {

				    return ( data == '' ? '<i>Empty</i>' : data );
				} ,
				"type": "string"
	        },

			{
	        	"data": "subject",
	        	"render": function ( data, type, full, meta ) {

				    return ( data == '' ? '<i>Empty</i>' : data );
				} ,
				"type": "string"
	        },

			{
	        	"data": "start_time",
	        	"render": function ( data, type, full, meta ) {

				    return ( data == '' ? '<i>Empty</i>' : data );
				} ,
				"type": "string"
	        },

			{
	        	"data": "end_time",
	        	"render": function ( data, type, full, meta ) {

				    return ( data == '' ? '<i>Empty</i>' : data );
				} ,
				"type": "string"
	        },

			{
	        	"data": "number_of_student",
	        	"render": function ( data, type, full, meta ) {
				    return $.number(data);
				},
				"type": "num"
	        },

			{
	        	"data": "category_id",
	        	"render": function ( data, type, full, meta ) {

					if ( full.category ) {

						return full.category.name;
					}

					if ( full.category_id == '0' ) {
						return '-';
					}
					return $.number(data);
				},
				"type": "string"
	        },

	        {
	        	"data": "updated_by",
	        	"render": function ( data, type, full, meta ) {

					if ( full.updated_user ) {

						return full.updated_user.user_name;
					}

					if ( full.updated_by == '0' ) {
						return '-';
					}
					return $.number(data);
				},
				"type": "string"
	        },
	        {
	        	"data": "updated_at_text",
	        	"render": function ( data, type, full, meta ) {

					return data;
					// X-Editable
					//if ( full.status == 0 )

				    	//return '<a href="#" class="status-selected" id="status_' + full.whois_id + '" data-type="select" data-pk="1" data-value="' + full.status + '" data-original-title="' + full.status_title + '"></a>';

				    //else
				    	//return ( full.status == 1 ? '<span class="txt-color-green">' + full.status_title + '</span>' : '<span class="txt-color-red">' + full.status_title + '</span>' )
				    // End X-Editable

				},
				"type": "string"
	        }
		],

		"iDisplayLength": {{ $limit }}
    });




	$('#createDataModal').on('show.bs.modal', function (e) {

		$("[name=name]").val('');
		$("[name=description]").val('');
		$("[name=number_of_student]").val('');
		$("[name=subject]").val('');
		$("[name=start_time]").val('');
		$("[name=end_time]").val('');
		$("[name=category_id]").val('');
		$("[name=category_autocomplete]").val('');
	});


	// custom toolbar
	@if ( $app->user->permission->is_can_create_course )
	$("div.toolbar").html('<div class="text-right"><button class="btn btn-primary" id="createData-button">เพิ่ม Course</button></div>');
	@endif

	$('#createData-button').click(function()
	{
		$('#createDataModal').modal('show');
	});


	$('#createData-submit').click(function(e)
	{
		var obj = this;
		$(obj).prop('disabled');

		e.preventDefault();
		//alert('Submit');

		var dataSend = $('#createData-form').serialize();

		console.log( dataSend );

		if ( $('#name').val() == '' )
		{
			$.smallBox({
						title : "ล้มเหลว",
						content : "<i class='fa fa-clock-o'></i> <i>กรุณาพิมพ์ Name</i>",
						color : "#C46A69",
						iconSmall : "fa fa-thumbs-down bounce animated",
						timeout : 3000
					});
			return;
		}

		console.log( dataSend );


		ajaxPost(dataSend, '{{ Request::url() }}', function(response)
		{
			$(obj).prop('disabled', '');

			console.log(response);

			if ( response.status == '1' )
			{
				$('#createDataModal').modal('hide');
				otable.ajax.reload();
			}
		});
	});


	/*
	 * TIMEPICKER
	 */

	 //Load time picker script

	loadScript("{{ URL::asset('assets/smartadmin/js/plugin/bootstrap-timepicker/bootstrap-timepicker.min.js') }}", runTimePicker);

	function runTimePicker() {

		$('.time-picker').timepicker({
			disableFocus: true,
			showInputs: false,
			showSeconds: true,
			showMeridian: false,
		}).on('changeTime.timepicker', function(e) {

		    var inputName = $(this).prop('name');

			var h = e.time.hours;
    		var m = e.time.minutes;
			var s = e.time.seconds;

			console.log('inputName = ', inputName, e.time);
			if ( inputName == 'start_time') {

				var newTime = (h+1) + ':' + m + ':' + s;

				console.log('end_time = ', newTime);
				$('[name=end_time]').timepicker('setTime', newTime);
			}

		  });

	}

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
