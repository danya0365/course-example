

<!-- row -->
<div class="row">

	@if($errors->has())
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
		</div>
	</div>
	@endif

	@if ( isset($error) ||  !empty($error) )
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			<p>{{ $error }}</p>
		</div>
	</div>
	@endif

	@if ( isset($success) ||  !empty($success) )
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-success alert-block" id="success-wrapper">
			{{ $success }}
		</div>
	</div>
	@endif


	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="well well-sm" style="max-width: 768px; margin: 0 auto;">

							<div class="row">

								<div class="col-sm-12">
									<div id="myCarousel" class="carousel fade profile-carousel">
										<ol class="carousel-indicators">
											<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
											<li data-target="#myCarousel" data-slide-to="1" class=""></li>
											<li data-target="#myCarousel" data-slide-to="2" class=""></li>
										</ol>
										<div class="carousel-inner">
											<!-- Slide 1 -->
											<div class="item active">
												<img src="{{ URL::asset('assets/smartadmin/img/demo/s1.jpg') }}" alt="demo user">
											</div>
											<!-- Slide 2 -->
											<div class="item">
												<img src="{{ URL::asset('assets/smartadmin/img/demo/s2.jpg') }}" alt="demo user">
											</div>
											<!-- Slide 3 -->
											<div class="item">
												<img src="{{ URL::asset('assets/smartadmin/img/demo/m3.jpg') }}" alt="demo user">
											</div>
										</div>
									</div>
								</div>

								<div class="col-sm-12">

									<div class="row">

										<div class="col-sm-3 profile-pic">
											<img src="{{ $user->photo ? $user->photo : URL::asset('assets/smartadmin/img/avatars/sunny-big.png') }}" alt="">

										</div>
										<div class="col-sm-9">
											<h1>{{ $user->name }}</h1>

											<ul class="list-unstyled">
												<li>
													<p class="text-muted">
														<i class="fa fa-user"></i>&nbsp;&nbsp;
														{{ $user->user_name}}
													</p>
												</li>
												<li>
													<p class="text-muted">
														<i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">Register at
														{{ $user->created_at }}
														</span>
													</p>
												</li>
											</ul>
											<br>
											<p class="font-md">
												<i>A little about me...</i>
											</p>
											<p>

												Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio
												cumque nihil impedit quo minus id quod maxime placeat facere

											</p>


										</div>


									</div>

								</div>

							</div>


			</div>

	</div>

</div>




<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */
var pageJavascriptTrigger = function()
{
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
