


<!-- row -->
<div class="row">


	@if($errors->has())
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
		</div>
	</div>
	@endif

	@if ( isset($error) ||  !empty($error) )
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-danger alert-block" id="error-wrapper">
			<h4 class="alert-heading">Error!</h4>
			<p>{{ $error }}</p>
		</div>
	</div>
	@endif

	@if ( isset($success) ||  !empty($success) )
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="alert alert-success alert-block" id="success-wrapper">
			{{ $success }}
		</div>
	</div>
	@endif


	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		<div class="well no-padding" id="form-wrapper">
			<form action="{{ Request::url() }}" id="login-form" class="smart-form client-form" method="post">
				<header>
					Request new password
				</header>

				<fieldset>


					<section>
						<label class="label">E-mail</label>
						<label class="input"> <i class="icon-append fa fa-user"></i>
							<input type="email" name="email">
							</label>
					</section>

					<section>
						<label class="label">or Code</label>
						<label class="input"> <i class="icon-append fa fa-user"></i>
							<input type="code" name="code">
							</label>
					</section>

				</fieldset>
				<footer>
					<button type="submit" class="btn btn-primary">
						Submit
					</button>
				</footer>
			</form>

		</div>

	</div>

</div>


<script type="text/javascript">

	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */
var pageJavascriptTrigger = function()
{
	pageSetUp();

	/*
	 * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
	 * eg alert("my home function");
	 *
	 * var pagefunction = function() {
	 *   ...
	 * }
	 * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
	 *
	 */

	// PAGE RELATED SCRIPTS

	/* remove previous elems */
	if($('.DTTT_dropdown.dropdown-menu').length){
		$('.DTTT_dropdown.dropdown-menu').remove();
	}

	// Needed if you are rendering multiple tables in ajax version
	//var tableDestroyer = [];



	//alert('Load');

}

	{{ Input::get('ajax') ? 'pageJavascriptTrigger()' : '' }}
</script>
<style>
#form-wrapper, #error-wrapper, #success-wrapper
{
	margin: 15px auto !important;
	width: 370px;
}
@media screen and (max-width: 400px) {
	#form-wrapper, #error-wrapper, #success-wrapper
	{
		width:  100%;
	}
}
</style>
