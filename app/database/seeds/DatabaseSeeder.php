<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('InitDataSeeder');
	}

}


class InitDataSeeder extends Seeder {

	public function run()
	{
		$timeNow = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('settings')->truncate();

		DB::table('settings')->insert(
			array(
				array(
                    'name'       => 'จำนวนแสดงข้อมูลต่อหน้าสำหรับ Admin',
                    'var'        => 'admin_data_limit_perpage',
                    'value'      => 30,
                    'created_at' => $timeNow,
                    'updated_at' => $timeNow,
				),
				array(
                    'name'       => 'อายุสำหรับ Token ผู้ใช้ล็อกอิน (วินาที)',
                    'var'        => 'user_access_token_expired_timestamp',
                    'value'      => 60*60*24*30,
                    'created_at' => $timeNow,
                    'updated_at' => $timeNow,
				),
				array(
                    'name'       => 'Format วันเวลาสำหรับ Insert Database',
                    'var'        => 'datetime_database_timestamp_format',
                    'value'      => 'Y-m-d H:i:s',
                    'created_at' => $timeNow,
                    'updated_at' => $timeNow,
				),
				array(
                    'name'       => 'ความยาวรหัสผ่านอย่างน้อย',
                    'var'        => 'user_password_min',
                    'value'      => '4',
                    'created_at' => $timeNow,
                    'updated_at' => $timeNow,
				),
			)
		);

		DB::table('roles')->truncate();

		DB::table('roles')->insert(
			array(
				array(
                    'name'                  => 'Instructor',
                    'is_allow_select'       => 1,
                    'is_guest'              => 0,
                    'is_can_create_course'  => 1,
                    'is_can_view_course'    => 1,
                    'is_can_update_profile' => 1,
                    'is_can_manage_setting' => 1,
                    'is_can_manage_role'    => 1,
                    'is_can_manage_setting' => 1,
                    'is_can_manage_user'    => 1,
                    'created_at'            => $timeNow,
                    'updated_at'            => $timeNow,
				),
				array(
                    'name'                  => 'Student',
                    'is_allow_select'       => 1,
                    'is_guest'              => 0,
                    'is_can_create_course'  => 0,
                    'is_can_view_course'    => 1,
                    'is_can_update_profile' => 1,
                    'is_can_manage_setting' => 0,
                    'is_can_manage_role'    => 0,
                    'is_can_manage_setting' => 0,
                    'is_can_manage_user'    => 0,
                    'created_at'            => $timeNow,
                    'updated_at'            => $timeNow,
				),
				array(
                    'name'                  => 'Guest',
                    'is_allow_select'       => 0,
                    'is_guest'              => 1,
                    'is_can_create_course'  => 0,
                    'is_can_view_course'    => 0,
                    'is_can_update_profile' => 0,
                    'is_can_manage_setting' => 0,
                    'is_can_manage_role'    => 0,
                    'is_can_manage_setting' => 0,
                    'is_can_manage_user'    => 0,
                    'created_at'            => $timeNow,
                    'updated_at'            => $timeNow,
				),
			)
		);

		DB::table('users')->truncate();

		DB::table('users')->insert(
			array(
				array(
                'user_name'  => 'Admin',
                'password'   => Hash::make('Admin'),
                'created_at' => $timeNow,
                'updated_at' => $timeNow,
				)
			)
		);

		DB::table('roles_users')->truncate();

		DB::table('roles_users')->insert(
			array(
				array('user_id' => 1, 'role_id' => 1, 'created_at' => $timeNow, 'updated_at' => $timeNow,)
			)
		);
	}
}
