<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Tutorial http://laravelbook.com/laravel-migrations-managing-databases/
		  //$t->increments('id');
          //$t->integer('age')->nullable();
          //$t->boolean('active')->default(1);
          //$t->integer('role_id')->unsigned();
          //$t->text('bio');
          //$t->dateTime('created_by');
          //$t->timestamps();
          
          Schema::create('roles_users', function($t) {

              $t->increments('id');
              $t->integer('user_id');
              $t->integer('role_id');
              $t->integer('created_by')->default(0);
              $t->integer('updated_by')->default(0);
              $t->timestamps();
              
              $t->unique( array('user_id', 'role_id') );
          });
          
          
          /* altar table 
	          
	          Schema::table('authors', function($t) {
                	$t->string('email', 64);
			});
        
        */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles_users');
		
		/* dropColumn
			Schema::table('authors', function($t) {
                $t->dropColumn('email');
        });
        
        */
	}


}
