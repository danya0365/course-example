<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration {


	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Tutorial http://laravelbook.com/laravel-migrations-managing-databases/
		  //$t->increments('id');
          //$t->integer('age')->nullable();
          //$t->boolean('active')->default(1);
          //$t->integer('role_id')->unsigned();
          //$t->text('bio');
          //$t->dateTime('created_by');
          //$t->timestamps();

          Schema::create('roles', function($t) {

              $t->increments('id');
              $t->string('name')->unique();
              $t->boolean('is_guest')->default(0);
			  $t->boolean('is_can_manage_role')->default(0);
			  $t->boolean('is_can_manage_user')->default(0);
			  $t->boolean('is_can_manage_setting')->default(0);
              $t->boolean('is_can_update_profile')->default(0);
			  $t->boolean('is_can_create_course')->default(0);
			  $t->boolean('is_can_view_course')->default(0);
              $t->boolean('is_allow_select')->default(1); //สามารถเลือกอยู่ตำแหน่งนี้ได้
              $t->integer('created_by')->default(0);
              $t->integer('updated_by')->default(0);
              $t->timestamps();
          });


          /* altar table

	          Schema::table('authors', function($t) {
                	$t->string('email', 64);
			});

        */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('roles');

		/* dropColumn
			Schema::table('authors', function($t) {
                $t->dropColumn('email');
        });

        */
	}


}
