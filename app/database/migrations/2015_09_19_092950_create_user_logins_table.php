<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserLoginsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Tutorial http://laravelbook.com/laravel-migrations-managing-databases/
		  //$t->increments('id');
          //$t->integer('age')->nullable();
          //$t->boolean('active')->default(1);
          //$t->integer('role_id')->unsigned();
          //$t->text('bio');
          //$t->dateTime('created_by');
          //$t->timestamps();

          Schema::create('user_logins', function($t) {

              $t->increments('id');
              $t->integer('device_id');
              $t->integer('user_id');
              $t->string('user_access_token')->unique();
              $t->dateTime('expired_at');
              $t->timestamps();

              $t->unique( array('device_id', 'user_id') );

          });


          /* altar table

	          Schema::table('authors', function($t) {
                	$t->string('email', 64);
			});

        */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_logins');

		/* dropColumn
			Schema::table('authors', function($t) {
                $t->dropColumn('email');
        });

        */
	}


}
