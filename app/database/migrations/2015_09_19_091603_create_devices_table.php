<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Tutorial http://laravelbook.com/laravel-migrations-managing-databases/
		  //$t->increments('id');
          //$t->integer('age')->nullable();
          //$t->boolean('active')->default(1);
          //$t->integer('role_id')->unsigned();
          //$t->text('bio');
          //$t->dateTime('created_by');
          //$t->timestamps();
          Schema::create('devices', function($t) {

              $t->increments('id');
			  $t->string('access_token', 50)->unique();
			  $t->string('device_id')->unique();
              $t->string('name');
              $t->string('version', 50);
              $t->string('os', 50);
			  $t->string('app_version', 50)->default('1.0');
              $t->timestamps();
          });


          /* altar table

	          Schema::table('authors', function($t) {
                	$t->string('email', 64);
			});

        */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devices');

		/* dropColumn
			Schema::table('authors', function($t) {
                $t->dropColumn('email');
        });

        */
	}

}
