<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function($t) {

			$t->increments('id');
			$t->string('name')->uniqid();
			$t->text('description')->nullable();
			$t->string('subject');
			$t->time('start_time');
			$t->time('end_time');
			$t->integer('category_id');
			$t->integer('number_of_student')->default(0);
			$t->integer('created_by')->default(0);
			$t->integer('updated_by')->default(0);
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
