<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//Tutorial http://laravelbook.com/laravel-migrations-managing-databases/
		  //$t->increments('id');
          //$t->integer('age')->nullable();
          //$t->boolean('active')->default(1);
          //$t->integer('role_id')->unsigned();
          //$t->text('bio');
          //$t->dateTime('created_by');
          //$t->timestamps();

          Schema::create('settings', function($t) {

              $t->increments('id');
              $t->string('name')->unique();
              $t->string('var', 100)->unique();
              $t->mediumText('value');
			  $t->string('input_type', 100)->default('text');
              $t->integer('created_by')->default(0);
              $t->integer('updated_by')->default(0);
              $t->timestamps();
          });


          /* altar table

	          Schema::table('authors', function($t) {
                	$t->string('email', 64);
			});

        */
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');

		/* dropColumn
			Schema::table('authors', function($t) {
                $t->dropColumn('email');
        });

        */
	}

}
