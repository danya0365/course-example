
<?php

class JsonController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	private static function __generateMethodName(&$method)
	{
		$names = explode('_', $method);
		$names = array_map('ucfirst', $names);
		$method = implode('', $names);
		return $method;
	}

	public function getResponse($method)
	{
		if ( ! MyAuth::check() ) {

			return Response::json('please log in', 404);
		}

		$method = self::__generateMethodName($method);

		if ( method_exists($this,  $method) )
		{
			return call_user_func( array( $this, $method ) );
		}

		return Response::json('method not found', 404);
	}

	public function users()
	{
		$search = Input::get('search');
		$result = User::getDatas($this->app, ['search' => trim($search), 'limit' => 10]);

		if ( $result['status'] == 1 ) {

			return Response::json($result['data']['data']);
		}
		return Response::json([]);
	}

	public function categories()
	{
		$search = Input::get('search');
		$result = Category::getDatas($this->app, ['search' => trim($search), 'limit' => 10]);

		if ( $result['status'] == 1 ) {

			return Response::json($result['data']['data']);
		}
		return Response::json([]);
	}


	public function course()
	{
		$search = Input::get('search');
		$result = Course::getDatas($this->app, ['search' => trim($search), 'limit' => 10]);

		if ( $result['status'] == 1 ) {

			return Response::json($result['data']['data']);
		}
		return Response::json([]);
	}

}
