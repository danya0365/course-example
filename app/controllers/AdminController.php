<?php

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showLoginView()
	{
		$assignVariable = array();

        if ( Input::get('ajax') )
		{
			return App::make('ErrorController')->error(500, 'Please Log in');
			//return ErrorController::error(500, 'Please Log in');
		}

		return Redirect::to('login');
	}

	private static function __generateAppName(&$appName)
	{
		$names = explode('_', $appName);
		$names = array_map('ucfirst', $names);
		$appName = implode('', $names);
		return $appName;
	}

	#############################
	#
	# GET METHOD
	#

	public function getActionView($appName = 'index', $action = '', $id = '')
	{
		$assignVariable = array();

		if ( ! MyAuth::check() ) {

			return $this->showLoginView();
        }

		$this->appName = $appName;

		$this->__generateAppName($appName);

		$mthodCall = 'get' . ucfirst($appName);

		//dd($mthodCall);

		if ( method_exists($this,  $mthodCall) )
		{
			return call_user_func( array( $this, $mthodCall ), $action, $id);
		}
		else
		{
			$assignVariable['pageTitle'] = ( $this->appName ? '/' . $this->appName . '/' . $action : 'Admin Panel' );

			if ( Input::get('ajax') )
			{
				return View::make('smartadmin/ajax/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
			}

			return View::make('smartadmin/pages/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
		}
	}

	public function getSetting($action, $id)
	{
		if ( ! $this->app->user->permission->is_can_manage_setting )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage Setting');
			//return ErrorController::error(500, 'You don\' have permission to Manage Setting');
		}

		//$this->appName = strtolower( preg_replace('/^get/', '', __FUNCTION__) );

		$assignVariable = array('pageTitle' => $action );
		$assignVariable['settings'] = Setting::all();

		if ( Input::get('ajax') )
		{
			return View::make('smartadmin/ajax/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
		}

		return View::make('smartadmin/pages/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
	}

	public function getUser($action, $id)
	{
        if ( ! $this->app->user->permission->is_can_manage_user )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage User');
			//return ErrorController::error(500, 'You don\' have permission to Manage User');
		}

		//$this->appName = strtolower( preg_replace('/^get/', '', __FUNCTION__) );

		$assignVariable = array('pageTitle' => $action );

		if ( in_array($action, array('all_users')) )
		{
			$limit = ( isset($this->app->setting->admin_data_limit_perpage) ? $this->app->setting->admin_data_limit_perpage : 30 ) ;
			$draw = Input::get('draw');
			if ( !empty($draw) )
			{
				return $this->getDataList($draw, $limit, 'User::getDatas');
			}

			$assignVariable['pageTitle'] = "All Users";
			$assignVariable['limit'] = $limit;
			$assignVariable['roleList'] = Role::where('is_allow_select', '=', 1)->get();
		}

		if ( $action == 'edit' )
		{
	        if ( ! $id )
			{
				return App::make('ErrorController')->error(500, 'Profile not specific');
				//return ErrorController::error(500, 'Profile not specific');
			}

			$limit = ( isset($this->app->setting->admin_data_limit_perpage) ? $this->app->setting->admin_data_limit_perpage : 30 ) ;


			$userInfo =  User::getData($this->app, ['user_id' => $id]);

			$userRoles = [];
			foreach ( $userInfo->roles AS $key => $roleInfo )
			{
				$userRoles[] = $roleInfo->id;
			}

			$assignVariable['pageTitle'] = "Edit User";
			$assignVariable['userInfo'] = $userInfo;
			$assignVariable['userRoles'] = $userRoles;
			$assignVariable['roleList'] = Role::where('is_allow_select', '=', 1)->get();
			$assignVariable['limit'] = $limit;
		}

		if ( Input::get('ajax') )
		{
			return View::make('smartadmin/ajax/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
		}

		return View::make('smartadmin/pages/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);

	}

	public function getRole($action, $id)
	{
        if ( ! $this->app->user->permission->is_can_manage_role )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage Role');
			//return ErrorController::error(500, 'You don\' have permission to Manage User');
		}

		//$this->appName = strtolower( preg_replace('/^get/', '', __FUNCTION__) );

		$assignVariable = array('pageTitle' => $action );

		if ( in_array($action, array('all_roles')) )
		{
			$limit = ( isset($this->app->setting->admin_data_limit_perpage) ? $this->app->setting->admin_data_limit_perpage : 30 ) ;

			$draw = Input::get('draw');
			if ( !empty($draw) )
			{
				return $this->getDataList($draw, $limit, 'Role::getDatas');
			}

			$assignVariable['pageTitle'] = "All Roles";
			$assignVariable['limit'] = $limit;
			$assignVariable['roleList'] = Role::where('is_allow_select', '=', 1)->get();
		}

		if ( $action == 'edit' )
		{
	        if ( ! $id )
			{
				return App::make('ErrorController')->error(500, 'Role not specific');
				//return ErrorController::error(500, 'Role not specific');
			}

			$roleInfo =  Role::find($id);
			$assignVariable['pageTitle'] = "Edit Role";
			$assignVariable['roleInfo'] = $roleInfo;
		}

		if ( Input::get('ajax') )
		{
			return View::make('smartadmin/ajax/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);
		}

		return View::make('smartadmin/pages/' . $this->appName . ( $action ? '/' . $action : ''), $assignVariable);

	}


	#############################
	#
	# POST METHOD
	#

	public function postActionView($appName = '', $action = '', $id = '')
	{
		if ( ! MyAuth::check() ) {

			return $this->showLoginView();
        }

		/*
        if ( ! $this->app->user->permission->is_can_access_admincp )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to access Admin Control Panel');
		}
		*/

		$this->appName = $appName;

		$this->__generateAppName($appName);

		$mthodCall = 'post' . ucfirst($appName);

		if ( method_exists($this, $mthodCall) )
		{
			return call_user_func( array( $this, $mthodCall ), $action, $id);
		}

		return App::make('ErrorController')->error(404, 'Page not found');
		//return ErrorController::error(404, 'Page not found');
	}

	public function postSetting($action, $id)
	{
        if ( ! $this->app->user->permission->is_can_manage_setting )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage Setting');
			//return ErrorController::error(500, 'You don\' have permission to Manage Setting');
		}

		$result = Setting::updateObj($this->app, Input::all());
		return Response::json($result);
	}

	public function postUser($action, $id)
	{

        if ( ! $this->app->user->permission->is_can_manage_user )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage User');
			//return ErrorController::error(500, 'You don\' have permission to Manage User');
		}

		if ( in_array($action, array('create') )  )
		{

			$user_roles = Input::get('user_roles');

			if ( ! is_array($user_roles) ) {
			    $user_roles = explode(',', $user_roles);
			    $user_roles = array_map('abs', $user_roles);
			}

			$result = User::createObj($this->app, Input::all(), $user_roles);
			return Response::json($result);
		}


		if ( $action == 'generate_user' ) {

			$result = User::generateUserByCode($this->app, Input::all());
			return Response::json($result);
		}

		if ( $action == 'edit' )
		{

			if ( Input::get('addUserPayment') ) {

				$result = UserPayment::createOrUpdateObj($this->app, Input::all());
				return Response::json($result);
			}

			if ( Input::get('deleteUserPayment') ) {

				$result = UserPayment::deleteObj($this->app, Input::all());
				return Response::json($result);
			}

			if ( Input::get('deleteUserPaymentSummary') ) {
				$result = UserPaymentSummary::deleteObj($this->app, Input::all());
				return Response::json($result);
			}

			$user_roles = Input::get('user_roles');

			if ( ! is_array($user_roles) ) {
			    $user_roles = explode(',', $user_roles);
			    $user_roles = array_map('abs', $user_roles);
			}

			$result = User::updateObj($this->app, Input::all(),$user_roles);
			return Response::json($result);
		}


		if ( $action == 'delete_user') {
			$result = User::deleteObj($this->app, Input::all());
			return Response::json($result);
		}

	}


	public function postRole($action, $id)
	{

        if ( ! $this->app->user->permission->is_can_manage_role )
		{
			return App::make('ErrorController')->error(500, 'You don\' have permission to Manage Role');
			//return ErrorController::error(500, 'You don\' have permission to Manage Role');
		}

		if ( $action == 'edit' )
		{
			$postData = ['id' => $id];
			$postData = array_merge($postData, Input::all());
			$result = Role::updateObj($this->app, $postData);
			return Response::json($result);
		}

	}

}
