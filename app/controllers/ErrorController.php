<?php

//use Illuminate\Http\Request;

class ErrorController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function error($code, $title = '', $description = '')
	{
		$assignVar = ['url' => Request::url(), 'pageTitle' => $code, 'title' => $title, 'description' => $description, 'code' => $code];
		//dd(Request::ajax());
		//$app = App::make('app');

		//$assignVar['app'] = $app;

		if ( Input::get('ajax') || Request::ajax()  )
		{
			return View::make('ajax.error', $assignVar);
		}

		return Response::view('error', $assignVar, $code);
	}

}
