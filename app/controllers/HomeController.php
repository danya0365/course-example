<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndexPage()
	{

		if ( ! MyAuth::check() ) {

			return Redirect::to('login');
		}


		$country = 'Thailand';

		if ( Input::get('ajax') )
			return View::make('ajax/home', array('country' => $country));

		return View::make('home', array('country' => $country, 'pageTitle' => 'Homepage' ));
	}

}
