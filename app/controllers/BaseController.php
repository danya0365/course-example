<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	public $app;

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public static function printLastQuery()
	{
		$queries = DB::getQueryLog();
		$last_query = end($queries);
		dd($last_query);
	}

	public function __construct()
	{
		$app = App::make('app');
		$this->app = $app;
		View::share('app', $app);

		$queryStrings = Request::input();
		View::share('queryStrings', $queryStrings);

		$quarter = MyDatetime::getQuarteryDatetime( time() );
		$currentQuarterNumber = $quarter['number'];
		View::share('currentQuarterNumber', $currentQuarterNumber);

		$currentQuarterYear = $quarter['year'];
		View::share('currentQuarterYear', $currentQuarterYear);
	}

	#############################
	#
	# Helper METHOD
	#

	public function getDataList($draw, $limit, $callFunction, $options = [])
	{
		$sort = $order = $search = '';
		$orderInput = Input::get('order');
		if ( is_array($orderInput) )
		{
			$sort = $orderInput[0]['column'];
			$order = $orderInput[0]['dir'];
		}
		if ( Input::get('search') )
		{
			$searchInput = Input::get('search');
			$searchVar = is_array($searchInput) ? $searchInput : array();
			$search = isset($searchVar['value']) ? trim($searchVar['value']) : '';
		}

		$limit = Input::get('length');
		$skip = Input::get('start');

		$post = Input::all();

		$params = [
			'draw' 		=>	$draw,
			'offset' 	=> 	$skip,
			'limit' 	=> 	$limit,
			'sort'		=>	$sort,
			'order'		=>	$order,
			'search'	=>	$search,
		];

		$inputData = array_merge($post, $params);

		if ( count($options) > 0 ) {
			$inputData = array_merge($inputData, $options);
		}

		//$result = Role::getRoleList($this->app, $params);
		$result = call_user_func($callFunction, $this->app, $inputData);

		return Response::json( $result['data'] );
	}

}
