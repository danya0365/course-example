<?php
/**
 * ApiController class.
 *
 * @extends BaseController
 */


/**

# RESTful API Document:
 <pre>
 POST   : To create resource
 PUT  : Update resource
 GET   : Get a resource or list of resources
 DELETE : To delete resource
 </pre>

  # Description Of Usual Server Responses:
 - 200 `OK` - the request was successful (some API calls may return 201 instead).
 - 201 `Created` - the request was successful and a resource was created.
 - 204 `No Content` - the request was successful but there is no representation to return (i.e. the response is empty).
 - 400 `Bad Request` - the request could not be understood or was missing required parameters.
 - 401 `Unauthorized` - authentication failed or user doesn't have permissions for requested operation.
 - 403 `Forbidden` - access denied.
 - 404 `Not Found` - resource was not found.
 - 405 `Method Not Allowed` - requested method is not supported for resource.

 **/
class ApiController extends BaseController {

	public $responseData = array('status' => 0, 'message' => '', 'data' => '', 'device_access_token_error' => 0, 'user_access_token_error' => 0, 'user_info' => []);

	public $device;

	public $userInfo;

	public $methodNotRequiredApi = ['devices'];

	public $methodRequiredLogin = ['users'];

	public $appName = '';


	public function __construct()
	{
		//override BaseController
		// Reset $this->app
	}


	private static function __generateAppName(&$appName)
	{
		$names = explode('_', $appName);
		$names = array_map('ucfirst', $names);
		$appName = implode('', $names);
		return $appName;
	}

	private function __getDevice($accesstoken = '')
	{

		$api = null;

		if ( ! $accesstoken )
		{
			if ( MyAuth::check() ) {

				/*
				$device_id = Request::getClientIp();
				$result = Device::createOrUpdateObj(['device_id' => $device_id, 'name' => 'Default', 'version' => '1.0', 'os' => 'WebBrowser']);

				if ( $result['status'] == 1 ) {

					$api = $result['data'];
				}
				*/
			}
			else
			{

				$this->responseData['message'] = 'Empty Access Token';
				$this->responseData['device_access_token_error'] = 1;
				return $this->responseData;
			}
		}

		if ( ! $api && ! empty($accesstoken) ) {

			$api = Device::where('access_token', 'LIKE', $accesstoken)->first();
		}

		if ( ! $api )
		{
			$this->responseData['message'] = 'Invalid Access Token';
			$this->responseData['device_access_token_error'] = 1;
			return $this->responseData;
		}

		$this->responseData['status'] = 1;
		$this->responseData['data'] = $api;
		return $this->responseData;
	}

	public function getMethod($appName, $action = '', $options = '', $reference = '')
	{
		$device_access_token = Input::get('device_access_token') ? Input::get('device_access_token') : Input::get('access_token');
		if ( ! $device_access_token ) {
			$device_access_token = Request::header('device_access_token');
		}

		$getApiResult = $this->__getDevice($device_access_token);

		if ( $getApiResult['status'] == 0 ) {

			if ( ! in_array($appName, $this->methodNotRequiredApi) ) {

				return Response::json($getApiResult, 403);
			}
		}

		$this->device = $getApiResult['data'];

		$user_access_token = Input::get('user_access_token');
		if ( ! $user_access_token ) {
			$user_access_token = Request::header('user_access_token');
		}

		$getUserResult = MyAuth::getUserWithAccessToken($user_access_token);

		if ( $getUserResult['status'] == 0 ) {

			if ( in_array($appName, $this->methodRequiredLogin)) {

				$this->responseData = array_merge($this->responseData, $getUserResult);
				$this->responseData['user_access_token_error'] = 1;
				return Response::json($this->responseData, 403);
			}

			$this->app = MyApp::getAppByUserId(0);
			$this->responseData['user_access_token_error'] = 1;
		}
		else {

			$user = $getUserResult['data'];
			$this->app = MyApp::getAppByUserId($user->id);
			$this->responseData['user_access_token_error'] = 0;
		}

		$this->userInfo = $getUserResult['data'];

		$this->responseData['user_info'] = $this->userInfo;

		$this->appName = self::__generateAppName($appName);

		$methodCall = 'get' . ucfirst($appName);

		if ( ! method_exists($this, $methodCall) ) {

			$this->responseData['message'] = 'Method not found';
			return Response::json($this->responseData, 405);
		}


		return call_user_func( array( $this, $methodCall ), $action, $options, $reference);
	}

	public function postMethod($appName, $action = '', $options = '', $reference = '')
	{
		$device_access_token = Input::get('device_access_token') ? Input::get('device_access_token') : Input::get('access_token');
		if ( ! $device_access_token ) {
			$device_access_token = Request::header('device_access_token');
		}

		$getApiResult = $this->__getDevice($device_access_token);

		if ( $getApiResult['status'] == 0 ) {

			if ( ! in_array($appName, $this->methodNotRequiredApi) ) {

				return Response::json($getApiResult, 403);
			}
		}

		$this->device = $getApiResult['data'];

		$user_access_token = Input::get('user_access_token');
		if ( ! $user_access_token ) {
			$user_access_token = Request::header('user_access_token');
		}

		$getUserResult = MyAuth::getUserWithAccessToken($user_access_token);

		if ( $getUserResult['status'] == 0 ) {

			if ( in_array($appName, $this->methodRequiredLogin)) {

				$this->responseData = array_merge($this->responseData, $getUserResult);
				$this->responseData['user_access_token_error'] = 1;
				return Response::json($this->responseData, 403);
			}

			$this->app = MyApp::getAppByUserId(0);
			$this->responseData['user_access_token_error'] = 1;
		}
		else {

			$user = $getUserResult['data'];
			$this->app = MyApp::getAppByUserId($user->id);
			$this->responseData['user_access_token_error'] = 0;
		}

		$this->userInfo = $getUserResult['data'];

		$this->responseData['user_info'] = $this->userInfo;

		$this->appName = self::__generateAppName($appName);

		$methodCall = 'post' . ucfirst($appName);

		if ( ! method_exists($this, $methodCall) ) {

			$this->responseData['message'] = 'Method not found';
			return Response::json($this->responseData, 405);
		}


		return call_user_func( array( $this, $methodCall ), $action, $options, $reference);
	}

	public function putMethod($appName, $action = '', $options = '', $reference = '')
	{
		$device_access_token = Input::get('device_access_token') ? Input::get('device_access_token') : Input::get('access_token');
		if ( ! $device_access_token ) {
			$device_access_token = Request::header('device_access_token');
		}

		$getApiResult = $this->__getDevice($device_access_token);

		if ( $getApiResult['status'] == 0 ) {

			if ( ! in_array($appName, $this->methodNotRequiredApi) ) {

				return Response::json($getApiResult, 403);
			}
		}

		$this->device = $getApiResult['data'];

		$user_access_token = Input::get('user_access_token');
		if ( ! $user_access_token ) {
			$user_access_token = Request::header('user_access_token');
		}

		$getUserResult = MyAuth::getUserWithAccessToken($user_access_token);

		if ( $getUserResult['status'] == 0 ) {

			if ( in_array($appName, $this->methodRequiredLogin)) {

				$this->responseData = array_merge($this->responseData, $getUserResult);
				$this->responseData['user_access_token_error'] = 1;
				return Response::json($this->responseData, 403);
			}

			$this->app = MyApp::getAppByUserId(0);
			$this->responseData['user_access_token_error'] = 1;
		}
		else {

			$user = $getUserResult['data'];
			$this->app = MyApp::getAppByUserId($user->id);
			$this->responseData['user_access_token_error'] = 0;
		}

		$this->userInfo = $getUserResult['data'];

		$this->responseData['user_info'] = $this->userInfo;

		$this->appName = self::__generateAppName($appName);

		$methodCall = 'put' . ucfirst($appName);

		if ( ! method_exists($this, $methodCall) ) {

			$this->responseData['message'] = 'Method not found';
			return Response::json($this->responseData, 405);
		}


		return call_user_func( array( $this, $methodCall ), $action, $options, $reference);
	}

	public function deleteMethod($appName, $action = '', $options = '', $reference = '')
	{
		$device_access_token = Input::get('device_access_token') ? Input::get('device_access_token') : Input::get('access_token');
		if ( ! $device_access_token ) {
			$device_access_token = Request::header('device_access_token');
		}

		$getApiResult = $this->__getDevice($device_access_token);

		if ( $getApiResult['status'] == 0 ) {

			if ( ! in_array($appName, $this->methodNotRequiredApi) ) {

				return Response::json($getApiResult, 403);
			}
		}

		$this->device = $getApiResult['data'];

		$user_access_token = Input::get('user_access_token');
		if ( ! $user_access_token ) {
			$user_access_token = Request::header('user_access_token');
		}

		$getUserResult = MyAuth::getUserWithAccessToken($user_access_token);

		if ( $getUserResult['status'] == 0 ) {

			if ( in_array($appName, $this->methodRequiredLogin)) {

				$this->responseData = array_merge($this->responseData, $getUserResult);
				$this->responseData['user_access_token_error'] = 1;
				return Response::json($this->responseData, 403);
			}

			$this->app = MyApp::getAppByUserId(0);
			$this->responseData['user_access_token_error'] = 1;
		}
		else {

			$user = $getUserResult['data'];
			$this->app = MyApp::getAppByUserId($user->id);
			$this->responseData['user_access_token_error'] = 0;
		}

		$this->userInfo = $getUserResult['data'];

		$this->responseData['user_info'] = $this->userInfo;

		$this->appName = self::__generateAppName($appName);

		$methodCall = 'delete' . ucfirst($appName);

		if ( ! method_exists($this, $methodCall) ) {

			$this->responseData['message'] = 'Method not found';
			return Response::json($this->responseData, 405);
		}

		return call_user_func( array( $this, $methodCall ), $action, $options, $reference);
	}

	public function postDevices($userId, $action, $reference)
	{
		$createOrUpdateApiResult = Device::createOrUpdateObj(Input::all());
		if ( $createOrUpdateApiResult['status'] == 1 ) {
			$this->responseData['device_access_token_error'] = 0;
			$this->responseData = array_merge($this->responseData, $createOrUpdateApiResult);
			return Response::json($this->responseData, 201);
		}

		$this->responseData['device_access_token_error'] = 1;
		$this->responseData = array_merge($this->responseData, $createOrUpdateApiResult);
		return Response::json($this->responseData, 200);
	}

	public function putDevices($deviceId, $action, $reference)
	{
		if ( ! $this->device ) {

			$this->responseData['device_access_token_error'] = 1;
			$this->responseData['message'] = 'Device not found';
			return Response::json($this->responseData, 403);
		}


		if ( is_numeric($deviceId)) {

			$post = Input::all();
			$post['device_id'] = $this->device->id;
			$post['app_version'] = $this->device->app_version;

			if ( $action == 'push_notifications' ) {

				$result = DevicePushNotification::createOrUpdateObj($this->app, $post);

				if ( $result['status'] == 1 ) {

					$this->responseData['device_access_token_error'] = 0;
					$this->responseData = array_merge($this->responseData, $result);
					return Response::json($this->responseData, 201);
				}

				$this->responseData['message'] = 'can not update';
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);

			}
		}

		$this->responseData['message'] = 'You don\'t have permission to update all device ';
		return Response::json($this->responseData, 401);
	}

	//POST /login - creates session
	public function postLogin($action, $options, $reference)
	{
		if ( $action == 'facebook' ) {

			$logInResult = MyAuth::userFacebookLogin($this->app, Input::all(), false, $this->device);

			if ( $logInResult['status'] == 1 ) {

				$this->responseData['user_access_token_error'] = 0;
				$this->responseData = array_merge($this->responseData, $logInResult);
				return Response::json($this->responseData, 201);

			}

			$this->responseData['user_access_token_error'] = 1;
			$this->responseData = array_merge($this->responseData, $logInResult);
			return Response::json($this->responseData, 200);

		}

		$logInResult = MyAuth::userLogin($this->app, Input::all(), false, $this->device);

		if ( $logInResult['status'] == 1 ) {

			$this->responseData['user_access_token_error'] = 0;
			$this->responseData = array_merge($this->responseData, $logInResult);
			return Response::json($this->responseData, 201);

		}

		$this->responseData['user_access_token_error'] = 1;
		$this->responseData = array_merge($this->responseData, $logInResult);
		return Response::json($this->responseData, 200);
	}


	//DELETE /logout - destroys session
	public function deleteLogout($action, $options, $reference)
	{
		//dd(__FUNCTION__);
		$logInResult = MyAuth::userLogout($this->app, Input::get('user_access_token'));
		$this->responseData = array_merge($this->responseData, $logInResult);
		return Response::json($this->responseData, 200);
	}

	public function postRequestNewPassword($action, $options, $reference)
	{
		$result = User::requestNewPassword($this->app, Input::all());
		$statusCode = 200;
		if ( $result['status'] == 1 ) {
			$statusCode = 201;
		}
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, $statusCode);
	}


	public function postRegister($action, $options, $reference)
	{
		/*
		if ( $action == 'facebook' ) {

			$data = [];
			$data['is_confirmed_email'] = 1;
			$data = array_merge($data, Input::all());
			$result = User::registerViaFacebook($this->app, $data);
			$this->responseData = array_merge($this->responseData, $result);
			return Response::json($this->responseData);
		}
		*/

		$result = User::register($this->app, Input::all());

		if ( $result['status'] == 1 ) {

			$this->responseData = array_merge($this->responseData, $result);
			return Response::json($this->responseData, 201);

		}

		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}


	public function getUsers($userId, $action, $reference)
	{
		if ( is_numeric($userId) ) {

			if ( $userId != $this->userInfo->id ) {

				if ( ! $this->app->user->permission->is_can_manage_user ) {

					$this->responseData['message'] = 'You don\'t have permission to view other profile ';
					return Response::json($this->responseData, 401);
				}
			}

			if ( $action == 'payments' ) {

				$post = Input::all();
				$post['user_id'] = $userId;
				$result = UserPayment::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'payment_summaries' ) {

				$post = Input::all();
				$post['user_id'] = $userId;

				$result = UserPaymentSummary::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			$user = User::getData($this->app, ['user_id' => $userId]);

			if ( $user ) {

				$this->responseData['data'] = $user->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}


		if ( ! $this->app->user->permission->is_can_manage_user ) {

			$this->responseData['message'] = 'You don\'t have permission to view all users ';
			return Response::json($this->responseData, 401);
		}

		$post = Input::all();
		$result = User::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function postUsers($userId, $action, $reference)
	{
		if ( is_numeric($userId) ) {

			if ( $userId != $this->userInfo->id ) {

				if ( ! $this->app->user->permission->is_can_manage_user ) {

					$this->responseData['message'] = 'You don\'t have permission to create on other profile ';
					return Response::json($this->responseData, 401);
				}
			}

			if ( $action == 'payments' ) {

				$post = Input::all();
				$post['user_id'] = $userId;

				$result = UserPayment::createOrUpdateObj($this->app, $post);
				$statusCode = 200;
				if ( $result['status'] == 1 ) {
					$statusCode = 201;
				}
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, $statusCode);
			}

			$this->responseData['message'] = 'You don\'t have permission to do this';
			return Response::json($this->responseData, 401);
		}

		if ( ! $this->app->user->permission->is_can_manage_user ) {

			$this->responseData['message'] = 'You don\'t have permission to create on user ';
			return Response::json($this->responseData, 401);
		}

		$user_roles = Input::get('user_roles');

		if ( ! is_array($user_roles) ) {
			$user_roles = explode(',', $user_roles);
			$user_roles = array_map('abs', $user_roles);
		}

		$result = User::createObj($this->app, Input::all(), $user_roles);
		$statusCode = 200;
		if ( $result['status'] == 1 ) {
			$statusCode = 201;
		}
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, $statusCode);
	}

	public function putUsers($userId, $action, $reference)
	{
		if ( is_numeric($userId) ) {

			if ( $userId != $this->userInfo->id ) {

				if ( ! $this->app->user->permission->is_can_manage_user ) {

					$this->responseData['message'] = 'You don\'t have permission to update other profile ';
					return Response::json($this->responseData, 401);
				}
			}

			$post = Input::all();
			$post['id'] = $userId;
			$result = User::updateProfile($this->app, $post);
			$this->responseData = array_merge($this->responseData, $result);
			return Response::json($this->responseData, 200);
		}

		$this->responseData['message'] = 'You don\'t have permission to update users ';
		return Response::json($this->responseData, 401);

	}

	public function getNewsEvents($newsId, $action, $reference)
	{
		if ( is_numeric($newsId) ) {

			$newsEvent = NewsEvent::with('updatedUser')->find($newsId);

			if ( $newsEvent ) {

				$this->responseData['data'] = $newsEvent->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$result = NewsEvent::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getProductBrands($brandId, $action, $reference)
	{
		if ( is_numeric($brandId) ) {


			if ( $action == 'product_types' ) {

				$post = Input::all();
				$post['brand_id'] = $brandId;
				$result = ProductType::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}


			$productBrand = ProductBrand::with('updatedUser')->find($brandId);

			if ( $productBrand ) {

				$this->responseData['data'] = $productBrand->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$result = ProductBrand::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getProductTypes($typeId, $action, $reference)
	{
		if ( is_numeric($typeId) ) {


			if ( $action == 'products' ) {

				$post = Input::all();
				$post['type_id'] = $typeId;
				$result = Product::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}


			$productType = ProductType::with('productBrand')->with('updatedUser')->find($typeId);

			if ( $productType ) {

				$this->responseData['data'] = $productType->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$result = ProductType::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}


	public function getProducts($productId, $action, $reference)
	{
		if ( is_numeric($productId) ) {

			$product = Product::with('productBrand')->with('productType')->with('updatedUser')->find($productId);

			if ( $product ) {

				$this->responseData['data'] = $product->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$result = Product::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getPromotions($promotionId, $action, $reference)
	{
		if ( is_numeric($promotionId) ) {

			$promotion = Promotion::with('updatedUser')->find($promotionId);

			if ( $promotion ) {

				$this->responseData['data'] = $promotion->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		if ( ! isset($post['is_trip']) ) $post['is_trip'] = 0;
		if ( ! isset($post['is_campaign']) ) $post['is_campaign'] = 0;
		$result = Promotion::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getPointRewards($pointRewardId, $action, $reference)
	{
		if ( is_numeric($pointRewardId) ) {

			$pointReward = PointReward::with('updatedUser')->find($pointRewardId);

			if ( $pointReward ) {

				$this->responseData['data'] = $pointReward->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		if ( ! isset($post['is_trip']) ) $post['is_trip'] = 0;
		if ( ! isset($post['is_campaign']) ) $post['is_campaign'] = 0;
		$result = PointReward::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getSettings($settingVar, $action, $reference)
	{
		$this->responseData['status'] = 1;
		$this->responseData['data'] = get_object_vars($this->app->setting);
		return Response::json($this->responseData, 200);
	}

	public function getTrips($contentId, $action, $reference)
	{
		if ( is_numeric($contentId) ) {

			if ( $action == 'promotions' ) {

				$post = Input::all();
				$post['trip_id'] = $contentId;
				$result = Promotion::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'user_points' ) {

				$post = Input::all();
				$post['trip_id'] = $contentId;
				$post['user_id'] = $this->app->user->id;

				$result = UserTripPoint::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'point_rewards' ) {

				$post = Input::all();
				$post['trip_id'] = $contentId;
				$result = PointReward::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'user_summaries' ) {

				$post = Input::all();
				$post['trip_id'] = $contentId;
				$post['user_id'] = $this->app->user->id;
				$result = UserTripSummary::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			$trip = Trip::with('updatedUser')->find($contentId);

			if ( $trip ) {

				$this->responseData['data'] = $trip->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$post['user_id'] = $this->app->user->id;
		$result = Trip::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}

	public function getCampaigns($contentId, $action, $reference)
	{
		if ( is_numeric($contentId) ) {

			if ( $action == 'promotions' ) {

				$post = Input::all();
				$post['campaign_id'] = $contentId;
				$result = Campaign::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'user_point' ) {

				$post = Input::all();
				$post['campaign_id'] = $contentId;
				$post['user_id'] = $this->app->user->id;
				$result = UserCampaignPoint::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'point_rewards' ) {

				$post = Input::all();
				$post['campaign_id'] = $contentId;
				$result = PointReward::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}

			if ( $action == 'user_summaries' ) {

				$post = Input::all();
				$post['campaign_id'] = $contentId;
				$post['user_id'] = $this->app->user->id;
				$result = UserCampaignSummary::getDatas($this->app, $post);
				$this->responseData = array_merge($this->responseData, $result);
				return Response::json($this->responseData, 200);
			}


			$campaign = Campaign::with('updatedUser')->find($contentId);

			if ( $campaign ) {

				$this->responseData['data'] = $campaign->toArray();
				$this->responseData['status'] = 1;
				return Response::json($this->responseData, 200);
			}

			$this->responseData['data'] = null;
			$this->responseData['status'] = 0;
			return Response::json($this->responseData, 204);
		}

		$post = Input::all();
		$post['user_id'] = $this->app->user->id;
		$result = Campaign::getDatas($this->app, $post);
		$this->responseData = array_merge($this->responseData, $result);
		return Response::json($this->responseData, 200);
	}
}
