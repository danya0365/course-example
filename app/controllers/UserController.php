<?php

class UserController extends BaseController {

	public function getConfirmByEmail($userId, $confirmCode)
	{
		$user = User::find($userId);

		if ( ! $user ) {

			return App::make('ResultController')->failed(500, 'User Not found');
			//return App::make('SuccessController')->success(200, 'สำเร็จ', 'User Not found');
		}

		$result = $user->confirmedEmailByCode($this->app, $confirmCode);

		if ( $result['status'] == 0) {

			return App::make('ResultController')->failed(500, 'ล้มเหลว', $result['message']);
		}

		//return App::make('ResultController')->success(200, 'สำเร็จ', $result['message']);

		MyAuth::doLoginWithUserId($this->app, $user->id);
		return Redirect::to('/')
			->with('success', $result['message']);
	}

	public function sendWelcomeEmail($userId)
	{
		$user = User::find($userId);

		if ( ! $user ) {

			return App::make('ResultController')->failed(500, 'User Not found');
			//return App::make('SuccessController')->success(200, 'สำเร็จ', 'User Not found');
		}

		$result = $user->sendWelcomeEmail($this->app);

		return App::make('ResultController')->success(200, 'สำเร็จ', $result['message']);
	}

	public function getLogin()
	{

		if ( ! $this->app->user->permission->is_guest )
		{
			return $this->checkRedirectPage();
		}


		if ( Request::isMethod('post')) {

			Input::merge(array_map('trim', Input::all()));

			$logInResult = MyAuth::userLogin($this->app, Input::all(), Input::get('remember'), null);

			if ( Request::ajax()) {
				return Response::json($result);
			}

			if ( $logInResult['status'] == 0 ) {

				return Redirect::to('login')
					->withErrors([$logInResult['message']]);

			} else {

				return Redirect::to('/');
			}
		}

		$facebook_error = Session::get('facebook_error');

		$assignVariable = array('pageTitle' => 'Login');

		if ( !empty($facebook_error) )
		{
			$assignVariable['error'] = $facebook_error;
		}

		$error = Session::get('error');
		if ($error) {

			$assignVariable['error'] = $error;
		}
		$success = Session::get('success');
		if ($success) {

			$assignVariable['success'] = $success;
		}

		if ( Input::get('ajax') )
		{
			return View::make('ajax/login', $assignVariable);
		}
		return View::make('login', $assignVariable);
	}

	public function getCourseList()
	{
		if ( ! MyAuth::check() )
		{
			return Redirect::to('login');
		}


		if (Request::isMethod('post'))
		{

			if ( $this->app->user->permission->is_can_create_course ) {

				$result = Course::createOrUpdateObj($this->app, Input::all());

				if ( Request::ajax()) {

					return Response::json($result);
				}
			}
		}


		if ( ! $this->app->user->permission->is_can_view_course ) {

			return App::make('ErrorController')->error(500, 'You don\'t have permission to view course');
		}

		$limit = ( isset($this->app->setting->admin_data_limit_perpage) ? $this->app->setting->admin_data_limit_perpage : 30 ) ;

		if ( Request::ajax() && Input::get('draw')) {

			return $this->getDataList(Input::get('draw'), $limit, 'Course::getDatas');
		}

		$assignVariable = array('pageTitle' => 'Course List');

		$error = Session::get('error');
		if ($error) {

			$assignVariable['error'] = $error;
		}
		$success = Session::get('success');
		if ($success) {

			$assignVariable['success'] = $success;
		}

		$assignVariable['limit'] = $limit;
		$assignVariable['user'] = $this->app->user;

		if ( Input::get('ajax') )
		{
			return View::make('ajax/course_list', $assignVariable);
		}
		return View::make('course_list', $assignVariable);
	}

	public function getUpdateProfile()
	{
		if ( ! MyAuth::check() )
		{
			return Redirect::to('login');
		}

		if ( ! $this->app->user->permission->is_can_update_profile ) {

			return App::make('ErrorController')->error(500, 'You don\'t have permission to update profile');
		}

		if (Request::isMethod('post'))
		{
			$result = User::updateProfile($this->app, Input::all());

			if ( Request::ajax()) {
				return Response::json($result);
			}

		}

		$assignVariable = array('pageTitle' => 'Profile');

		$error = Session::get('error');
		if ($error) {

			$assignVariable['error'] = $error;
		}
		$success = Session::get('success');
		if ($success) {

			$assignVariable['success'] = $success;
		}

		$assignVariable['user'] = $this->app->user;

		if ( Input::get('ajax') )
		{
			return View::make('ajax/update_profile', $assignVariable);
		}
		return View::make('update_profile', $assignVariable);
	}

	public function getProfile()
	{
		if ( ! MyAuth::check() )
		{
			return Redirect::to('login');
		}

		$assignVariable = array('pageTitle' => 'Profile');

		$error = Session::get('error');
		if ($error) {

			$assignVariable['error'] = $error;
		}
		$success = Session::get('success');
		if ($success) {

			$assignVariable['success'] = $success;
		}

		$assignVariable['user'] = $this->app->user;

		if ( Input::get('ajax') )
		{
			return View::make('ajax/profile', $assignVariable);
		}
		return View::make('profile', $assignVariable);
	}

	public function getChangePassword()
	{
		if ( ! MyAuth::check() )
		{
			return Redirect::to('login');
		}

		if (Request::isMethod('post'))
		{
			if ( ! MyAuth::check() )
			{
				return Redirect::to('login');
			}

			Input::merge(array_map('trim', Input::all()));
			$post = Input::all();
			$post['id'] = $this->app->user->id;
			$result = User::changePassword($this->app, $post);

			if ( Request::ajax()) {

				return Response::json($result);
			}

			if ( $result['status'] == 0 ) {

				return Redirect::to('/change_password')
					->with('error', $result['message']);
			}
			else
			{
				return Redirect::to('/profile')
					->with('success', $result['message']);
			}
		}


		$assignVariable = array('pageTitle' => 'Change Password');

		$error = Session::get('error');
		if ($error) {

			$assignVariable['error'] = $error;
		}
		$success = Session::get('success');
		if ($success) {

			$assignVariable['success'] = $success;
		}

		$assignVariable['user'] = $this->app->user;

		if ( Input::get('ajax') )
		{
			return View::make('ajax/change_password', $assignVariable);
		}
		return View::make('change_password', $assignVariable);
	}

	public function checkRedirectPage()
	{
		if ( ! MyAuth::check() ) {

			return Redirect::to('login');
        }

        if ( ! $this->app->user->permission )
		{
			return 'You don\'t have permission to access This Site';
			//return ErrorController::errorGiver500('You don\' have permission to access Staff Control Panel');
		}

		if ( $this->app->user->permission->is_admin )
		{
			return Redirect::to('admin');
		}

		return Redirect::to('/');
	}


	public function doLogout()
	{
		MyAuth::logout();
		return Redirect::to('/');
	}
}
