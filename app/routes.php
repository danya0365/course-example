<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/



Route::get('send_welcome/{userId}', 'UserController@sendWelcomeEmail');

Route::get('/', function()
{
	if ( ! MyAuth::check() ) {

		return Redirect::to('login');
	}

	$app = App::make('app');

    /*
	if ( $app->user->permission->is_can_access_admincp )
	{
		return Redirect::to('admin');
	}
    */

	return Redirect::to('home');
});


Route::get('redirect', 'UserController@checkRedirectPage');
Route::get('home', 'HomeController@getIndexPage');

Route::post('json/{method}', 'JsonController@getResponse');

Route::get('profile', 'UserController@getProfile');
Route::match(array('GET', 'POST'), 'update_profile', function ()    {

	return App::make('UserController')->getUpdateProfile();
});


Route::match(array('GET', 'POST'), 'change_password', function ()    {

	return App::make('UserController')->getChangePassword();
});


Route::match(array('GET', 'POST'), 'login', function ()    {

	return App::make('UserController')->getLogin();
});


Route::match(array('GET', 'POST'), 'course_list', function ()    {

	return App::make('UserController')->getCourseList();
});

Route::get('logout', 'UserController@doLogout');




Route::get('admin/{apps?}/{action?}/{id?}', 'AdminController@getActionView');
Route::post('admin/{apps?}/{action?}/{id?}', 'AdminController@postActionView');


Route::get('api/{method?}/{action?}/{option?}/{reference?}', 'ApiController@getMethod')->where(array('method' => '[a-z_]+'));
Route::post('api/{method?}/{action?}/{option?}/{reference?}', 'ApiController@postMethod')->where(array('method' => '[a-z_]+'));
Route::put('api/{method?}/{action?}/{option?}/{reference?}', 'ApiController@putMethod')->where(array('method' => '[a-z_]+'));
Route::delete('api/{method?}/{action?}/{option?}/{reference?}', 'ApiController@deleteMethod')->where(array('method' => '[a-z_]+'));
