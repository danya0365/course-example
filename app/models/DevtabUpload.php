<?php

class DevtabUpload {

	public static function image($type, $member_id = '0')
	{
		$jsonResponse = array(
            'status'  => 0,
            'data'    => array('original' => '', '240x240' => '', '240x240_crop' => '', '480x480' => '', '1024x768' => '', '1920x1080' => ''),
            'message' => ''
			);

		$ds = DIRECTORY_SEPARATOR;  //1

		$file = Input::file('file');

		if ( empty($file) )
		{
			$jsonResponse['message'] = 'POST "file" not found';
			return $jsonResponse;
		}

		$input = Input::all();

		$rules = array
		(
		    'file' => 'mimes:jpeg,bmp,png,gif | Max:2048'
		);


		$validator = Validator::make(
                [
                    'file' => $file,
                    'extension'  => strtolower($file->getClientOriginalExtension()),
                ],
                [
                    'file' => 'required|max:2000000',
                    'extension'  => 'required|in:jpg,jpeg,gif,png'
                ]
            );

		if ( $validator->passes() ) {

            $resize_150x150px   = TRUE;
            $resize_300x300px   = TRUE;
            $resize_900x900px   = TRUE;
            $resize_1024x768px  = TRUE;
            $resize_1280x720px  = TRUE;
            $resize_1920x1080px = TRUE;

			$filename = str_replace( ['_','-','.',' '] , '', microtime());
			$yearMonth = date('Y-m');
            $fileLocation = '/uploads/' . $type . '/'.$member_id.'/' . $yearMonth . '/' . $filename . '/';
            //$fileLocation = '/uploads/' . $type . '/'.$member_id.'/' . $yearMonth . '/';
			$destinationPath = base_path() . $fileLocation;
            if ( !file_exists($destinationPath) ) {
                // path does not exist
                $result = File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }

            #Referrence http://www.phpclasses.org/package/2181-PHP-Process-files-and-images-uploaded-via-a-form.html




            $handle                  = new upload( $_FILES['file'] );
            $handle->dir_auto_create = true;
            $extension               = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $file_src_name           = basename($file->getClientOriginalName(),'.'.$extension);
            $file_src_name           = str_replace(' ', '_', $file_src_name);

			if ($handle->uploaded)
			{
				//$file_src_name = $handle->file_src_name;

				//$file_src_name = '1';

				$handle->file_new_name_body = $file_src_name . '_original';
				//$handle->image_convert = 'jpg';
				$handle->process($destinationPath);

				if ($handle->processed)
				{
					$file_url_original  = URL::to($fileLocation . $handle->file_dst_name);
					if ( $file_url_original )
					{
						list($width, $height, $type, $attr) = @getimagesize($file_url_original);

						if ( $width < 50 || $height < 50 )
						{
							$handle->clean();
							$jsonResponse['message'] = 'ความกว้างและความยาวต้องมากกว่า 50px';
							return $jsonResponse;
						}

					}

					$jsonResponse['data']['original'] = $file_url_original;
				}
				else
				{
					$handle->clean();
					$jsonResponse['message'] = $handle->error;
					return $jsonResponse;
				}


				//if ( $width >= 240 && $height >= 240 )
				if ( 1 )
				{
                    $handle->file_new_name_body     = $file_src_name . '_240x240';
					//$handle->image_convert = 'jpg';
                    $handle->image_background_color = '#FFFFFF';
                    $handle->image_resize           = true;

					if ( $height > $width)
					{
                        $handle->image_ratio_fill = true;
                        $handle->image_ratio_crop = false;
					}
					else
					{
                        $handle->image_ratio_fill = false;
                        $handle->image_ratio_crop = true;
					}

                    $handle->image_x         = 240;
                    $handle->image_y         = 240;
                    $handle->dir_auto_create = true;

					$handle->process($destinationPath);

					if ($handle->processed)
					{
						$jsonResponse['data']['240x240'] = URL::to($fileLocation . $handle->file_dst_name);
					}
					else
					{
						$handle->clean();
						$jsonResponse['message'] = $handle->error;
						return $jsonResponse;
					}
				}

				if ( 1 )
				{
                    $handle->file_new_name_body     = $file_src_name . '_240x240_crop';
					//$handle->image_convert = 'jpg';
                    $handle->image_background_color = '#FFFFFF';
                    $handle->image_resize           = true;
                    $handle->image_ratio_fill       = false;
                    $handle->image_ratio_crop       = true;
                    $handle->image_x                = 240;
                    $handle->image_y                = 240;
                    $handle->dir_auto_create        = true;

					$handle->process($destinationPath);

					if ($handle->processed)
					{
						$jsonResponse['data']['240x240_crop'] = URL::to($fileLocation . $handle->file_dst_name);
					}
					else
					{
						$handle->clean();
						$jsonResponse['message'] = $handle->error;
						return $jsonResponse;
					}
				}

				//if ( $width >= 480 && $height >= 480 )
				if ( 1 )
				{
                    $handle->file_new_name_body     = $file_src_name . '_480x480';
					//$handle->image_convert = 'jpg';
                    $handle->image_background_color = '#FFFFFF';
                    $handle->image_resize           = true;

					if ( $height > $width)
					{
                        $handle->image_ratio_fill = true;
                        $handle->image_ratio_crop = false;
					}
					else
					{
                        $handle->image_ratio_fill = false;
                        $handle->image_ratio_crop = true;
					}

                    $handle->image_x         = 480;
                    $handle->image_y         = 480;
                    $handle->dir_auto_create = true;

					$handle->process($destinationPath);

					if ($handle->processed)
					{
						$jsonResponse['data']['480x480'] = URL::to($fileLocation . $handle->file_dst_name);
					}
					else
					{
						$handle->clean();
						$jsonResponse['message'] = $handle->error;
						return $jsonResponse;
					}
				}

				if ( $width >= 1024 && $height >= 768 )
				{
                    $handle->file_new_name_body     = $file_src_name . '_1024x768';
					//$handle->image_convert = 'jpg';
                    $handle->image_background_color = '#FFFFFF';
                    $handle->image_resize           = true;

					if ( $height > $width)
					{
                        $handle->image_ratio_fill = true;
                        $handle->image_ratio_crop = false;
					}
					else
					{
                        $handle->image_ratio_fill = false;
                        $handle->image_ratio_crop = true;
					}

                    $handle->image_x         = 1024;
                    $handle->image_y         = 768;
                    $handle->dir_auto_create = true;

					$handle->process($destinationPath);

					if ($handle->processed)
					{
						$jsonResponse['data']['1024x768'] = URL::to($fileLocation . $handle->file_dst_name);
					}
					else
					{
						$handle->clean();
						$jsonResponse['message'] = $handle->error;
						return $jsonResponse;
					}
				}

				if ( $width >= 1920 && $height >= 1080 )
				{
                    $handle->file_new_name_body     = $file_src_name . '_1920x1080';
					//$handle->image_convert = 'jpg';
                    $handle->image_background_color = '#FFFFFF';
                    $handle->image_resize           = true;

					if ( $height > $width)
					{
                        $handle->image_ratio_fill = true;
                        $handle->image_ratio_crop = false;
					}
					else
					{
                        $handle->image_ratio_fill = false;
                        $handle->image_ratio_crop = true;
					}

                    $handle->image_x         = 1920;
                    $handle->image_y         = 1080;
                    $handle->dir_auto_create = true;

					$handle->process($destinationPath);

					if ($handle->processed)
					{
						$jsonResponse['data']['1920x1080'] = URL::to($fileLocation . $handle->file_dst_name);
					}
					else
					{
						$handle->clean();
						$jsonResponse['message'] = $handle->error;
						return $jsonResponse;
					}
				}

			}
			else
			{
				$jsonResponse['message'] = 'Failed to upload file';
				return $jsonResponse;
			}

			$handle->clean();

			$jsonResponse['status'] = 1;
			return $jsonResponse;
		}
		else
		{
			$jsonResponse['message'] = $validator;
		    return $jsonResponse;
		}

		return $jsonResponse;
	}


	public static function pdf()
	{
		$jsonResponse = array(
            'status'  => 0,
            'data'    => null,
            'message' => ''
			);

		$ds = DIRECTORY_SEPARATOR;  //1

		$file = Input::file('file');

		if ( empty($file) )
		{
			$jsonResponse['message'] = 'POST "file" not found';
			return $jsonResponse;
		}

		$validator = Validator::make(
                [
                    'file' => $file,
                    'extension'  => strtolower($file->getClientOriginalExtension()),
                ],
                [
                    'file' => 'required|max:' . (1024*20),
                    'extension'  => 'required|in:pdf'
                ]
            );

		if ( $validator->passes() ) {

			$filename = str_replace( ['_','-','.',' '] , '', microtime());
			$yearMonth = date('Y-m');
            $fileLocation = '/uploads/pdf2image/' . $yearMonth . '/' . $filename . '/';
			$destinationPath = base_path() . $fileLocation;
            if ( !file_exists($destinationPath) ) {

                $result = File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }

            #Referrence http://www.phpclasses.org/package/2181-PHP-Process-files-and-images-uploaded-via-a-form.html


            $handle                  = new upload( $_FILES['file'] );
            $handle->dir_auto_create = true;
            $extension               = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
            $file_src_name           = basename($file->getClientOriginalName(),'.'.$extension);
            $file_src_name           = str_replace(' ', '_', $file_src_name);

			if ($handle->uploaded)
			{
				$handle->file_new_name_body = $file_src_name . '_original';
				$handle->process($destinationPath);

				if ( $handle->processed )
				{
					$file_url_original  = URL::to($fileLocation . $handle->file_dst_name);
					if ( $file_url_original )
					{
						//$images = Pdf2Image::process($file_url_original);
						$images = Pdf2Image::process(base_path() . $fileLocation, $handle->file_dst_name);

						$tempImage = [];
						foreach ($images as $key => $image) {
							$tempImage[] = URL::to($fileLocation . basename($image) );
						}

						$jsonResponse['data']['images'] = $tempImage;
					}
					$jsonResponse['data']['original'] = $file_url_original;
				}
				else
				{
					$handle->clean();
					$jsonResponse['message'] = $handle->error;
					return $jsonResponse;
				}
			}
			else
			{
				$jsonResponse['message'] = 'Failed to upload file';
				return $jsonResponse;
			}

			$handle->clean();

			$jsonResponse['status'] = 1;
			return $jsonResponse;
		}
		else
		{
			$jsonResponse['message'] = $validator;
		    return $jsonResponse;
		}

		return $jsonResponse;
	}

}
?>
