<?php

class Device extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table      = 'devices';
    protected $primaryKey = 'id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'access_token', 'remember_token');

    public static function deleteObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
				'id' => 'required',
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		self::where('id', '=', $data['id'])->delete();
        //DevicePushNotification::where('device_id', '=', $data['id'])->delete();
        //UserLogin::where('device_id', '=', $data['id'])->delete();

		$returnData['status'] = 1;
		return $returnData;
	}

	public static function createOrUpdateObj($data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$apiInfo = self::where('device_id', '=', $data['device_id'])->first();

		if ( ! $apiInfo ) {

            $validator = Validator::make(
    			$data,
    			array(
                    'device_id' => 'required',
                    'name'      => 'required',
                    'version'   => 'required',
                    'os'        => 'required',
    			),
    			array(
    		    	'same'    => 'The :attribute and :other must match.',
    			    'size'    => 'The :attribute must be exactly :size.',
    			    'between' => 'The :attribute must be between :min - :max.',
    			    'in'      => 'The :attribute must be one of the following types: :values',
    			)
    		);


    		if ($validator->fails())
    		{
    			$messages = $validator->messages();
    			$errorMessage = '';
    			foreach ($messages->all() as $message)
    			{
    				if ( $errorMessage )
    					break;

    				$errorMessage = $message;
    			}

    			$returnData['message'] = $errorMessage;
    			return $returnData;

    		}

			$access_token = DevtabGenerator::accessToken('api_access_token' . time());

            $apiInfo               = new self();
            $apiInfo->access_token = $access_token;
		}

        $optionTableField = ['device_id', 'name', 'version', 'os'];

        foreach ($optionTableField as $key => $field) {

            if ( isset($data[$field])) {

                $apiInfo->$field = $data[$field];
            }
        }

        $apiInfo->app_version = isset($data['app_version']) ? $data['app_version'] : '1.0';
        $apiInfo->save();

		if ( ! $apiInfo->id ) {

			$returnData['message'] = 'can not create api data';
			return $returnData;
		}

		$returnData['status'] = 1;
		$returnData['data'] = $apiInfo;
		return $returnData;
	}


    public static function getDatas($app, $params, $sortField = array('id', 'name', 'version', 'os'))
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( ! is_array($sortField)) {
			$sortField = [];
		}

		if ( isset($params['sort'])) {

			$params['sort'] = urldecode($params['sort']);
			$startOfString = substr($params['sort'], 0, 1);
			if ( in_array($startOfString, ['-', ' ', '+']) ) {
				$params['order'] = $startOfString == '-' ? 'ASC' : 'DESC';
				$params['sort'] = substr($params['sort'], 1);
			}

			$params['sort'] = trim($params['sort']);
		}

        $draw   = isset($params['draw']) ? abs($params['draw']) : 1;
        $offset = isset($params['offset']) ? abs($params['offset']) : 0;
        $limit  = isset($params['limit']) ? abs($params['limit']) : ( isset($app->setting->admin_data_limit_perpage) ? $app->setting->admin_data_limit_perpage : 30 ) ;

		if ( $limit <= 0 || $limit > 1000 ) {
		    $limit = 30;
		}

		if ( ! is_array($sortField)) {
			$sortField = [];
		}
        $sortBy  = 'id';
        $orderBy = 'DESC';
        $sort    = isset($params['sort']) ? $params['sort'] : $sortBy;
        $order   = isset($params['order']) ? $params['order'] : $orderBy;

		if ( isset($sortField[$sort]) )
		{
			$sortBy = $sortField[$sort];
		}
		elseif ( in_array($sort, $sortField) )
		{
			$sortBy = $sort;
		}

        if ( in_array( strtoupper($order) , array('ASC','DESC')) )
        {
            $orderBy = $order;
        }

		//dd($sortBy . ' ' . $orderBy);

		/*
		$dataArrays = DB::table('users as u')
				->select(
					DB::raw(
					'u.*'
					)
				);

		$recordsTotal = $dataArrays->count('u.id');
		$filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataArrays = $dataArrays->where(function($query)
            {
	            global $searchText;
                $query
                ->where('u.name', 'LIKE', '%' . $searchText . '%')
                ->orWhere('u.email', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataArrays->count('u.id');
        }

		$dataArrays = $dataArrays->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->get();

		$data = array();

		foreach ( $dataArrays AS $key => $array )
		{
			if ( is_object($array)) {
			    $array = json_decode(json_encode($array), true);
			}

			$array['DT_RowId'] = $array['id'];
			$data[] = $array;
		}
		*/


        $dataObjs      = self::whereNotNull('id');
        $recordsTotal  = $dataObjs->count('id');
        $filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataObjs = $dataObjs->where(function($query) use ($searchText)
            {
                $query
                ->where('name', 'LIKE', '%' . $searchText . '%');
                //->orWhere('email', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataObjs->count('id');
        }

		$dataObjs = $dataObjs->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->get();

		$data = array();

		foreach ( $dataObjs AS $key => $obj )
		{
			$array = is_object($obj) ? $obj->toArray() : $obj;

            $array['DT_RowId']        = $array['id'];
            $array['updated_at_text'] = MyDatetime::timeElapsedToString($array['updated_at']);
            $data[]                   = $array;
		}


		$data = array('draw' => $draw, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $filteredCount, 'data' => $data);

        $returnData['status'] = 1;
        $returnData['data']   = $data;
		return $returnData;
	}


}
