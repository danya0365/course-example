<?php

class RolesUsers extends Eloquent {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles_users';
	protected $primaryKey = 'id';

	//protected $fillable = array('member_id', 'app_id','status');


	public function user(){

		return $this->belongsTo('User', 'user_id', 'id');
	}

	public function role(){

		return $this->belongsTo('Role', 'role_id', 'id');
	}

}
