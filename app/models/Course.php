<?php

class Course extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table      = 'courses';
    protected $primaryKey = 'id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'access_token', 'remember_token');


    public function category(){

		return $this->belongsTo('Category', 'category_id', 'id')->select( ['id','name'] );
	}

    public function updatedUser(){

		return $this->belongsTo('User', 'updated_by', 'id')->select( ['id','user_name','nick_name'] );
	}

    public function toArray()
    {
        $array = parent::toArray();
        $array['DT_RowId']        = $array['id'];
        if ( isset($array['updated_at'])) {
            $array['updated_at_text'] = MyDatetime::timeElapsedToString($array['updated_at']);
        }

        $array = MyUtility::arrayKeyToString($array);
        return $array;
    }


    public static function selectOrCreateObj($app, $data, $isToArray = 1)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$course = self::whereNotNull('id');

		if ( isset($data['name']) && ! empty($data['name']) ) {
			$course->where('name', '=', trim($data['name']) );
		}

		if ( isset($data['id']) && $data['id'] > 0 ) {
			$course->where('id', '=', trim($data['id']) );
		}

		$course = $course->first();

		if ( ! $course ) {

			$result = self::createOrUpdateObj($app, $data);

			if ( $result['status'] == 1 ) {

                $course = $result['data'];
			}
		}


		if ( ! $course ) {

			$returnData['message'] = 'Can not get $course detail';
			return $returnData;
		}

        $course = $isToArray ? $course->toArray() : $course;

        $returnData['status'] = 1;
        $returnData['data']   = $course;
		return $returnData;
	}

    public static function deleteObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
				'id' => 'required',
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		self::where('id', '=', $data['id'])->delete();

		$returnData['status'] = 1;
		return $returnData;
	}



    private static function __getCategory($app, &$data)
	{
        $data['category_id']           = isset($data['category_id']) ? $data['category_id'] : '';
        $data['category_autocomplete'] = isset($data['category_autocomplete']) ? $data['category_autocomplete'] : '';

		if ( $data['category_id'] && !empty($data['category_autocomplete']) ) {

			$checkData = Category::find($data['category_id']);

			if ( $checkData->name !=  $data['category_autocomplete'] ) {

				$data['category_id'] = '';
			}
		}

		if ( ( $data['category_id'] == '' || !  $data['category_id'] ) && !empty($data['category_autocomplete']) ) {

			$result = Category::selectOrCreateObj($app, ['name' => $data['category_autocomplete']]);

			if ( $result['status'] == 1  ) {

				$data['category_id'] = $result['data']['id'];
			}
		}
	}

	public static function createOrUpdateObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

        $id = isset($data['id']) ? $data['id']: 0;

        $course = null;

        if ( $id ) {
            $course = self::find($id);
        }

        if ( ! $course && isset($data['name'])) {
            $course = self::where('name', '=', $data['name'])->first();
        }

        self::__getCategory($app, $data);

		if ( ! $course ) {


    		$validator = Validator::make(
    			$data,
    			array(
                    'name'              => 'required',
                    'subject'           => 'required',
                    'start_time'        => 'required',
                    'end_time'          => 'required',
                    'number_of_student' => 'required',
                    'category_id'       => 'required'
    			),
    			array(
    		    	'same'    => 'The :attribute and :other must match.',
    			    'size'    => 'The :attribute must be exactly :size.',
    			    'between' => 'The :attribute must be between :min - :max.',
    			    'in'      => 'The :attribute must be one of the following types: :values',
    			)
    		);


    		if ($validator->fails())
    		{
    			$messages = $validator->messages();
    			$errorMessage = '';
    			foreach ($messages->all() as $message)
    			{
    				if ( $errorMessage )
    					break;

    				$errorMessage = $message;
    			}

    			$returnData['message'] = $errorMessage;
    			return $returnData;

    		}


            $startTime = intval( str_replace(':', '', $data['start_time']) );
            $endTime = intval( str_replace(':', '', $data['end_time']) );

            if ( $startTime > $endTime ) {
                $returnData['message'] = '$startTime must less than $endTime';
    			return $returnData;
            }

            $course             = new self();
            $course->created_by = $app->user->id;
		}

        $optionTableField = ['name', 'description', 'category_id', 'subject', 'start_time', 'end_time', 'number_of_student'];

        foreach ($optionTableField as $key => $field) {

            if ( isset($data[$field])) {

                $course->$field = $data[$field];
            }
        }




        $course->updated_by = $app->user->id;
        $course->save();

        if ( ! $course->id ) {

            $returnData['message'] = 'can not create or update $course data';
            return $returnData;
        }

        $returnData['status'] = 1;
        $returnData['data']   = $course;
		return $returnData;
	}

    public static function getDatas($app, $params, $sortField = array('id', 'name', 'subject', 'id', 'id', 'number_of_student', 'updated_by', 'updated_at'))
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( ! is_array($sortField)) {
			$sortField = [];
		}

		if ( isset($params['sort'])) {

			$params['sort'] = urldecode($params['sort']);
			$startOfString = substr($params['sort'], 0, 1);
			if ( in_array($startOfString, ['-', ' ', '+']) ) {
				$params['order'] = $startOfString == '-' ? 'ASC' : 'DESC';
				$params['sort'] = substr($params['sort'], 1);
			}

			$params['sort'] = trim($params['sort']);
		}

        $draw   = isset($params['draw']) ? abs($params['draw']) : 1;
        $offset = isset($params['offset']) ? abs($params['offset']) : 0;
        $limit  = isset($params['limit']) ? abs($params['limit']) : ( isset($app->setting->admin_data_limit_perpage) ? $app->setting->admin_data_limit_perpage : 30 ) ;

		if ( $limit <= 0 || $limit > 1000 ) {
		    $limit = 30;
		}

		if ( ! is_array($sortField)) {
			$sortField = [];
		}
        $sortBy  = 'id';
        $orderBy = 'ASC';
        $sort    = isset($params['sort']) ? $params['sort'] : $sortBy;
        $order   = isset($params['order']) ? $params['order'] : $orderBy;

		if ( isset($sortField[$sort]) )
		{
			$sortBy = $sortField[$sort];
		}
		elseif ( in_array($sort, $sortField) )
		{
			$sortBy = $sort;
		}

        if ( in_array( strtoupper($order) , array('ASC','DESC')) )
        {
            $orderBy = $order;
        }


        $dataObjs      = self::with('category')->with('updatedUser');

        if ( isset($params['name']) && $params['name'] != '' ) {

			$dataObjs->where('name', 'LIKE', '%' . $params['name'] . '%');
		}

        if ( isset($params['time_range']) && $params['time_range'] != '' ) {

            $time_ranges = explode(':', $params['time_range']);
            $time_range = sprintf('%02d', $time_ranges[0]) . ':' . sprintf('%02d', $time_ranges[1]) . ':' . sprintf('%02d', $time_ranges[2]);

            $dataObjs->whereRaw( "CAST(start_time AS char) < '{$time_range}' AND CAST(end_time AS char) > '{$time_range}'");
		}

        $recordsTotal  = $dataObjs->count('id');
        $filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataObjs = $dataObjs->where(function($query) use ($searchText)
            {
                $query->where('name', 'LIKE', '%' . $searchText . '%')
                ->orWhere('description', 'LIKE', '%' . $searchText . '%')
                ->orWhere('subject', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataObjs->count('id');
        }

		$dataObjs = $dataObjs->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->orderBy('id', 'DESC')->get();

		$data = array();

		foreach ( $dataObjs AS $key => $obj )
		{
			$array = is_object($obj) ? $obj->toArray() : $obj;
            $data[]                   = $array;
		}


		$data = array('draw' => $draw, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $filteredCount, 'data' => $data, 'params' => $params);

        $returnData['status'] = 1;
        $returnData['data']   = $data;
		return $returnData;
	}

}
