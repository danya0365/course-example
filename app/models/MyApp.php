<?php

class MyApp {

	public static function getAppByUserLogin()
	{
		$id = null;

		if ( MyAuth::check() ) {

            $id = MyAuth::user()->getId();
		}

		return self::getAppByUserId($id);
	}


	public static function getCountry()
	{
		$geocoder = new \Geocoder\ProviderAggregator();
		$adapter  = new \Ivory\HttpAdapter\CurlHttpAdapter();

		$chain = new \Geocoder\Provider\Chain([
		    new \Geocoder\Provider\FreeGeoIp($adapter),
		    new \Geocoder\Provider\HostIp($adapter),
		]);

		$geocoder->registerProvider($chain);

		$address   = $geocoder->geocode( Request::getClientIp() )->first();

		// $address is an instance of Address
		$formatter = new \Geocoder\Formatter\StringFormatter();

		//$formatter->format($address, '%S %n, %z %L');
		// 'Badenerstrasse 120, 8001 Zuerich'

		//$formatter->format($address, '<p>%S %n, %z %L</p>');
		// '<p>Badenerstrasse 120, 8001 Zuerich</p>'

		return $formatter->format($address, '%C');
	}

	public static function getAppByUserId($userId)
	{
		$app = new stdClass;
        $app->isLogedin = false;
        $settingObjs = Setting::all();
        $app->setting = new stdClass;

        foreach ( $settingObjs AS $obj )
        {
	        $app->setting->{$obj->var} = $obj->value;
        }

		if ( $userId ) {

		    $app->user = User::with('roles')->find($userId);

			//dd($userId);
			//dd($app->user);

		    if ( $app->user )
		    {
				$app->user->permission = new stdClass;

	            if ( $app->user->roles )
	            {
		            foreach ($app->user->roles as $key => $roleObj ) {

						$roleInfo = $roleObj->getAttributes();

						foreach ($roleInfo as $key => $value) {

							if ( is_numeric($value)) {

								if ( ! isset($app->user->permission->$key) OR $app->user->permission->$key == 0 )
								{
									$app->user->permission->$key = $value;
								}
							}
						}
		            }
	            }

	            $app->isLogedin = true;
            }
		}




		if ( ! $app->isLogedin )
		{
			//MyAuth::logout();

            $app->user = new stdClass;
            $app->user->id = 0;
            $app->user->name = 'Unregistered';
            $app->user->permission = new stdClass;
            $guestRoleObj = Role::where('is_guest', '=', 1)->first();
            $guestRoleInfo = $guestRoleObj->getAttributes();
            foreach ($guestRoleInfo as $key => $value) {

				if ( is_numeric($value)) {

					if ( ! isset($app->user->permission->$key) OR $app->user->permission->$key == 0 )
					{
						$app->user->permission->$key = $value;
					}
				}
			}
		}

		/*
		$country = Session::get('country');
		if ( $country ) {
			$app->country = $country;
		}
		else {
			$app->country = self::getCountry();
			Session::put('country', $app->country);
		}
		*/
        return $app;
	}
}
