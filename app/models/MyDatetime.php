<?php

class MyDatetime {


	public static function getQuarterDatetimeByQuarterNumberAndYear($quarterNumber, $quarterYear)
	{
		$monthQuarterNumber = [1 => 1, 2 => 4, 3 => 7, 4 => 10];
		$month = isset($monthQuarterNumber[ $quarterNumber ]) ? $monthQuarterNumber[ $quarterNumber ] : 1;
		return date('Y-m-d H:i:s', mktime(0, 0, 0, $month, 1, $quarterYear) );
	}

	public static function getQuarterDatetimeByMonthAndYear($quarterMonth, $quarterYear)
	{
		$quarterNumber = MyDatetime::getQuarterNumber( $quarterMonth );

		return self::getQuarterDatetimeByQuarterNumberAndYear($quarterNumber, $quarterYear);
	}

	public static function getQuarterNumber($currentMonth)
	{
		if ( $currentMonth >= 1 && $currentMonth <= 3) {
			return 1;
		}
		if ( $currentMonth >= 4 && $currentMonth <= 6) {
			return 2;
		}
		if ( $currentMonth >= 7 && $currentMonth <= 9) {
			return 3;
		}
		if ( $currentMonth >= 10 && $currentMonth <= 12) {
			return 4;
		}
		return 1;
	}

	public static function getCurrentQuarterNumber()
	{
		return self::getQuarterNumber(date('n'));
	}

	public static function getQuarteryDatetime($datetime)
	{

		$datetime = ( ! is_numeric($datetime) ? strtotime($datetime) : $datetime );

		$month = date('n', $datetime );

		$quarterYear = date('Y', $datetime);
		$quarterNumber = self::getQuarterNumber($month);

		return ['number' => $quarterNumber, 'year' => $quarterYear];
	}

	public static function timeElapsedToString($timestamp)
	{
		if ( ! is_numeric($timestamp) )
			$timestamp = strtotime($timestamp);

		if ( $timestamp <= 0 ) {
		    return '-';
		}
		$timediff = time() - $timestamp;

		if ( $timediff <= 0 ) return 'ตอนนี้';

		$stringMap = array(
            60 * 60 * 24 * 365 => 'ปี',
            60 * 60 * 24 * 30  => 'เดือน',
            60 * 60 * 24       => 'วัน',
            60 * 60            => 'ชั่วโมง',
            60                 => 'นาที',
            1                  => 'วินาที'
		);

		foreach ( $stringMap AS $seconds => $string )
		{
			$d = $timediff / $seconds;
			if ( $d >= 1 )
			{
				$r = round($d);
				return $r . ' ' . $string . 'ที่แล้ว';
			}
		}
		return '-';
	}

	public static function thaiMonthYear($timeStamp)
	{
		$monthTh = array(
            1  => 'ม.ค',
            2  => 'ก.พ',
            3  => 'มี.ค',
            4  => 'เม.ย',
            5  => 'พ.ค',
            6  => 'มิ.ย',
            7  => 'ก.ค',
            8  => 'ส.ค',
            9  => 'ก.ย',
            10 => 'ตุ.ค',
            11 => 'พ.ย',
            12 => 'ธ.ค',
		);

		$monthNumber = date('n', $timeStamp);
		$yearNumber = date('Y', $timeStamp) + 543;
		return $monthTh[ $monthNumber ] . ' ' . $yearNumber;
	}


	public static function thaiDayMonthShortYear($timeStamp)
	{
		$monthTh = array(
            1  => 'ม.ค',
            2  => 'ก.พ',
            3  => 'มี.ค',
            4  => 'เม.ย',
            5  => 'พ.ค',
            6  => 'มิ.ย',
            7  => 'ก.ค',
            8  => 'ส.ค',
            9  => 'ก.ย',
            10 => 'ตุ.ค',
            11 => 'พ.ย',
            12 => 'ธ.ค',
		);

		$dayNumber = date('j', $timeStamp);
		$monthNumber = date('n', $timeStamp);
		$yearNumber = date('Y', $timeStamp) + 543;
		return $dayNumber . ' ' . $monthTh[ $monthNumber ] . ' ' . substr($yearNumber, 2, 2);
	}

	public static function thaiDayMonthYear($timeStamp)
	{
		$monthTh = array(
            1  => 'ม.ค',
            2  => 'ก.พ',
            3  => 'มี.ค',
            4  => 'เม.ย',
            5  => 'พ.ค',
            6  => 'มิ.ย',
            7  => 'ก.ค',
            8  => 'ส.ค',
            9  => 'ก.ย',
            10 => 'ตุ.ค',
            11 => 'พ.ย',
            12 => 'ธ.ค',
		);

		$dayNumber = date('j', $timeStamp);
		$monthNumber = date('n', $timeStamp);
		$yearNumber = date('Y', $timeStamp) + 543;
		return $dayNumber . ' ' . $monthTh[ $monthNumber ] . ' ' . $yearNumber;
	}

}
