<?php

class DevtabGenerator {

	public static function accessToken($userId)
	{
		return md5(uniqid( time(), true) . '_access_token_' . $userId);
	}

	public static function confirmEmail($userId)
	{
		return md5(uniqid( time(), true) . '_confirm_email_' . $userId);
	}

	public static function requestNewPassword($userId)
	{
		return md5(uniqid( time(), true) . '_set_new_password_' . $userId);
	}

	/**
	 *
	 * Generate v4 UUID
	 *
	 * Version 4 UUIDs are pseudo-random.
	 */
	public static function v4()
	{
	    return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

	    // 32 bits for "time_low"
	    mt_rand(0, 0xffff), mt_rand(0, 0xffff),

	    // 16 bits for "time_mid"
	    mt_rand(0, 0xffff),

	    // 16 bits for "time_hi_and_version",
	    // four most significant bits holds version number 4
	    mt_rand(0, 0x0fff) | 0x4000,

	    // 16 bits, 8 bits for "clk_seq_hi_res",
	    // 8 bits for "clk_seq_low",
	    // two most significant bits holds zero and one for variant DCE1.1
	    mt_rand(0, 0x3fff) | 0x8000,

	    // 48 bits for "node"
	    mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
	    );
	}

	public static function sCBPaymentCode($member_id, $getztar_id = '')
	{
		$byteArray = unpack('C*', $getztar_id);

		$byteArray = array_values($byteArray);

		//var_dump($byteArray);

		//echo $byteArray[0];
		//exit;

		$stringCode = $member_id;

		$countByte = count($byteArray);
		$count = 0;

		while ( strlen($stringCode) < 10 )
		{
			if ( $countByte > $count )
			{
				$stringCode .= $byteArray[$count];
				$count++;
				//echo '1';
			}

			if ( $count == $countByte )
			{
				$stringCode .= time();
				//echo '2 ';
			}
		}

		return substr($stringCode, 0, 10);
	}
}
?>
