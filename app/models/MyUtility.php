<?php

class MyUtility
{
    /*
    public static function castToString($item, $key)
    {
        $item = (string)$item;
    }

    public static function arrayKeyToString($array)
    {
        array_walk_recursive($array, 'self::castToString');

        return $array;
    }
    */

    public static function arrayKeyToString($array) {

        if ( is_array($array) ) $array = (array) $array;

        if( is_array($array) ) {
            $new = array();
            foreach($array as $key => $val) {

                $new[$key] = self::arrayKeyToString($val);
            }
        }
        else
            $new = (string)$array;
        return $new;
    }

    public static function objectToArray($obj) {

        if (is_object($obj) ) $obj = (array) $obj;

        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = self::objectToArray($val);
            }
        }
        else
            $new = $obj;
        return $new;
    }

    public static function replaceWidthToMaxWidth($text)
    {
        return preg_replace("/(<img (src=(.*?)) (.*?(style=(.*?))?)\/?>)/", "<img $2 style=\"max-width: 100%\" />", $text);
    }


    public static function importXls($app)
    {
        if (Input::hasFile('import'))
        {
            $file = Input::file('import');

            $path = $file->getRealPath();
            //$extension = $file->getClientOriginalExtension();
            //$mime = $file->getMimeType();


            $fileName = str_replace( ['_','-','.',' '] , '', microtime());
            $yearMonth = date('Y-m');
            $fileLocation = '/uploads/import/' . $yearMonth . '/';
			$destinationPath = base_path() . $fileLocation;

            /*
            $movedResult = $file->move($destinationPath, $fileName);

            if ( ! $movedResult ) {
                dd('Moved Error');
            }
            */

            $results = [];

            Excel::load($path, function($reader) use ($path, &$results) {



                // Loop through all sheets
                $reader->each(function($sheet) use (&$results) {

                    // Loop through all rows
                    $sheet->each(function($row) use (&$results) {

                        $results[] = $row->toArray();
                    });
                });

                //dd($results);

                File::delete($path);

            });


            $users = [];
            $userCodes = [];

            $orderDates = [];

            $userCodeIndex = 1;
            $userCodeValue = '';

            $wsNameIndex = 2;
            $wsNameValue = '';

            $mamNameIndex = 3;
            $mamNameValue = '';

            $orderDateIndex = 4;

            $quarterNumberIndex = 5;
            $quarterNumberValue = 0;
            $quarterYearIndex = 6;
            $quarterYearValue = 0;


            $aquiredPointBrand1Index = 7;
            $aquiredPointBrand2Index = 8;
            $aquiredPointBrand3Index = 9;
            $aquiredPointBrand4Index = 10;

            $aquiredPointIndex = 11;
            $aquiredPointValue = 0;

            $totalPaidBrand1Index = 12;
            $totalPaidBrand2Index = 13;
            $totalPaidBrand3Index = 14;
            $totalPaidBrand4Index = 15;

            $totalPaidIndex = 16;
            $totalPaidValue = 0;

            foreach ($results as $key => $row) {

                //dd($row);

                $userCode = '';
                $userId = '';
                $orderingDate = '';
                $totalPaid = 0;
                $totalPaidBrand1 = 0;
                $totalPaidBrand2 = 0;
                $totalPaidBrand3 = 0;
                $totalPaidBrand4 = 0;

                if ( isset($row[$userCodeIndex])) {

                    if ( strlen($row[$userCodeIndex]) == 6 ) {

                        $userCode = $row[$userCodeIndex];
                        $userCodes[] = $userCode;
                    }
                }

                if ( $userCode != '' ) {
                    $getUser = User::where('code', '=', $userCode)->first();
                    $userId = $getUser ? $getUser->id : null;
                }


                if ( $userId ) {

                    if ( isset($row[$orderDateIndex])) {

                        if ( strlen($row[$orderDateIndex]) == 8 ) {

                            $orderingDate = $row[$orderDateIndex];

                            if ( ! isset($orderDates[$orderingDate]) ) {

                                list($day, $month, $year) = explode('-', $orderingDate);

                                $orderingDate = mktime(0, 0, 0, $month, $day, $year);
                                $orderingDate = date('Y-m-d', $orderingDate);

                                $orderDates[ $row[$orderDateIndex] ] = $orderingDate;
                            }
                            else {
                                $orderingDate = $orderDates[$orderingDate];
                            }
                        }
                    }

                    $aquiredPointBrand1Value = $row[ $aquiredPointBrand1Index ];
                    $aquiredPointBrand2Value = $row[ $aquiredPointBrand2Index ];
                    $aquiredPointBrand3Value = $row[ $aquiredPointBrand3Index ];
                    $aquiredPointBrand4Value = $row[ $aquiredPointBrand4Index ];

                    $totalPaidBrand1 = $row[$totalPaidBrand1Index];
                    $totalPaidBrand2 = $row[$totalPaidBrand2Index];
                    $totalPaidBrand3 = $row[$totalPaidBrand3Index];
                    $totalPaidBrand4 = $row[$totalPaidBrand4Index];

                    $quarterNumberValue = $row[$quarterNumberIndex];
                    $quarterYearValue = $row[ $quarterYearIndex ];

                    $mamNameValue = $row[ $mamNameIndex ];
                    $wsNameValue = $row[ $wsNameIndex ];
                    $aquiredPointValue = $row[ $aquiredPointIndex ];
                    $totalPaidValue = $row[ $totalPaidIndex ];

                    $quarterYearValue = $row[ $quarterYearIndex ];

                    //dd($quarterYearValue);

                    $quarterMonth = ( ($quarterNumberValue -1) * 3 ) + 1;

                    //dd($quarterMonth);

                    $orderingAtQuarterDatetime = MyDatetime::getQuarterDatetimeByMonthAndYear($quarterMonth, $quarterYearValue);

                    //dd($orderingAtQuarterDatetime);

                    UserPayment::where('ordering_at_quarter_datetime', '=', $orderingAtQuarterDatetime)->where('user_id', '=', $userId)->delete();

                    UserPayment::createOrUpdateObj(
                        $app, ['user_id' => $userId, 'brand_id' => 1, 'ordering_date' => $orderingDate, 'total_paid' => $totalPaidBrand1, 'aquired_point' => $aquiredPointBrand1Value, 'status' => 'approved']
                    );
                    UserPayment::createOrUpdateObj(
                        $app, ['user_id' => $userId, 'brand_id' => 2, 'ordering_date' => $orderingDate, 'total_paid' => $totalPaidBrand2, 'aquired_point' => $aquiredPointBrand2Value, 'status' => 'approved']
                    );
                    UserPayment::createOrUpdateObj(
                        $app, ['user_id' => $userId, 'brand_id' => 3, 'ordering_date' => $orderingDate, 'total_paid' => $totalPaidBrand3, 'aquired_point' => $aquiredPointBrand3Value, 'status' => 'approved']
                    );
                    UserPayment::createOrUpdateObj(
                        $app, ['user_id' => $userId, 'brand_id' => 4, 'ordering_date' => $orderingDate, 'total_paid' => $totalPaidBrand4, 'aquired_point' => $aquiredPointBrand4Value, 'status' => 'approved']
                    );

                    if ( strpos( strtolower($wsNameValue), 'michelin') > -1 || strpos($wsNameValue, 'มิชลิน') > -1 ) {
                        $wsNameValue = 'Michelin';
                    }

                    UserPaymentSummary::createOrUpdateObj(
                        $app, ['user_id' => $userId, 'dealer_autocomplete' => $wsNameValue, 'retailer_autocomplete' => $wsNameValue, 'status' => 1, 'quarter_month' => $quarterMonth, 'quarter_year' => $quarterYearValue, 'aquired_point' => $aquiredPointValue, 'total_paid' => $totalPaidValue, 'paid_at' => $orderingAtQuarterDatetime

                    ]);

                    //dd($row);

                }

            }


            return $results;

        }

        return [];
    }


}


 ?>
