<?php

class Role extends Eloquent {



	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'roles';
	protected $primaryKey = 'id';

	//protected $fillable = array('member_id', 'app_id','status');


	public function users(){

		return $this->belongsToMany('User', 'roles_users', 'role_id', 'user_id');
	}

	public function updatedUser(){

		return $this->belongsTo('User', 'updated_by', 'id')->select( ['id','user_name','nick_name'] );
	}

	public static function updateObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
                'id'   => 'required',
                'name' => 'required'
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			/*
			foreach ($messages->get('email') as $message)
			{
				//
			}
			*/

			$returnData['message'] = $errorMessage;
			return $returnData;

		}


        $id   = $data['id'];
        $name = $data['name'];

		$checkRole = self::
						where('id', '!=', $id)
						->where(function($query) use ($name)
			            {
			                $query->where('name', 'LIKE', '%' . $name . '%');
			            })
						->first();

		if ( $checkRole )
		{
			if ( $checkUser->name == $name )
			{
				$returnData['message'] = 'ชื่อ ' . $name . ' ถูกใช้ไปแล้ว';
				return $returnData;
			}
		}

		$role = self::find($id);

		if ( $role )
		{
			$role->name = $name;


            $role->is_can_manage_user    = $data['is_can_manage_user'];
            $role->is_can_manage_role    = $data['is_can_manage_role'];
            $role->is_can_manage_setting = $data['is_can_manage_setting'];
            $role->is_can_update_profile = $data['is_can_update_profile'];
            $role->is_can_create_course  = $data['is_can_create_course'];
            $role->is_can_view_course    = $data['is_can_view_course'];
            $role->updated_by            = $app->user->id;
			$role->save();

			$returnData['status'] = 1;
			$returnData['data'] = $role->getAttributes();
			return $returnData;
		}

		$returnData['message'] = 'Role not found';
		return $returnData;
	}

	public static function getDatas($app, $params, $sortField = array('id', 'name', 'is_admin', 'is_member', 'is_guest', 'updated_by', 'updated_at'))
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( ! is_array($sortField)) {
			$sortField = [];
		}

		if ( isset($params['sort'])) {

			$params['sort'] = urldecode($params['sort']);
			$startOfString = substr($params['sort'], 0, 1);
			if ( in_array($startOfString, ['-', ' ', '+']) ) {
				$params['order'] = $startOfString == '-' ? 'ASC' : 'DESC';
				$params['sort'] = substr($params['sort'], 1);
			}

			$params['sort'] = trim($params['sort']);
		}

        $draw   = isset($params['draw']) ? abs($params['draw']) : 1;
        $offset = isset($params['offset']) ? abs($params['offset']) : 0;
        $limit  = isset($params['limit']) ? abs($params['limit']) : ( isset($app->setting->admin_data_limit_perpage) ? $app->setting->admin_data_limit_perpage : 30 ) ;

		if ( $limit <= 0 || $limit > 1000 ) {
		    $limit = 30;
		}

		if ( ! is_array($sortField)) {
			$sortField = [];
		}
        $sortBy  = 'id';
        $orderBy = 'DESC';
        $sort    = isset($params['sort']) ? $params['sort'] : $sortBy;
        $order   = isset($params['order']) ? $params['order'] : $orderBy;

		if ( isset($sortField[$sort]) )
		{
			$sortBy = $sortField[$sort];
		}
		elseif ( in_array($sort, $sortField) )
		{
			$sortBy = $sort;
		}

		if ( in_array( strtoupper($order) , array('ASC','DESC')) )
		{
			$orderBy = $order;
		}

		//dd($sortBy . ' ' . $orderBy);

		/*
		$dataArrays = DB::table('users as u')
				->select(
					DB::raw(
					'u.*'
					)
				);

		$recordsTotal = $dataArrays->count('u.id');
		$filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataArrays = $dataArrays->where(function($query)
            {
	            global $searchText;
                $query
                ->where('u.name', 'LIKE', '%' . $searchText . '%')
                ->orWhere('u.email', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataArrays->count('u.id');
        }

		$dataArrays = $dataArrays->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->get();

		$data = array();

		foreach ( $dataArrays AS $key => $array )
		{
			if ( is_object($array)) {
			    $array = json_decode(json_encode($array), true);
			}

			$array['DT_RowId'] = $array['id'];
			$data[] = $array;
		}
		*/


        $dataObjs      = self::with('updatedUser');
        $recordsTotal  = $dataObjs->count('id');
        $filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataObjs = $dataObjs->where(function($query) use ($searchText)
            {
                $query
                ->where('name', 'LIKE', '%' . $searchText . '%');
                //->orWhere('email', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataObjs->count('id');
        }

		$dataObjs = $dataObjs->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->get();

		$data = array();

		foreach ( $dataObjs AS $key => $obj )
		{
			$array = is_object($obj) ? $obj->toArray() : $obj;

            $array['DT_RowId']        = $array['id'];
            $array['updated_at_text'] = MyDatetime::timeElapsedToString($array['updated_at']);
            $data[]                   = $array;
		}


		$data = array('draw' => $draw, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $filteredCount, 'data' => $data);

        $returnData['status'] = 1;
        $returnData['data']   = $data;
		return $returnData;
	}


}
