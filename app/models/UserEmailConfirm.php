<?php

class UserEmailConfirm extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_email_confirms';

	protected $primaryKey = 'user_id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');

	//protected $fillable = array('code', 'expired_at', 'user_id');

	public function user(){
		return $this->belongsTo('User', 'user_id', 'id');
	}

}
