<?php


/**
 *
 */
class MyAuth
{

	public static function check()
	{
		return ! is_null(self::user());
	}


	public static function guest()
	{
		return ! self::check();
	}


	public static function user($_access_token = null)
	{
        $_user = self::__getSession( self::__getLoginKey() );


        if ( empty($_user)) {

			if ( empty($_access_token) )
            	$_access_token = self::getAccessToken();

			if ( ! empty($_access_token)) {

				$userLogin = UserLogin::select('user_id')->where('user_access_token', '=', $_access_token)->first();

				if ( $userLogin ) {

					$_user = User::find($userLogin->user_id);

					if ( $_user ) {

						self::__setSession(self::__getLoginKey(), $_user);
					}
					else
					{
						self::__deleteAccessTokenKey();
					}
				}
				else
				{
					self::__deleteAccessTokenKey();
				}
			}

        }
		return $_user;
	}

	private static function __deleteAccessTokenKey()
	{
		self::__deleteSession(self::__getAccessTokenKey());
		self::__deleteCookie(self::__getAccessTokenKey());
	}


	public static function getUserWithAccessToken($accessToken)
	{
		$responseData = array('status' => 0, 'message' => '', 'data' => array());
		$accessToken = trim($accessToken);

		if ( empty($accessToken) )
		{
			$responseData['message'] = 'User Access Token Not Found';
			return $responseData;
		}

		$userLogin = UserLogin::select('user_id')->where('user_access_token', '=', $accessToken)->first();

		if ( ! $userLogin )
		{
			$responseData['message'] = 'User Not Found';
			return $responseData;
		}


        $user                   = User::find($userLogin->user_id);
        $responseData['status'] = 1;
        $responseData['data']   = $user;
		return $responseData;
	}

	public static function doLoginWithAccessToken($app, $accessToken)
	{
		$userLogin = UserLogin::select('user_id')->where('user_access_token', '=', $accessToken)->first();

		return self::__doLoginWithUserId($app, $userLogin->user_id);
	}

	public static function doLoginWithUserId($app, $userId)
	{
		$user = User::find($userId);
		if ( empty($user) )
			return false;

		$post = $user->toArray();

		return self::__doLogin($app, $post, false);
	}

	private static function __doLogin($app, $post, $remember = true, $deviceInfo = null)
	{
		if ( ! $deviceInfo )
		{
			$device_id = Request::getClientIp();
			$result = Device::createOrUpdateObj(['device_id' => $device_id, 'name' => 'Default', 'version' => '1.0', 'os' => 'WebBrowser']);

			if ( $result['status'] == 1 ) {

				$deviceInfo = $result['data'];
			}

			if ( ! $deviceInfo) {
				exit('! $deviceInfo');
				return false;
			}
		}

		$userId = isset($post['id']) ? $post['id'] : null ;

		if ( ! $userId ) {

            $email    = isset($post['email']) ? $post['email'] : '';
			$user_name    = isset($post['user_name']) ? $post['user_name'] : '';
            $password = $post['password'];

			$findUser = null;

			if ( $email )
				$findUser = User::select( array('id', 'password') )->where('email', '=', $email)->first();

			if ( $user_name )
				$findUser = User::select( array('id', 'password') )->where('user_name', '=', $user_name)->first();

			if ( ! $findUser )
			{
				return false;
			}


			if ( ! Hash::check($password, $findUser->password) )
			{
				return false;
			}

			$userId = $findUser->id;
		}

		UserLogin::where('device_id', '=', $deviceInfo->id)->where('user_id', '!=', $userId )->delete();
		$userLogin = UserLogin::where('device_id', '=', $deviceInfo->id)->where('user_id', '=', $userId )->first();

		if ( $userLogin )
		{
			$time_now = time();
			$access_token_expired_timestamp = strtotime($userLogin->expired_at);

			if ( $time_now > $access_token_expired_timestamp ) {

                $userLogin->user_access_token = DevtabGenerator::accessToken('user_login_' . $deviceInfo->id . $userId);
                $userLogin->expired_at        = date($app->setting->datetime_database_timestamp_format, time()+$app->setting->user_access_token_expired_timestamp );
				$userLogin->save();
			}
		}

		if ( ! $userLogin )
		{
            $userLogin                    = new UserLogin;
            $userLogin->device_id         = $deviceInfo->id;
            $userLogin->user_id           = $userId;
            $userLogin->user_access_token = DevtabGenerator::accessToken('user_login_' . $deviceInfo->id . $userId);
            $userLogin->expired_at        = date($app->setting->datetime_database_timestamp_format, time()+$app->setting->user_access_token_expired_timestamp );
			$userLogin->save();

		}

		self::__setSession(self::__getAccessTokenKey(), $userLogin->user_access_token );

		if ( $remember ) {

			$result = self::__setCookie(self::__getAccessTokenKey(), $userLogin->user_access_token );
		}
		return $userLogin;
	}

	private static function __setSession($key, $value)
	{
		return Session::put($key, $value);
	}

	private static function __getSession($key)
	{
		return Session::get($key);
	}

	private static function __deleteSession($key)
	{
		return Session::forget($key);
	}


	private static function __setCookie($key, $value)
	{
		//return $cookie = Cookie::forever($key, $value);
		return Cookie::queue($key, $value, 60 * 24 * 365);
	}

	private static function __getCookie($key)
	{
		return Cookie::get($key);
	}

	private static function __deleteCookie($key)
	{
		return Cookie::forget($key);
	}


    public static function getAccessToken()
    {

        $session_access_token = self::__getSession( self::__getAccessTokenKey() );

        if ( $session_access_token ) {
            return $session_access_token;
        }


        $cookie_access_token = self::__getCookie( self::__getAccessTokenKey() );
        if ( $cookie_access_token ) {
            return $cookie_access_token;
        }

        return null;
    }

	private static function __getLoginKey()
	{
		return 'login_user_'.md5(get_called_class());
	}

	private static function __getAccessTokenKey()
	{
		return 'login_access_token'.md5(get_called_class());
	}

	public static function logout($access_token = '')
	{
		if (  empty($access_token)) {

			$access_token = self::getAccessToken();
		}


		if ( $access_token) {

			$userLogin = UserLogin::where('user_access_token', '=', $access_token)->first();
			if ( $userLogin )
			{
				$userLogin->delete();
			}
		}

		self::__deleteSession( self::__getAccessTokenKey() );
		self::__deleteCookie( self::__getAccessTokenKey() );
		self::__deleteSession( self::__getLoginKey() );

		return true;
	}

	public static function userLogin($app, $post, $remember, $deviceInfo)
	{
		$responseData = array('status' => 0, 'message' => '', 'data' => new stdClass);

		//$post = array_map('urldecode', $post);

		$rules = array(
            //'email'    => 'required|email', // make sure the email is an actual email
			//'user_code' => 'required',
            'password' => 'required' // password can only be alphanumeric and has to be greater than 3 characters
		);

		$messages = array(
            //'email.required'    => 'กรุณาพิมพ์อีเมล์!',
            //'email.email'       => 'รูปแบบอีเมล์ผิด!',
			//'user_code.required' => 'กรุณากรอกรหัสผู้ใช้งาน',
            'password.required' => 'กรุณาพิมพ์รหัสผ่าน!',
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules, $messages);

		if ($validator->fails())
		{

			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$responseData['message'] = $errorMessage;
			return $responseData;

		}

		$userLogin = self::__doLogin($app, $post, $remember, $deviceInfo);

		if ( empty($userLogin) )
		{
			$responseData['message'] = 'ไม่สามารถเข้าสู่ระบบได้';
			return $responseData;
		}

		$responseData['status'] = 1;
		$responseData['data'] = $userLogin;
		return $responseData;
	}


	public static function userLogout($app, $user_access_token)
	{
		$responseData = array('status' => 0, 'message' => '', 'data' => array());

		self::logout($user_access_token);

		$responseData['status'] = 1;
		return $responseData;
	}


}



 ?>
