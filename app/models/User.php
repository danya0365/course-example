<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use SammyK\LaravelFacebookSdk\FacebookableTrait;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, FacebookableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey = 'id';
	public $real_password;

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public function roles(){

		return $this->belongsToMany('Role', 'roles_users', 'user_id', 'role_id');
	}

	public function updatedUser(){

		return $this->belongsTo('User', 'updated_by', 'id')->select( ['id','user_name','nick_name'] );
	}

	public function userLogin(){
		return $this->hasMany('UserDevice', 'user_id', 'id');
	}


	public function toArray()
    {
        $array = parent::toArray();
        $array['DT_RowId']        = $array['id'];

		if ( isset($array['updated_at'])) {

			$array['updated_at_text'] = MyDatetime::timeElapsedToString($array['updated_at']);
		}


		if ( ! empty($this->real_password) ) {
			$array['real_password'] = $this->real_password;
		}
		$array = MyUtility::arrayKeyToString($array);
        return $array;
    }

	public function getAge()
	{
		if ( empty($this->birthday) ) {
			return -1;
		}

		if ( strtotime($this->birthday) <= 0 ) {
			return -1;
		}

		list($year, $month, $day) = explode("-", $this->birthday);

		$age = ( date("md", date("U", mktime(0, 0, 0, $month, $day, $year))) > date("md") ? ((date("Y") - $year) - 1) : (date("Y") - $year));

		return $age;
	}

	public function getId()
	{
	  return $this->id;
	}

	public function getAuthPassword() {

    	return $this->password;
	}

	public static function deleteObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
                'id' => 'required',
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			/*
			foreach ($messages->get('email') as $message)
			{
				//
			}
			*/

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		if ( $data['id'] == $app->user->id ) {
			$returnData['message'] = 'ไม่สามารถลบตัวคุณเองได้';
			return $returnData;
		}

		self::where('id', '=', $data['id'])->delete();
		RolesUsers::where('user_id', '=', $data['id'])->delete();
		UserLogin::where('user_id', '=', $data['id'])->delete();

		$returnData['status'] = 1;
		return $returnData;
	}


	public static function updateProfile($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
                'id' => 'required',
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		$profileFields = [ 'birthday', 'first_name', 'last_name', 'nick_name'];


        $id   = $data['id'];
        $name = isset($data['user_name']) ? $data['user_name'] : '';

		if ( ! empty($name) ) {

			$checkUser = User::
							where('id', '!=', $id)
							->where(function($query) use ($name)
							{
								$query->where('user_name', 'LIKE', $name );
							})
							->first();

			if ( $checkUser )
			{

				if ( $checkUser->name == $name )
				{
					$returnData['message'] = 'ชื่อ ' . $name . ' ถูกใช้ไปแล้ว';
					return $returnData;
				}

			}

		}

		$user = User::find($id);

		if ( $user )
		{
			foreach ( $profileFields as $key => $field) {

				if ( isset($data[$field])) {

                    $value        = $data[$field];
                    $user->$field = $value;
				}
			}

            $user->updated_by = $app->user->id;
			$user->save();

            $returnData['status'] = 1;
            $returnData['data']   = $user->toArray();
			return $returnData;
		}

		$returnData['message'] = 'User not found';
		return $returnData;
	}

	public static function changePassword($app, $data, $toArray = true)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());
		$rules = array(
            'id'                   => 'required',
            'new_password'         => 'required|same:new_password_confirm|min:' . $app->setting->user_password_min,
            'new_password_confirm' => 'required'
		);

		$messages = array(
            'new_password.required'         => 'กรุณาพิมพ์รหัสผ่านใหม่',
            'new_password.same'             => 'รหัสผ่านไม่ตรงกัน',
            'new_password_confirm.required' => 'กรุณาพิมพ์ยืนยันรหัสผ่านใหม่อีกครั้ง',
		);

		$validator = Validator::make($data, $rules, $messages);

		if ( $validator->fails()) {

			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$returnData['message'] = $errorMessage;
			return $returnData;
		}

        $password = isset($data['new_password']) ? $data['new_password'] : '';
        $id       = $data['id'];
        $user     = User::find($id);

		if ( $user )
		{
            $user->password   = Hash::make($password);
            $user->updated_by = $app->user->id;
			$user->save();

			$returnData['status'] = 1;
			$returnData['message'] = 'ระบบได้ทำการเปลี่ยนรหัสผ่านเรียบร้อย';
			$returnData['data'] = $toArray ? $user->toArray() : $user;
			return $returnData;
		}

		$returnData['message'] = 'User not found';
		return $returnData;
	}

	public static function updateObj($app, $data, $roles, $toArray = true)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
                'id'    => 'required'
			),
			array(
                'same'    => 'The :attribute and :other must match.',
                'size'    => 'The :attribute must be exactly :size.',
                'between' => 'The :attribute must be between :min - :max.',
                'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			/*
			foreach ($messages->get('email') as $message)
			{
				//
			}
			*/

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		$userFields = ['birthday', 'first_name', 'last_name', 'nick_name'];


        $id    = $data['id'];
        $name  = isset($data['user_name']) ? $data['user_name'] : '';
		$password = isset($data['password']) ? $data['password'] : '';

		$checkUser = User::
						where('id', '!=', $id)
						->where(function($query) use ($name)
			            {
			                $query->where('user_name', '=', $name );
			            })
						->first();

		if ( $checkUser )
		{

			if ( $name && $checkUser->name == $name )
			{
				$returnData['message'] = 'ชื่อ ' . $name . ' ถูกใช้ไปแล้ว';
				return $returnData;
			}
		}

		if ( ! empty($password) ) {

			if ( mb_strlen($password) < $app->setting->user_password_min ) {

				$returnData['message'] = 'รหัสผ่านต้องมีความยาวมากกว่าหรือเท่ากับ ' . number_format($app->setting->user_password_min) . ' ตัวอักษร';
				return $returnData;
			}
		}

		$user = User::find($id);

		if ( $user )
		{
			if ( ! empty($password) )
				$user->password = Hash::make( $password );

			foreach ( $userFields as $key => $field) {

				if ( isset($data[$field])) {

                    $value        = $data[$field];
                    $user->$field = $value;
				}
			}

            $user->updated_by         = $app->user->id;
			$user->save();

			//dd($roles);

			if ( count($roles)) {
				$rolesUpdated = self::updateRoles($app, $user->id, $roles);
			}


			$returnData['status'] = 1;
			$returnData['data'] = $toArray ? $user->toArray() : $user;
			return $returnData;
		}

		$returnData['message'] = 'User not found';
		return $returnData;
	}

	public static function register($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
				'password' => 'confirmed',
			),
			array(
				'same'    => 'The :attribute and :other must match.',
				'size'    => 'The :attribute must be exactly :size.',
				'between' => 'The :attribute must be between :min - :max.',
				'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}
			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		return self::createObj($app, $data, [2], true);
	}


	public static function createObj($app, $data, $roles = [2], $toArray = true, $isFacebookLoggedIn = false )
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( is_array($roles)) {
			foreach($roles as $key => $value)
			{
			    if(is_null($value) || $value == '')
			        unset($roles[$key]);
			}
			array_values($roles);
		}

		if ( ! is_array($roles) || count($roles) <= 0 ) {
			$roles = [2];
		}

		$validator = Validator::make(
			$data,
			array(
				'user_name'		=>	'required',
                'password' 	=> 'required|min:' . $app->setting->user_password_min,
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}
			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		$userFields = ['birthday', 'first_name', 'last_name', 'nick_name', 'user_name'];

        $name       = isset($data['user_name']) ? $data['user_name'] : '';
		$password       = isset($data['password']) ? $data['password'] : '';

		$checkUser = User::WhereNotNull('id')
					->where(function($q) use ($name)
					{
						$q->where('user_name', '=', $name);

					})->first();

		if ( $checkUser )
		{
			if ( $name && $checkUser->name == $name )
			{
				$returnData['message'] = 'ชื่อ ' . $name . ' ถูกใช้ไปแล้ว';
				return $returnData;
			}
		}

        $user = new User();

		if ( ! empty($password) )
			$user->password = Hash::make( $password );

		foreach ( $userFields as $key => $field) {

			if ( isset($data[$field])) {

                $value        = $data[$field];
                $user->$field = $value;
			}
		}

        $user->created_by         = $app->user->id;
        $user->updated_by         = $app->user->id;
		$user->save();

		if ( $user->id )
		{
			$rolesAdded = self::insertRoles($app, $user->id, $roles);

			if ( count($rolesAdded) <= 0 ) {
			    $returnData['message'] = 'ไม่สามารถ Add Roles ได้';
				return $returnData;
			}

			$user->real_password = $password;

            $returnData['status'] = 1;
            $returnData['data']   = $toArray ? $user->toArray() : $user;
			return $returnData;
		}
		$returnData['message'] = 'ไม่สามารถสร้าง User ได้';
		return $returnData;
	}

	public function sendWelcomeEmail($app)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());
		$result = MyEmail::sendWelcomeEmail($app, $this);

		$returnData['status'] = 1;
		$returnData['message'] = 'Success send welcome message to ' . $this->email;
		return $returnData;
	}

	public static function updateRoles($app, $userId, $roles = [] )
	{
		$rolesUpdated = [];

		$roles = array_map('abs', $roles);

		RolesUsers::where('user_id', '=', $userId)->whereNotIn('role_id', $roles)->delete();

		foreach ( $roles AS $key => $roleId )
		{
			if ( $roleId <= 0 ) {
			    continue;
			}

			$rolesUsers = RolesUsers::where('user_id', '=', $userId)->where('role_id', '=', $roleId)->first();

			if ( empty($rolesUsers) )
			{
                $rolesUsers          = new RolesUsers;
                $rolesUsers->user_id = $userId;
                $rolesUsers->role_id = $roleId;

				if ( isset($app->user->id) )
				{
					$rolesUsers->created_by = $app->user->id;
					$rolesUsers->updated_by = $app->user->id;
				}
				$rolesUsers->save();

				if ( $rolesUsers->id )
					$rolesUpdated[] = $roleId;
			}
			else
			{
                $rolesUpdated[]         = $roleId;
                $rolesUsers->updated_by = $app->user->id;
				$rolesUsers->save();
			}
		}

		return $rolesUpdated;
	}

	public static function insertRolesIfEmpty($app, $userId, $roles = [] )
	{
		$userInfo =  User::with('roles')->find($userId);

		$userRoles = [];

		if ( $userInfo->roles ) {

		    foreach ( $userInfo->roles AS $key => $roleInfo )
			{
				$userRoles[] = $roleInfo->id;
			}
		}


		if ( count($userRoles) > 0 ) {
		    return [];
		}

		if ( ! is_array($roles) || count($roles) <= 0 ) {
		    $roles = array(2);
		}

		$roles = array_map('abs', $roles);

		$rolesAdded = [];

		foreach ( $roles AS $key => $roleId )
		{
			if ( $roleId <= 0 ) {
			    continue;
			}

			$rolesUsers = RolesUsers::where('user_id', '=', $userId)->where('role_id', '=', $roleId)->first();

			if ( empty($rolesUsers) )
			{
                $rolesUsers          = new RolesUsers;
                $rolesUsers->user_id = $userId;
                $rolesUsers->role_id = $roleId;
				if ( isset($app->user->id) )
				{
					$rolesUsers->created_by = $app->user->id;
					$rolesUsers->updated_by = $app->user->id;
				}
				$rolesUsers->save();

				if ( $rolesUsers->id )
					$rolesAdded[] = $roleId;
			}
			else
			{
				$rolesAdded[] = $roleId;
			}
		}
		return $rolesAdded;
	}

	public static function insertRoles($app, $userId, $roles = [] )
	{
		if ( ! is_array($roles) || count($roles) <= 0 ) {
		    $roles = array(2);
		}

		$roles = array_map('abs', $roles);

		$rolesAdded = [];

		foreach ( $roles AS $key => $roleId )
		{
			if ( $roleId <= 0 ) {
			    continue;
			}

			$rolesUsers = RolesUsers::where('user_id', '=', $userId)->where('role_id', '=', $roleId)->first();

			if ( empty($rolesUsers) )
			{
                $rolesUsers          = new RolesUsers;
                $rolesUsers->user_id = $userId;
                $rolesUsers->role_id = $roleId;
				if ( isset($app->user->id) )
				{
					$rolesUsers->created_by = $app->user->id;
					$rolesUsers->updated_by = $app->user->id;
				}
				$rolesUsers->save();

				if ( $rolesUsers->id )
					$rolesAdded[] = $roleId;
			}
			else
			{
				$rolesAdded[] = $roleId;
			}
		}
		return $rolesAdded;
	}

	public static function getData($app, $params)
	{
		return User::with('updatedUser')->with('roles')->find($params['user_id']);
	}

	public static function getDatas($app, $params, $sortField = array('id', 'user_name', 'first_name', 'last_name', 'updated_by', 'updated_at'))
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( ! is_array($sortField)) {
			$sortField = [];
		}

		if ( isset($params['sort'])) {

			$params['sort'] = urldecode($params['sort']);
			$startOfString = substr($params['sort'], 0, 1);
			if ( in_array($startOfString, ['-', ' ', '+']) ) {
				$params['order'] = $startOfString == '-' ? 'ASC' : 'DESC';
				$params['sort'] = substr($params['sort'], 1);
			}

			$params['sort'] = trim($params['sort']);
		}

        $draw   = isset($params['draw']) ? abs($params['draw']) : 1;
        $offset = isset($params['offset']) ? abs($params['offset']) : 0;
        $limit  = isset($params['limit']) ? abs($params['limit']) : ( isset($app->setting->admin_data_limit_perpage) ? $app->setting->admin_data_limit_perpage : 30 ) ;

		if ( $limit <= 0 || $limit > 1000 ) {
		    $limit = 30;
		}

		if ( ! is_array($sortField)) {
			$sortField = [];
		}
        $sortBy  = 'id';
        $orderBy = 'DESC';
        $sort    = isset($params['sort']) ? $params['sort'] : $sortBy;
        $order   = isset($params['order']) ? $params['order'] : $orderBy;

		if ( isset($sortField[$sort]) )
		{
			$sortBy = $sortField[$sort];
		}
		elseif ( in_array($sort, $sortField) )
		{
			$sortBy = $sort;
		}

		if ( in_array( strtoupper($order) , array('ASC','DESC')) )
		{
			$orderBy = $order;
		}

		$dataObjs = User::with('roles')->with('updatedUser');

        $recordsTotal  = $dataObjs->count('id');
        $filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataObjs = $dataObjs->where(function($query) use ($searchText)
            {
                $query
                ->where('user_name', 'LIKE', '%' . $searchText . '%')
                ->orWhere('first_name', 'LIKE', '%' . $searchText . '%')
				->orWhere('last_name', 'LIKE', '%' . $searchText . '%')
				->orWhere('nick_name', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataObjs->count('id');
        }

		$dataObjs = $dataObjs->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->get();

		$data = array();

		foreach ( $dataObjs AS $key => $obj )
		{
			$array = is_object($obj) ? $obj->toArray() : $obj;
			//dd($array);
            $data[]                   = $array;
		}


		$data = array('draw' => $draw, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $filteredCount, 'data' => $data);

        $returnData['status'] = 1;
        $returnData['data']   = $data;
		return $returnData;
	}


}
