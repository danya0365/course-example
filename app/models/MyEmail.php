<?php

class MyEmail {

	public static function sendConfirmToEmail($app, User $user, $confirmCode, $expiredAt)
	{
		$confirm_code_url = URL::to('user_email_confirm/' . $user->id . '/' . $confirmCode);

		Mail::send('emails.confirm_account', ['confirm_code' => $confirmCode, 'user' => $user, 'confirm_code_url' => $confirm_code_url], function($message) use ($user)
		{
		    $message->to($user->email, $user->name)->subject('Roti-webapp::Please Confirm Your account');
		});

	}

	public static function sendWelcomeEmail($app, User $user)
	{
		Mail::send('emails.welcome', ['user' => $user], function($message) use ($user)
		{
		    $message->to($user->email, $user->name)->subject('Roti-webapp::Welcome to Roti-webapp');
		});

	}

	public static function testSendEmail($app, $toEmail, $subject)
	{
		Mail::send('emails.test_send_email', ['toEmail' => $toEmail, 'subject' => $subject], function($message) use ($toEmail, $subject)
		{
		    $message->to($toEmail, $toEmail)->subject($subject);
		});

	}

	public static function sendRequestNewPassword($app, User $user, $secretCode, $expiredAt)
	{
		$set_new_password_url = URL::to('set_new_password/' . $user->id . '/' . $secretCode);

		Mail::send('emails.request_new_password', ['secret_code' => $secretCode, 'user' => $user, 'set_new_password_url' => $set_new_password_url], function($message) use ($user)
		{
		    $message->to($user->email, $user->name)->subject('Roti-webapp::You have requested to set new password');
		});

	}

}
