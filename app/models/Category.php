<?php

class Category extends Eloquent {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table      = 'categories';
    protected $primaryKey = 'id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'access_token', 'remember_token');


    public function courses(){

		return $this->hasMany('Course', 'category_id', 'id');
	}

    public function updatedUser(){

		return $this->belongsTo('User', 'updated_by', 'id')->select( ['id','user_name','nick_name'] );
	}

    public function toArray()
    {
        $array = parent::toArray();
        $array['DT_RowId']        = $array['id'];
        if ( isset($array['updated_at'])) {
            $array['updated_at_text'] = MyDatetime::timeElapsedToString($array['updated_at']);
        }

        $array = MyUtility::arrayKeyToString($array);
        return $array;
    }


    public static function selectOrCreateObj($app, $data, $isToArray = 1)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$category = self::whereNotNull('id');

		if ( isset($data['name']) && ! empty($data['name']) ) {
			$category->where('name', '=', trim($data['name']) );
		}

		if ( isset($data['id']) && $data['id'] > 0 ) {
			$category->where('id', '=', trim($data['id']) );
		}

		$category = $category->first();

		if ( ! $category ) {

			$result = self::createOrUpdateObj($app, $data);

			if ( $result['status'] == 1 ) {

                $category = $result['data'];
			}
		}


		if ( ! $category ) {

			$returnData['message'] = 'Can not get $category detail';
			return $returnData;
		}

        $category = $isToArray ? $category->toArray() : $category;

        $returnData['status'] = 1;
        $returnData['data']   = $category;
		return $returnData;
	}

    public static function deleteObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		$validator = Validator::make(
			$data,
			array(
				'id' => 'required',
			),
			array(
		    	'same'    => 'The :attribute and :other must match.',
			    'size'    => 'The :attribute must be exactly :size.',
			    'between' => 'The :attribute must be between :min - :max.',
			    'in'      => 'The :attribute must be one of the following types: :values',
			)
		);


		if ($validator->fails())
		{
			$messages = $validator->messages();
			$errorMessage = '';
			foreach ($messages->all() as $message)
			{
				if ( $errorMessage )
					break;

				$errorMessage = $message;
			}

			$returnData['message'] = $errorMessage;
			return $returnData;

		}

		self::where('id', '=', $data['id'])->delete();

		$returnData['status'] = 1;
		return $returnData;
	}

	public static function createOrUpdateObj($app, $data)
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

        $id = isset($data['id']) ? $data['id']: 0;

        $category = null;

        if ( $id ) {
            $category = self::find($id);
        }


        if ( ! $category && isset($data['name'])) {
            $category = self::where('name', '=', $data['name'])->first();
        }

		if ( ! $category ) {


    		$validator = Validator::make(
    			$data,
    			array(
                    'name'      => 'required'
    			),
    			array(
    		    	'same'    => 'The :attribute and :other must match.',
    			    'size'    => 'The :attribute must be exactly :size.',
    			    'between' => 'The :attribute must be between :min - :max.',
    			    'in'      => 'The :attribute must be one of the following types: :values',
    			)
    		);


    		if ($validator->fails())
    		{
    			$messages = $validator->messages();
    			$errorMessage = '';
    			foreach ($messages->all() as $message)
    			{
    				if ( $errorMessage )
    					break;

    				$errorMessage = $message;
    			}

    			$returnData['message'] = $errorMessage;
    			return $returnData;

    		}

            $category             = new self();
            $category->created_by = $app->user->id;
		}

        $optionTableField = ['name'];

        foreach ($optionTableField as $key => $field) {

            if ( isset($data[$field])) {

                $category->$field = $data[$field];
            }
        }

        $category->updated_by = $app->user->id;
        $category->save();

        if ( ! $category->id ) {

            $returnData['message'] = 'can not create or update $category data';
            return $returnData;
        }

        $returnData['status'] = 1;
        $returnData['data']   = $category;
		return $returnData;
	}

    public static function getDatas($app, $params, $sortField = array('id', 'name', 'updated_by', 'updated_at'))
	{
		$returnData = array('status' => 0, 'message' => '', 'data' => array());

		if ( ! is_array($sortField)) {
			$sortField = [];
		}

		if ( isset($params['sort'])) {

			$params['sort'] = urldecode($params['sort']);
			$startOfString = substr($params['sort'], 0, 1);
			if ( in_array($startOfString, ['-', ' ', '+']) ) {
				$params['order'] = $startOfString == '-' ? 'ASC' : 'DESC';
				$params['sort'] = substr($params['sort'], 1);
			}

			$params['sort'] = trim($params['sort']);
		}

        $draw   = isset($params['draw']) ? abs($params['draw']) : 1;
        $offset = isset($params['offset']) ? abs($params['offset']) : 0;
        $limit  = isset($params['limit']) ? abs($params['limit']) : ( isset($app->setting->admin_data_limit_perpage) ? $app->setting->admin_data_limit_perpage : 30 ) ;

		if ( $limit <= 0 || $limit > 1000 ) {
		    $limit = 30;
		}

		if ( ! is_array($sortField)) {
			$sortField = [];
		}
        $sortBy  = 'id';
        $orderBy = 'ASC';
        $sort    = isset($params['sort']) ? $params['sort'] : $sortBy;
        $order   = isset($params['order']) ? $params['order'] : $orderBy;

		if ( isset($sortField[$sort]) )
		{
			$sortBy = $sortField[$sort];
		}
		elseif ( in_array($sort, $sortField) )
		{
			$sortBy = $sort;
		}

        if ( in_array( strtoupper($order) , array('ASC','DESC')) )
        {
            $orderBy = $order;
        }


        $dataObjs      = self::with('updatedUser');

        $recordsTotal  = $dataObjs->count('id');
        $filteredCount = $recordsTotal;


		$searchText = isset($params['search']) ? $params['search'] : '';

		if ( ! empty($searchText) )
		{
            $dataObjs = $dataObjs->where(function($query) use ($searchText)
            {
                $query->where('name', 'LIKE', '%' . $searchText . '%');

            });

            $filteredCount = $dataObjs->count('id');
        }

		$dataObjs = $dataObjs->skip($offset)->take($limit)->orderBy($sortBy, $orderBy)->orderBy('id', 'DESC')->get();

		$data = array();

		foreach ( $dataObjs AS $key => $obj )
		{
			$array = is_object($obj) ? $obj->toArray() : $obj;
            $data[]                   = $array;
		}


		$data = array('draw' => $draw, 'recordsTotal' => $recordsTotal, 'recordsFiltered' => $filteredCount, 'data' => $data, 'params' => $params);

        $returnData['status'] = 1;
        $returnData['data']   = $data;
		return $returnData;
	}

}
