<?php

class Setting extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	 public static function updateObj($app,  $dataUpdate)
	 {
	 	$returnData = array('status' => 0, 'message' => '', 'data' => array());

	 	foreach ( $dataUpdate AS $var => $value )
	 	{
	 		Setting::where('var', 'LIKE', $var)->update( array('value' => $value) );
	 	}

		$returnData['status'] = 1;
		$returnData['message'] = 'Updated!';
		return $returnData;

	 }
}
